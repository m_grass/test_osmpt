'''
Geographic data of targets-------------------

Put in the geographic coordinates of your target of interest and name it.
Use the "decimal degrees" coordintates (format) specified on this website as input: (West means negativ longitude values)
http://dateandtime.info/citycoordinates.php
Note that there are some restrictions:
Don't use spaces and other special symbols for the target name. To seperate use "_" e.g. NEW_YORK
Naming the target in capital letters is good practice.
It will add your target to all existing targets. If you want to see only your target use the "exclude old targets" parameter.
If you want to exclude the station in cosmographia as well, use the "exclude station" parameter.

You cannot use the same name twice. Therefore you have to delete the spk/targets/"name".bsp and fk/targets/"name".tf files before.
'''

import os
import subprocess
import json
from collections import OrderedDict
from h.path_setup import flp_path
from h.programs.mk_creation import create_mk

target_name = "AALEN"
target_lat = 48.8377700  # [degree]
target_lon = 10.0933000  # [degree]
target_alt = 0.433  # [km]

exclude_old_targets = 'off'  # 'on'/'off' value. On means only your newly specified target will be shown.
exclude_station = 'off'  # no stations will be shown in the cosmographia view.


def move_new_to_old():
    file = os.listdir(flp_path.json_target_new)
    # there should be only 1 file
    if file != []:
        move_json_target = 'mv ' + flp_path.json_target_new + file[0] + ' ' + flp_path.json_target_old + file[0]
        subprocess.call(move_json_target, shell=True)


def create_tf_setup(target_name,target_lat,target_lon,target_alt):

    # get the next free naif ID code. This is critical!
    ID_code_file = open(flp_path.spk_target_id_code, 'r')
    ID_code_old = ID_code_file.read()
    ID_code_new = int(ID_code_old)+1
    ID_code_file.close()
    ID_code_file = open(flp_path.spk_target_id_code, 'w')
    ID_code_file.write(str(ID_code_new))
    ID_code_file.close()

    # write pinpoint input file
    file = open(flp_path.spk_target_setup, 'w')

    text = str(
        "\\begindata\n\n" +

        "SITES                      += ( '" + target_name + "' )\n" +
        target_name + "_CENTER       = 399 \n" +
        target_name + "_FRAME        = 'ITRF93' \n" +
        target_name + "_IDCODE       = " + str(ID_code_new) + " \n" +
        target_name + "_LATLON       = ( " + str(target_lat) + ' ' + str(target_lon) + ' ' + str(target_alt) + " ) \n" +
        target_name + "_BOUNDS       = (@2017-JAN-1, @2020-JAN-1) \n" +
        target_name + "_UP       = 'Z' \n" +
        target_name + "_NORTH      = 'X' \n\n" +

        "\\begintext\n\n"
    )
    file.write(text)
    file.close()


def create_target_pinpoint(target_name):
    # Run the pinpoint utility
    station_command = 'pinpoint ' + '-def ' + flp_path.spk_target_setup + ' -pck ' + flp_path.pck_normal + \
                 ' -spk ' + flp_path.spk_target + target_name + '.bsp' + ' -fk ' + flp_path.fk_target + target_name + '.tf'
    subprocess.call(station_command, shell=True)


def create_json_target(target_name):

    trajectory = OrderedDict([('type', 'Spice'),
                              ('target', target_name),
                              ('center', 'Earth')])
    items = OrderedDict([('name', target_name),
                         ('center', 'Earth'),
                         ('trajectory', trajectory)])
    json_data = OrderedDict([('version', '1.0'),
                             ('name', target_name),
                             ('items', [items])])

    json_path = flp_path.json_target_new + target_name + ".json"
    with open(json_path, 'w') as outfile:
        json.dump(json_data, outfile, indent=3)


def update_scenario_json(exclude_old_targets, exclude_station):

    items = [
        "./metakernels.json",
        "./spacecraft_FLP.json",
        "./sensors/FLP_PAMCAM.json",
        "./sensors/FLP_OSIRIS.json"
    ]
    if exclude_station != 'on':
        items.append("./stations.json")

    if exclude_old_targets != 'on':
        target_items_old = os.listdir(flp_path.json_target_old)
        for i in range(len(target_items_old)):
            items.append("./targets/old/" + target_items_old[i])

    target_item_new = os.listdir(flp_path.json_target_new)
    items.append("./targets/new/" + target_item_new[0])  # there should only be 1 new item

    json_data = OrderedDict([('version', '1.0'),
                        ('name', 'load FLP'),
                        ('require',
                            items
                        )])

    json_path = flp_path.json_scenario
    with open(json_path, 'w') as outfile:
        json.dump(json_data, outfile, indent=3)


move_new_to_old()
create_tf_setup(target_name,target_lat,target_lon,target_alt)
create_target_pinpoint(target_name)
create_json_target(target_name)
update_scenario_json(exclude_old_targets, exclude_station)
create_mk()
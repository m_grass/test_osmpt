'''
This file creates the spk and fk kernels for all ground stations. To add new ground stations, open the spk100_setup file
and add a new ground station.
'''

import os
import subprocess
from h.path_setup import flp_path

# Development state- has to be changed...right now every sclk and ck files are deleted before a new one is created
if os.listdir(flp_path.spk_station) != []:
    remove_spk = 'rm ' + flp_path.spk_station + '*'
    subprocess.call(remove_spk, shell=True)
if os.listdir(flp_path.fk_station) != []:
    remove_fk = 'rm ' + flp_path.fk_station + '*'
    subprocess.call(remove_fk, shell=True)

# Run the pinpoint utility
station_command = 'pinpoint ' + '-def ' + flp_path.spk_station_setup + ' -pck ' + flp_path.pck_normal + \
                 ' -spk ' + flp_path.spk_station_spk + ' -fk ' + flp_path.fk_station_fk
subprocess.call(station_command, shell=True)
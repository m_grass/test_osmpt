'''
This file creates the spk kernels for all instruments. To add new instruments, open the instrument_setup file
and add a new instrument.
'''

import os
import subprocess
from .path_setup import flp_path

# Development state- has to be changed...right now every sclk and ck files are deleted before a new one is created
if os.listdir(flp_path.spk_instrument) != []:
    remove_spk = 'rm ' + flp_path.spk_instrument + '*'
    subprocess.call(remove_spk, shell=True)

# Run the pinpoint utility
station_command = 'pinpoint ' + '-def ' + flp_path.spk_instrument_setup + ' -spk ' + flp_path.spk_instrument_spk
subprocess.call(station_command, shell=True)
import os

def create_mk(flp_path):
    '''
    This script samples all files in the directories needed to run the FLP mission scenario. It is divided into 5 sections.
    1. static naif files
    2. static FLP files
    3. semi-static files (they don't belong to the FLP SC but shouldn't be changed. So far there's only ground stations)
    4. dynamic FLP files (SPK, CK - both created for the specific mission scenario)
    5. targets (Points of interests on the Earth surface)

    Notes:
    - the "kernel path" value still printed in the MK is not used but might be used in later releases.
    - observations shown in cosmographia do not belong to kernel files. They are only produced out of these kernel
    files for visualisation purposes.
    :param flp_path: all paths needed
    :return:
    '''

    file = open(flp_path.mk_out, 'w')

    # write opening and static kernels to mk
    path_values = "( '" + flp_path.mk_sample_path + "' )"
    path_symbols = "( 'KERNELS' )"
    kernels_to_load = "(\n\n" \
                      + "'" + flp_path.lsk_file + "', \n" \
                      + "'" + flp_path.spk_file + "', \n" \
                      + "'" + flp_path.pck_normal + "', \n" \
                      + "'" + flp_path.pck_itrf + "', " \
                      + "\n\n" \
                      + "'" + flp_path.sclk_file + "', \n" \
                      + "'" + flp_path.fk_file + "', \n" \
                      + "'" + flp_path.ik_file + "'," \
                      + "\n\n" \
                      + "'" + flp_path.fk_station_fk + "', \n" \
                      + "'" + flp_path.spk_station_spk + "'," \
                      + "\n\n"

    text_1 = str(
        "\\begindata\n\n"
        "PATH_VALUES = " + path_values + "\n\n"
        "PATH_SYMBOLS = " + path_symbols + "\n\n"
        "KERNELS_TO_LOAD = " + kernels_to_load
    )
    file.write(text_1)

    # write dynamic kernels to mk
    # NOTE: order of for-loops is very important as this defines the order listed in the mk!!
    for gmat_out in sorted(os.listdir(flp_path.gmat_out)):
        if not gmat_out.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.gmat_out + gmat_out + "'", "\n"]))
    for spk_prop in sorted(os.listdir(flp_path.spk_prop)):
        if not spk_prop.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.spk_prop + spk_prop + "'", "\n"]))
    for spk_out in sorted(os.listdir(flp_path.spk_out)):
        if not spk_out.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.spk_out + spk_out + "'", "\n\n"]))

    for ck_prop in sorted(os.listdir(flp_path.ck_prop)):
        if not ck_prop.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.ck_prop + ck_prop + "'", "\n"]))
    for ck_out in sorted(os.listdir(flp_path.ck_out)):
        if not ck_out.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.ck_out + ck_out + "'", "\n"]))

    file.write("\n")
    for fk_target in sorted(os.listdir(flp_path.fk_target)):
        if not fk_target.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.fk_target + fk_target + "'", "\n"]))
    file.write("\n")
    for spk_target in sorted(os.listdir(flp_path.spk_target)):
        if not spk_target.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.spk_target + spk_target + "'", "\n"]))

    file.write("\n")

    file.close()
'''
This script produces 3 CK propagation kernels. The most basic file is the "idle CK" which points towards the Sun. This
kernel takes over if no other attitude data is loaded. A second "default" kernel is a nadir pointing kernel which has
priority compared with the idle CK but not for the other two kernels. The third CK file produces pointing data for every
IRS flyby (or any other GS (target))above a specific topocentric elevation. This makes sure that the FLP points towards
the GS... .
Note that it cannot rotate around a specific orientation (no frequency for a defined rotation around an axes can be
simulated) --> for this, a routine needs to be implemented to produce the quaternions (as quaternion_computation.py) and
then transfered to a CK using MSOPCK.
No slew times are implemented which means that the position transfer is done instantaneously.
User defined orientations with fixed quaternions, offset to directions, etc. can be implemented in later steps.
'''


import os
import subprocess
import spiceypy as cspice
import numpy as np
from h.path_setup import flp_path

# all times in UTC
idle = 'off'
nadir = 'off'
nadir_times = [
    '2018 JUN 01 12:00:00', '2018 JUN 02 12:00:10',
    '2018 JUN 04 12:00:20', '2018 JUN 05 12:00:30'
    ]

target_pointing = 'off'
# every GS or target inside the ck_prop_furnsh metakernel. It will point to it for ALL passes, not a specific one!
target_p = 'IRS'
degree = 10  # mininlal elevation of the FLP SC in the topcentric target frame to be considered as pass

moon_pointing = 'off'
moon_pointing_times = [
    '2018 JUN 01 17:35:53.027', '2018 JUN 01 17:36:07.636',  # times out of occultation_computation.py script
    '2018 JUN 02 04:26:36.216', '2018 JUN 02 04:26:53.775'
    ]

nadir_offset = 'on'  # offset in left/right direction NOT forward/backwards direction of nadir mode!
nadir_offset_values = [
    '2018 MAY 19 08:54:38.259', '2018 MAY 19 08:59:38.259', 55.04589357278617,  # times out of quaternion_angle.py script
    '2018 MAY 19 10:30:09.372', '2018 MAY 19 10:33:09.372', -48.330314368244814  # angle in degree!!
    ]


cspice.furnsh(flp_path.lsk_file)


def create_ck_prop_furnsh():

    kernels_to_load = "(\n\n" \
                      + "'" + flp_path.lsk_file + "', \n" \
                      + "'" + flp_path.spk_file + "', \n" \
                      + "'" + flp_path.pck_normal + "', \n" \
                      + "'" + flp_path.pck_itrf + "', " \
                      + "\n\n" \
                      + "'" + flp_path.sclk_file + "', \n" \
                      + "'" + flp_path.fk_file + "', \n" \
                      + "'" + flp_path.ik_file + "'," \
                      + "\n\n" \
                      + "'" + flp_path.fk_station_fk + "', \n" \
                      + "'" + flp_path.spk_station_spk + "'," \
                      + "\n\n"

    file = open(flp_path.ck_prop_furnsh, 'w')

    text = str(
        "\\begindata\n\n"
        "KERNELS_TO_LOAD = " + kernels_to_load
    )
    file.write(text)

    for spk_prop in sorted(os.listdir(flp_path.spk_prop)):
        if not spk_prop.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.spk_prop + spk_prop + "'", "\n\n"]))
    for fk_target in sorted(os.listdir(flp_path.fk_target)):
        if not fk_target.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.fk_target + fk_target + "'", "\n"]))
    for spk_target in sorted(os.listdir(flp_path.spk_target)):
        if not spk_target.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.spk_target + spk_target + "'", "\n"]))

    file.write(")\n\n")
    file.write("\\begintext\n\n")
    file.close()


def create_ck_prop_idle():

    # the function below is needed after we know where we want to point (for near future ck generation)
    create_ck_prop_idle_spec()

    # Run the prediCkt utility
    ck_command_GPS = 'prediCkt -furnish ' + flp_path.ck_prop_furnsh + ' -spec ' + flp_path.ck_prop_idle_spec + \
                     ' -ck ' + flp_path.ck_prop_idle_ck + ' -tol 0.5 degrees -sclk ' + flp_path.sclk_file
    subprocess.call(ck_command_GPS, shell=True)


def create_ck_prop_idle_spec():

    cover = cspice.utils.support_types.SPICEDOUBLE_CELL(1000)
    cspice.spkcov(flp_path.spk_prop_name, -513, cover)
    style = 'DD-MON-YYYY-HR:MN:SC.### ::TDB'  # TDB needed to convert to "right" time. Otherwise there is an offset of approx. 70 sec.
    time_start = cspice.timout(np.ceil(cover[0]), style, 30)
    time_end = cspice.timout(np.floor(cover[-1]), style, 30)

    file = open(flp_path.ck_prop_idle_spec, 'w')

    text = str(
        "\\begindata\n\n" +

        "DIRECTION_SPECS += ('TO_EARTH = POSITION OF EARTH -', \n" +
        "' FROM -513 -', \n" +
        "' CORRECTION NONE')\n\n"

        "DIRECTION_SPECS += ('TO_SUN = POSITION OF SUN -', \n" +
        "' FROM -513 -', \n" +
        "' CORRECTION NONE')\n\n\n"

        "ORIENTATION_NAME += 'IDLE'\n" +
        "PRIMARY += '-Z = TO_SUN'\n" +
        "SECONDARY += 'Y = TO_EARTH'\n" +
        "BASE_FRAME += 'J2000'\n\n\n" +

        "CK-513000ORIENTATION += 'IDLE'\n" +
        "CK-513000START +=@" + time_start +"\n" +
        "CK-513000STOP +=@" + time_end + "\n\n\n" +

        "CK-SCLK = 513\n" +
        "CK-SPK = -513\n" +
        "CK-FRAMES = -513000\n\n" +

        "\\begintext\n\n"
    )
    file.write(text)
    file.close()


def create_ck_prop_nadir(nadir_times):

    # the function below is needed after we know where we want to point (for near future ck generation)
    create_ck_prop_nadir_spec(nadir_times)

    # Run the prediCkt utility
    ck_command_GPS = 'prediCkt -furnish ' + flp_path.ck_prop_furnsh + ' -spec ' + flp_path.ck_prop_nadir_spec + \
                     ' -ck ' + flp_path.ck_prop_nadir_ck + ' -tol 0.001 degrees -sclk ' + flp_path.sclk_file
    subprocess.call(ck_command_GPS, shell=True)


def create_ck_prop_nadir_spec(nadir_times):

    nadir_times_et = np.array([cspice.utc2et(nadir_times[i]) for i in range(len(nadir_times))])
    style = 'DD-MON-YYYY-HR:MN:SC.### ::TDB'
    time = np.array([cspice.timout(nadir_times_et[i], style, 30) for i in range(len(nadir_times_et))])

    file = open(flp_path.ck_prop_nadir_spec, 'w')

    text = str(
        "\\begindata\n\n" +

        "DIRECTION_SPECS += ('TO_EARTH = POSITION OF EARTH -', \n" +
        "' FROM -513 -', \n" +
        "' CORRECTION NONE')\n\n"

        "DIRECTION_SPECS += ('FLP_VEL = VELOCITY OF -513 -', \n" +
        "' FROM EARTH -', \n" +
        "' CORRECTION NONE')\n\n\n"

        "ORIENTATION_NAME += 'NADIR'\n" +
        "PRIMARY += 'Z = TO_EARTH'\n" +
        "SECONDARY += 'X = FLP_VEL'\n" +
        "BASE_FRAME += 'J2000'\n\n" +

        "CK-SCLK = 513\n" +
        "CK-SPK = -513\n" +
        "CK-FRAMES = -513000\n\n"
    )
    file.write(text)

    for i in range(0,len(time),2):

        time_window = str(
            "CK-513000ORIENTATION += 'GOTO NADIR'\n" +
            "CK-513000START +=@" + time[i] + "\n" +
            "CK-513000STOP +=@" + time[i+1] + "\n\n"
        )

        file.write(time_window)

    file.write("\\begintext\n\n")

    file.close()


def create_ck_prop_pass_all(target_p, degree):
    # the function below is needed after we know where we want to point (for near future ck generation)
    create_ck_prop_pass_all_spec(target_p, degree)

    # Run the prediCkt utility
    ck_command_GPS = 'prediCkt -furnish ' + flp_path.ck_prop_furnsh + ' -spec ' + flp_path.ck_prop_pass_all_spec + \
                     ' -ck ' + flp_path.ck_prop_pass_all_ck + ' -tol 0.01 degrees -sclk ' + flp_path.sclk_file
    subprocess.call(ck_command_GPS, shell=True)


def create_ck_prop_pass_all_spec(target_p, degree):

    cspice.furnsh(flp_path.ck_prop_furnsh)  # needed to be loaded here for GF and other SPICE routines to work...

    # precomputations to get all parameters
    obsrvr = target_p  # it is called observer because of the gf routine, think of it as "target" [valid: targets or GS generated with pinpoint]
    abcorr = 'NONE'
    refval = degree * cspice.rpd()  # elevation [degree] (after conversion)
    target_ID = str(cspice.bodn2c(obsrvr))  # ID code for target
    frame_obsrvr_pool = 'FRAME_1' + target_ID + '_NAME'  # this will work only for pinpoint generated tf files.
    frame_obsrvr = cspice.gcpool(frame_obsrvr_pool, 0, 1)[0]  # frame name of target
    crdsys = 'LATITUDINAL'
    coord = 'LATITUDE'
    relate = '>'
    adjust = 0
    step = 20 * 60  # stepsize [min]
    nintvls = 7500
    result = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    # get the start and stop times of the SPK propagation kernel
    cover = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cspice.spkcov(flp_path.spk_prop_name, -513, cover)
    et = [cover[0]+1, cover[-1]-1]  # 1 second margin as it somehow did not work without that margin...
    cspice.appndd(et, cnfine)

    # compute gf search
    # elevation above x [degrees]
    cspice.gfposc('FLP', frame_obsrvr, abcorr, obsrvr, crdsys, coord, relate, refval, adjust, step, nintvls, cnfine,
                  result)
    count = cspice.wncard(result)
    print("Number of result windows: " + str(count))

    style = 'DD-MON-YYYY-HR:MN:SC.### ::TDB'  # TDB needed to convert to "right" time. Otherwise there is an offset of approx. 70 sec.
    time = []
    for k in range(count):
        # get pass time
        et_start, et_stop = cspice.wnfetd(result, k)
        time.append(cspice.timout(et_start, style, 30))
        time.append(cspice.timout(et_stop, style, 30))

    file = open(flp_path.ck_prop_pass_all_spec, 'w')

    text = str(
        "\\begindata\n\n" +

        "DIRECTION_SPECS += ('TO_" + target_p + " = POSITION OF " + target_p + " -', \n" +
        "' FROM -513 -', \n" +
        "' CORRECTION NONE')\n\n"
        
        "DIRECTION_SPECS += ('FLP_VEL = VELOCITY OF -513 -', \n" +
        "' FROM EARTH -', \n" +
        "' CORRECTION NONE')\n\n"

        "ORIENTATION_NAME += '" + target_p + "'\n" +
        "PRIMARY += 'Z = TO_" + target_p + "'\n" +
        "SECONDARY += 'X = FLP_VEL'\n" +
        "BASE_FRAME += 'J2000'\n\n\n" + 

        "CK-SCLK = 513\n" +
        "CK-SPK = -513\n" +
        "CK-FRAMES = -513000\n\n"
    )
    file.write(text)

    for i in range(0, len(time), 2):
        time_window = str(
            "CK-513000ORIENTATION += 'GOTO " + target_p + "'\n" +
            "CK-513000START +=@" + time[i] + "\n" +
            "CK-513000STOP +=@" + time[i + 1] + "\n\n"
        )

        file.write(time_window)

    file.write("\\begintext\n\n")
    file.close()


def create_ck_prop_moon_pointing(moon_pointing_times):

    # the function below is needed after we know where we want to point (for near future ck generation)
    create_ck_prop_moon_pointing_spec(moon_pointing_times)

    # Run the prediCkt utility
    ck_command_GPS = 'prediCkt -furnish ' + flp_path.ck_prop_furnsh + ' -spec ' + flp_path.ck_prop_moon_pointing_spec + \
                     ' -ck ' + flp_path.ck_prop_moon_pointing_ck + ' -tol 0.001 degrees -sclk ' + flp_path.sclk_file
    subprocess.call(ck_command_GPS, shell=True)


def create_ck_prop_moon_pointing_spec(moon_pointing_times):
    moon_pointing_times_et = np.array([cspice.utc2et(moon_pointing_times[i]) for i in range(len(moon_pointing_times))])
    style = 'DD-MON-YYYY-HR:MN:SC.### ::TDB'
    time = np.array([cspice.timout(moon_pointing_times_et[i], style, 30) for i in range(len(moon_pointing_times_et))])

    file = open(flp_path.ck_prop_moon_pointing_spec, 'w')

    text = str(
        "\\begindata\n\n" +

        "DIRECTION_SPECS += ('TO_MOON = POSITION OF MOON -', \n" +
        "' FROM -513 -', \n" +
        "' CORRECTION NONE')\n\n"

        "DIRECTION_SPECS += ('TO_SUN = POSITION OF SUN -', \n" +
        "' FROM -513 -', \n" +
        "' CORRECTION NONE')\n\n\n"

        "ORIENTATION_NAME += 'MOON_POINTING'\n" +
        "PRIMARY += 'Z = TO_MOON'\n" +
        "SECONDARY += 'Y = TO_SUN'\n" +
        "BASE_FRAME += 'J2000'\n\n" +

        "CK-SCLK = 513\n" +
        "CK-SPK = -513\n" +
        "CK-FRAMES = -513000\n\n"
    )
    file.write(text)

    for i in range(0,len(time),2):

        time_window = str(
            "CK-513000ORIENTATION += 'GOTO MOON_POINTING'\n" +
            "CK-513000START +=@" + time[i] + "\n" +
            "CK-513000STOP +=@" + time[i+1] + "\n\n"
        )

        file.write(time_window)

    file.write("\\begintext\n\n")

    file.close()


def create_ck_prop_nadir_offset(nadir_offset_values):

    # the function below is needed after we know where we want to point (for near future ck generation)
    create_ck_prop_nadir_offset_spec(nadir_offset_values)

    # Run the prediCkt utility
    ck_command_GPS = 'prediCkt -furnish ' + flp_path.ck_prop_furnsh + ' -spec ' + flp_path.ck_prop_nadir_offset_spec + \
                     ' -ck ' + flp_path.ck_prop_nadir_offset_ck + ' -tol 0.001 degrees -sclk ' + flp_path.sclk_file
    subprocess.call(ck_command_GPS, shell=True)


def create_ck_prop_nadir_offset_spec(nadir_offset_values):
    nadir_offset_times_et = []
    nadir_offset_angles = []
    for i in range(len(nadir_offset_values)):
        if (i+1)%3 != 0:
            nadir_offset_times_et.append(cspice.utc2et(nadir_offset_values[i]))
        else:
            nadir_offset_angles.append(nadir_offset_values[i])

    style = 'DD-MON-YYYY-HR:MN:SC.### ::TDB'
    time = np.array([cspice.timout(nadir_offset_times_et[i], style, 30) for i in range(len(nadir_offset_times_et))])

    file = open(flp_path.ck_prop_nadir_offset_spec, 'w')

    text = str(
        "\\begindata\n\n" +

        "DIRECTION_SPECS += ('TO_EARTH = POSITION OF EARTH -', \n" +
        "' FROM -513 -', \n" +
        "' CORRECTION NONE')\n\n"

        "DIRECTION_SPECS += ('FLP_VEL = VELOCITY OF -513 -', \n" +
        "' FROM EARTH -', \n" +
        "' CORRECTION NONE')\n\n\n"
    )
    file.write(text)

    for i in range(len(nadir_offset_angles)):

        dir_spec = str(
            "DIRECTION_SPECS += ('PositionAngle_" + str(i) + " = ROTATE TO_EARTH -', \n" +
            "' " + str(nadir_offset_angles[i]) + " degrees ABOUT - ', \n" +
            "' FLP_VEL' ) \n\n" +

            "ORIENTATION_NAME += 'NADIR_OFFSET_" + str(i) + "'\n" +
            "PRIMARY += 'Z = PositionAngle_" + str(i) + "'\n" +
            "SECONDARY += 'Y = FLP_VEL'\n" +
            "BASE_FRAME += 'J2000'\n\n"

            "CK-513000ORIENTATION += 'GOTO NADIR_OFFSET_" + str(i) + "'\n" +
            "CK-513000START +=@" + time[i*2] + "\n" +
            "CK-513000STOP +=@" + time[i*2 + 1] + "\n\n\n"
        )
        file.write(dir_spec)


    text_2 = str(
        "CK-SCLK = 513\n" +
        "CK-SPK = -513\n" +
        "CK-FRAMES = -513000\n\n"
    )
    file.write(text_2)

    file.write("\\begintext\n\n")

    file.close()


def delete_inputs():

    remove_old_spk_source = "rm -f " + flp_path.ck_prop + "*"  # delete kernels/ck/propagation/ files...
    subprocess.call(remove_old_spk_source, shell=True)


delete_inputs()
create_ck_prop_furnsh()
if idle == 'on':
    create_ck_prop_idle()
if nadir == 'on':
    create_ck_prop_nadir(nadir_times)
if target_pointing == 'on':
    create_ck_prop_pass_all(target_p, degree)
if moon_pointing == 'on':
    create_ck_prop_moon_pointing(moon_pointing_times)
if nadir_offset == 'on':
    create_ck_prop_nadir_offset(nadir_offset_values)

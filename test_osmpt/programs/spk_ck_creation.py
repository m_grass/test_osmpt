


# -------------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------------
def spk_ck_creation():
    '''
    The TM data is stored in /home/flp/H/h/TM . The routine will load all data stored inside this directory. It will
    automatically detect whether it is a GPS or attitude file. The data need to be provided as a csv file using the
    following structure:

    GPS file:
    header(first line): "time";"timestamp";"AYTPOS00";"AYTPOS01";"AYTPOS02";"AYTVEL00";"AYTVEL01";"AYTVEL02"
    data(all other lines): jjjj-mm-dd HR:MN:SC.###;FLP timestamp;pos x;pos y; pos z;vel x;vel y;vel z
    Attitude file:
    header(first line): "time";"timestamp";"AYTFQU00";"AYTFQU01";"AYTFQU02";"AYTFQU03"
    data(all other lines): jjjj-mm-dd HR:MN:SC.###;FLP timestamp;quat 1;quat 2; quat 3; quat 4

    where ### represents  an integer, FLP timestamp represents a float type
    pos x,y,z, vel x,y,z and quat 1,2,3,4 are float type [m] ; [m/s] ; [-]
    quat style: sin(alpha/2), sin(alpha/2), sin(alpha/2) cos(alpha/2) --> no rotation = 0;0;0;1

    data lines with default values (pos = 0,0,0 or quat = 0,0,0,1) are treated as follows:
    GPS data:
    The line will will not be used but if the option gmat_prop is set to "on" the routine will substitute the gap with
    values on an "educated guess" precision.
    Attitude data:
    The line will not be used at all. You will get a "no spice data" error for this epoch.
    :return: Nothing
    '''


    flp_path = path_setup.flp_path()

    plots = 'off'  # if set to "on" a plot for each input file will be drawn
    figure_size = [20, 5]  # window size of the plots shown
    timesteps_plot = 10000  # how many data points should be calculated for the plot [not more then seconds of the interval]
    step_spk = 1  # define that only each nth-line will be stored for further computations (kernel generation)
    step_ck = 1  # define that only each nth-line will be stored for further computations (kernel generation)

    poly_degree = 2  # Polynomial order of spk data fitting curve
    max_interval_spk = 200  # time interval between two measured GPS data sets [s]   (200)
    max_interval_ck = 600  # time interval between two measured attitude points [s]  (600)

    gmat_prop = 'on'  # if set to "on" TM GPS data gaps will be propagated using GMAT
    SPK_dif_plot = 'on'  # If set to "on" no overlappting TM data is allowed! Moreover, gmat_prop needs to be set to "on"

    if plots != 'on' and plots != 'off':
        print('Invalid option. Allowed options are: \n\n' +
              'on:        enables plots\n')
    if step_ck == 0 or step_spk == 0:
        print('Invalid option. Allowed options are: \n\n' +
              'all positiv integers (excluding 0)\n')
    else:
        delete_inputs(flp_path)
        data_utc_sc_data_all_spk, data_utc_sc_data_all_ck = manage_input_data(plots, figure_size, step_spk, step_ck,
                                                                              poly_degree, max_interval_spk, flp_path)
        if data_utc_sc_data_all_spk != []:
            gmat = create_input_spk(poly_degree, flp_path)
            if gmat_prop == 'on' and gmat == "yes":
                prop_spk_gaps(flp_path)
            plot_validation_spk(data_utc_sc_data_all_spk, gmat_prop, figure_size, timesteps_plot, flp_path, gmat)
            if SPK_dif_plot == 'on':
                plot_spk_dif(data_utc_sc_data_all_spk, figure_size, flp_path, gmat_prop, gmat)
        if data_utc_sc_data_all_ck != []:
            create_input_ck(max_interval_ck, flp_path)
            plot_validation_ck(data_utc_sc_data_all_ck, figure_size, timesteps_plot, flp_path)
        create_mk(flp_path)


def manage_input_data(plots, figure_size, step_spk, step_ck, poly_degree, max_interval_spk, flp_path):

    # Get all TM input files
    print('Searching for input files ...')
    files_tmp = []
    for file in os.listdir(flp_path.TM_path):
        if file.endswith(".csv"):
            files_tmp.append(file)
    marker = '~'
    files = []
    for i in range(len(files_tmp)):
        if marker not in files_tmp[i]:
            files.append(files_tmp[i])
    print('Amount of input files detected: ' + str(len(files)))

    # Create new spk and ck source files
    cspice.furnsh(flp_path.lsk_file)  # needed to convert between times using cspice functions

    print('Started TM import ...')
    data_utc_sc_data_all_spk = []
    data_utc_sc_data_all_ck = []
    for i in range(len(files)):
        with open(flp_path.TM_path + files[i]) as file:

            # read file content
            reader = csv.reader(file, delimiter=';')
            rows = [r for r in reader]
            print('File ' + str(i+1) + ' (' + files[i] + ') has ' + str(len(rows)) + ' rows')
            header = rows[0]
            print('Computations ongoing ...')
            input_data = rows[1:]

            # check for default lines and store data in lists
            utc = []
            sc_data = []
            data_utc_sc_data = []
            cspice.furnsh(flp_path.lsk_file)
            for k in range(len(input_data)):
                if input_data[k][2] != '0':
                    utc.append(input_data[k][0])
                    sc_data.append(input_data[k][2:])
                    data_utc_sc_data.append(input_data[k])

            if 'AYTPOS00' in header[2]:

                # find GPS data gaps inside the actual input file
                ET = np.array([cspice.utc2et(utc[i]) for i in range(len(utc))])
                index_cut = [0]
                for i in range(len(ET)-1):
                    if ET[i+1] - ET[i] > max_interval_spk:
                        index_cut.append(i)
                index_cut.append(len(ET)-1)  # (second) last element ... really needed. otherwise the propagation etc gets weird. !!!!
                u = 0

                # divide input file data into data frames for each continous periode.
                for i in range(len(index_cut)-1):
                    if index_cut[i+1] - index_cut[i] > poly_degree*step_spk:  # avoid "windows" with too less data points
                        utc_cut = []
                        sc_data_cut = []
                        for k in range(index_cut[i]+1,index_cut[i+1]):  # +1 because of better propagation fit. this number is adjustable.
                            utc_cut.append(utc[k])
                            sc_data_cut.append(sc_data[k])
                        sc_data_cut = np.asarray(sc_data_cut).astype(np.float) / 1000  # change to an array with [km] instead of list with [m]

                        if plots == 'on':
                            plot_input_data(utc_cut, sc_data_cut, header, figure_size)

                        # store the file
                        tmp_start = utc_cut[0].split(".")[0].replace("-", "").replace(" ", "").replace(":", "")
                        tmp_end = utc_cut[-1].split(".")[0].replace("-", "").replace(" ", "").replace(":", "")

                        name = 'TMsplit_spk_' + tmp_start + '_' + tmp_end
                        # Store utc, sc_data in new file
                        file = open(flp_path.spk_source + name, 'w')
                        for i in range(0, len(utc_cut), step_spk):
                            file.write(','.join([utc_cut[i], str(sc_data_cut[i, 0]), str(sc_data_cut[i, 1]), str(sc_data_cut[i, 2]),
                                                     str(sc_data_cut[i, 3]), str(sc_data_cut[i, 4]), str(sc_data_cut[i, 5]) + '\n']))
                        # always include the last input row (but not twice)
                        if (len(utc_cut) - 1) % step_spk != 0:
                            file.write(','.join([utc_cut[-1], str(sc_data_cut[-1, 0]), str(sc_data_cut[-1, 1]), str(sc_data_cut[-1, 2]),
                                                     str(sc_data_cut[-1, 3]), str(sc_data_cut[-1, 4]), str(sc_data_cut[-1, 5]) + '\n']))
                        file.close()
                    else:
                        u = u+1
                        print(str(u) + ' files have not enough data rows')

                # append data from this file to the overall GPS data
                data_utc_sc_data_all_spk.append(data_utc_sc_data)

            if 'AYTFQU00' in header[2]:

                sc_data = np.asarray(sc_data).astype(np.float)

                if plots == 'on':
                    plot_input_data(utc, sc_data, header, figure_size)

                tmp_start = utc[0].split(".")[0].replace("-", "").replace(" ", "").replace(":", "")
                tmp_end = utc[-1].split(".")[0].replace("-", "").replace(" ", "").replace(":", "")

                name = 'TM_ck_' + tmp_start + '_' + tmp_end
                # Store utc, sc_data in new file
                file = open(flp_path.ck_source + name, 'w')
                for i in range(0, len(utc), step_ck):
                    file.write(' '.join([cspice.et2utc(cspice.utc2et(utc[i]), 'ISOC', 3),
                                         str(sc_data[i, 3]), str(sc_data[i, 0]*(-1)), str(sc_data[i, 1]*(-1)),
                                         str(sc_data[i, 2]*(-1)) + '\n']))
                # always include the last input row (but not twice)
                if (len(utc) - 1) % step_ck != 0:
                    # changes the input file to the spice quaternion style. this is not mandatory, but if you change back
                    # make sure you change the "quaternion style" argument in the msopck routine too!
                    file.write(
                        ' '.join([cspice.et2utc(cspice.utc2et(utc[-1]), 'ISOC', 3), str(sc_data[-1, 3]), str(sc_data[-1, 0]*(-1)),
                                  str(sc_data[-1, 1]*(-1)),
                                  str(sc_data[-1, 2]*(-1)) + '\n']))
                file.close()

                # append data from this file to the overall QUAT data
                data_utc_sc_data_all_ck.append(data_utc_sc_data)

    data_utc_sc_data_all_spk.sort()  # needed to be sorted for a nice timeline spk - gps data comparsion plot
    data_utc_sc_data_all_ck.sort()  # needed to be sorted for a nice timeline ck - TM data comparsion plot ##maybe does not work correctly

    print('Step 1 completed \n\n')
    cspice.unload(flp_path.lsk_file)

    return data_utc_sc_data_all_spk, data_utc_sc_data_all_ck


def plot_input_data(utc, sc_data, header, figure_size, flp_path):

    # general input to get nice labels
    cspice.furnsh(flp_path.lsk_file)
    et = cspice.str2et(utc)
    timeline = []
    style = 'YYYY-MM-DD-HR-MN-SC.######'
    time = cspice.timout(et, style, 26)
    for i in range(len(et)):
        sub = time[i].replace(".", "-").split("-")
        timeline.append(datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]),
                                   int(sub[3]), int(sub[4]), int(sub[5]), int(sub[6]), tzinfo=None))

    # check for ck/spk file (only works with the right input format)
    if 'AYTPOS00' in header[2]:

        # coordinate plot
        fig, ax = plt.subplots(figsize=(figure_size[0], figure_size[1]))
        plt.plot(timeline, sc_data[:,0], label='GPS x')
        plt.plot(timeline, sc_data[:,1], label='GPS y')
        plt.plot(timeline, sc_data[:,2], label='GPS z')
        plt.gcf().autofmt_xdate()
        ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=3)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        plt.grid()
        fig.subplots_adjust(bottom=0.28)
        plt.show()

        # coordinate magnitude plot
        vec_len = np.array([np.sqrt(np.square(sc_data[k,0]) +
                                    np.square(sc_data[k,1]) +
                                    np.square(sc_data[k,2]))
                                    for k in range(len(sc_data))])

        fig, ax = plt.subplots(figsize=(figure_size[0], figure_size[1]))
        plt.plot(timeline, vec_len, label='magnitude coordinate vector')
        plt.gcf().autofmt_xdate()
        ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=1)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        plt.grid()
        fig.subplots_adjust(bottom=0.28)
        plt.show()

    if 'AYTFQU00' in header[2]:

        # quaternion plot
        fig, ax = plt.subplots(figsize=(figure_size[0], figure_size[1]))
        plt.plot(timeline, sc_data[:, 0], label='quat x')
        plt.plot(timeline, sc_data[:, 1], label='quat y')
        plt.plot(timeline, sc_data[:, 2], label='quat z')
        plt.plot(timeline, sc_data[:, 3], label='quat q')
        plt.gcf().autofmt_xdate()
        ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=4)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        plt.grid()
        fig.subplots_adjust(bottom=0.28)
        plt.show()


def create_input_spk(poly_degree, flp_path):
    spk_master_creation(poly_degree,flp_path)
    create_spk_master_spkmerge_setup(flp_path)
    run_spkmerge_master(flp_path)

    cover = cspice.utils.support_types.SPICEDOUBLE_CELL(1000)
    cspice.spkcov(flp_path.spk_out_name, -513, cover)
    if len(cover) == 2:
        gmat = "no"
    else:
        gmat = "yes"

    return gmat


def spk_master_creation(poly_degree, flp_path):

    create_mkspk_master_setup(poly_degree,flp_path)

    # get all input data files
    source_files = []
    for file in os.listdir(flp_path.spk_source):
        if not file.endswith(".gitkeep"):
            source_files.append(file)
    print('Amount of GPS data files detected: ' + str(len(source_files)))

    for i in range(len(source_files)):

        # Create new spk files
        name_tmp = 'spk_new_' + str(i) + '.bsp'
        spk_command = 'mkspk -setup ' + flp_path.spk_master_mkspk_setup + ' -input ' + flp_path.spk_source + source_files[i] + \
                  ' -output ' + flp_path.spk_new + name_tmp + ' > /dev/null'
        subprocess.call(spk_command, shell=True)

        # name spk file related to its coverage
        spk = flp_path.spk_new + name_tmp
        sc_id = -513
        print(source_files[i])
        time_tmp = spk_window(spk,sc_id, flp_path)
        time_start = time_tmp[0].split(".")[0].replace("-", "").replace(" ", "").replace(":", "")
        time_end = time_tmp[1].split(".")[0].replace("-", "").replace(" ", "").replace(":", "")
        spk_name_new = flp_path.spk_new + 'spk_input_' + time_start + '_' + time_end + '.bsp'

        # name the newly gernerated file appropriate
        rename = 'mv ' + flp_path.spk_new + name_tmp + ' ' + spk_name_new
        subprocess.call(rename, shell=True)

    # Brief the files (doublecheck coverage) -- developing mode
    spk_brief = 'brief -t -a -utc ' + flp_path.lsk_file + ' ' + flp_path.spk_new + '*.bsp'
    subprocess.call(spk_brief, shell=True)


def create_mkspk_master_setup(poly_degree, flp_path):
    # write mkspk setup file
    file = open(flp_path.spk_master_mkspk_setup, 'w')
    text = str(
        "\\begindata\n\n"
        "INPUT_DATA_TYPE = 'STATES'" + "\n" +
        "OUTPUT_SPK_TYPE = 9" + "\n" +
        "OBJECT_ID = -513" + "\n" +
        "CENTER_ID = 399" + "\n" +
        "REF_FRAME_NAME = 'ITRF93'" + "\n" +
        "PRODUCER_ID = 'IRS'" + "\n" +
        "DATA_ORDER = 'epoch x y z vx vy vz'" + "\n" +
        "DATA_DELIMITER = ','" + "\n" +
        "LEAPSECONDS_FILE = '" + flp_path.lsk_file + "'\n" +
        "PCK_FILE = '" + flp_path.pck_normal + "'\n" +
        "COMMENT_FILE = '" + flp_path.spk_master_mkspk_comment + "'\n" +
        "INPUT_DATA_UNITS = 'DISTANCES= KM'" + "\n" +
        "LINES_PER_RECORD = 1" + "\n" +
        "POLYNOM_DEGREE = " + str(poly_degree) + "\n" +
        "SEGMENT_ID = 'SEGMENT X'" + "\n" +  # how should i name it?
        "APPEND_TO_OUTPUT = 'NO'" + "\n\n" +
        "\\begintext\n")
    file.write(text)


def spk_window(spk,sc_id, flp_path):
    cover = cspice.utils.support_types.SPICEDOUBLE_CELL(1000)
    cspice.spkcov(spk, sc_id, cover)
    intervals = cspice.wncard(cover)
    print(intervals)
    # check if it is one continuous interval or many seperated ones (in this case a
    # error message is thrown as I don't want to include not_continous intervalls
    if intervals >= 2:
        print("Attention: The spk coverage is not continous!!!")

    cspice.furnsh(flp_path.lsk_file)
    tmp_begin = cover[0]
    tmp_end = cover[-1]
    time_0 = cspice.et2utc(tmp_begin, 'ISOC', 0)
    time_1 = cspice.et2utc(tmp_end, 'ISOC', 0)
    time_tmp = [time_0, time_1]
    cspice.unload(flp_path.lsk_file)

    return time_tmp


def create_spk_master_spkmerge_setup(flp_path):

    # get a list of all new spks to merge inside "master spk 111"
    spk_source = []
    for file in os.listdir(flp_path.spk_new):
        if not file.endswith(".gitkeep"):
           spk_source.append(file)
    # NOTE: the spkmerge routine superseeds input files exactly the
    # other way around as the mk furnsh routine. That's why the for loop later on is inverted!
    # write spk_111_merge_setup_1 file
    file = open(flp_path.spk_master_merge_setup_1, 'w')

    text_1 = str(
        "LEAPSECONDS_KERNEL = " + flp_path.lsk_file + "\n" + \
        "SPK_KERNEL = " + flp_path.spk_out_name + "\n" )
    file.write(text_1)

    for i in range(len(spk_source)-1, -1, -1):
        file.write(','.join(["SOURCE_SPK_KERNEL = " + flp_path.spk_new + spk_source[i] + "\n"]))

    file.close()


def run_spkmerge_master(flp_path):

    # run the actual utility program
    run_spk_master_merge = "spkmerge " + flp_path.spk_master_merge_setup_1 + ' > /dev/null'
    subprocess.call(run_spk_master_merge, shell=True)


def prop_spk_gaps(flp_path):

    drag_area = 0.078

    print('Start gmat propagation of input spk gaps')
    # load all SPICE kernels needed for spice function usage (not the SPK file, this will be loaded individually)
    cspice.furnsh(flp_path.fk_file)
    cspice.furnsh(flp_path.pck_itrf)
    spk_111_file = flp_path.spk_out_name
    cspice.furnsh(spk_111_file)
    print('Used input spk file: ' + str(spk_111_file))

    cover = cspice.utils.support_types.SPICEDOUBLE_CELL(1000)
    cspice.spkcov(spk_111_file, -513, cover)
    time = np.array([cspice.et2utc(cover[i], 'C', 3) for i in range(len(cover))])

    print('If nothing is happening, maybe GMAT got a problem, delete the  > /dev/null in the code to debug... but wait a while before!!')

    for i in range(1,len(time)-1,2):
        # it is important to transform the (ITRF93 data) frame to the J2000Eq frame as this is the frame used for GMAT propagations!!!
        et_fix = cspice.str2et(time[i])
        duration = cspice.str2et(time[i+1]) - cspice.str2et(time[i])
        state = cspice.spkezr('-513', et_fix, 'J2000', 'NONE', 'EARTH')[0]

        # start iterations
        utc_fix_start = cspice.et2utc(et_fix, 'ISOC', 0)
        utc_fix_end = cspice.et2utc(et_fix + duration, 'ISOC', 0)
        gmat_sim_start = utc_fix_start.replace("-", "").replace(" ", "").replace(":", "")
        gmat_sim_stop = utc_fix_end.replace("-", "").replace(" ", "").replace(":", "")
        gmat_sim_spk_name = 'spk_gmat_' + gmat_sim_start + '_' + gmat_sim_stop + "_gmat.bsp"
        gmat_sim_script_name = 'spk_gmat_' + gmat_sim_start + '_' + gmat_sim_stop + ".script"

        default_script_name = flp_path.gmat_default_script
        storage_script_name = flp_path.gmat_storage + gmat_sim_script_name
        storage_spk_file = flp_path.gmat_storage + gmat_sim_spk_name
        gmat_console_path = flp_path.gmat

        # copy GMAT default_script
        copy = "cp " + default_script_name + " " + storage_script_name
        subprocess.call(copy, shell=True)

        # compute needed values for usage in GMAT script
        style = 'DD Mon YYYY HR:MN:SC.###'
        state_utc = cspice.timout(et_fix, style, 25)

        # insert state_new (and utc_fix) parameters in GMAT script
        file = open(storage_script_name)
        input = file.read()
        file.close()
        file = open(storage_script_name, 'w')
        output = re.sub(r'FLP_SC\.Epoch\s*=\s*\'Start_Time\'', "FLP_SC.Epoch = '" + state_utc + "'", input)
        output = re.sub(r'FLP_SC\.ElapsedSecs\s*=\s*\'Duration\'', "FLP_SC.ElapsedSecs = " + str(duration), output)
        output = re.sub(r'FLP_SPK\.Filename\s*=\s*\'Output_File_Path_Name\'',
                        "FLP_SPK.Filename = '" + storage_spk_file + "'", output)
        output = re.sub(r'FLP_SC\.X\s*=\s*\'X\'', "FLP_SC.X = " + str(state[0]), output)
        output = re.sub(r'FLP_SC\.Y\s*=\s*\'Y\'', "FLP_SC.Y = " + str(state[1]), output)
        output = re.sub(r'FLP_SC\.Z\s*=\s*\'Z\'', "FLP_SC.Z = " + str(state[2]), output)
        output = re.sub(r'FLP_SC\.VX\s*=\s*\'VX\'', "FLP_SC.VX = " + str(state[3]), output)
        output = re.sub(r'FLP_SC\.VY\s*=\s*\'VY\'', "FLP_SC.VY = " + str(state[4]), output)
        output = re.sub(r'FLP_SC\.VZ\s*=\s*\'VZ\'', "FLP_SC.VZ = " + str(state[5]), output)
        output = re.sub(r'FLP_SC\.DragArea\s*=\s*\'DRAG_AREA\'', "FLP_SC.DragArea = " + str(drag_area), output)
        file.write(output)
        file.close()

        # Run GMAT script
        os.chdir(gmat_console_path)
        gmat_script_command = './GmatConsole ' + storage_script_name + ' > /dev/null'
        subprocess.call(gmat_script_command, shell=True)

    cspice.unload(flp_path.fk_file)
    cspice.unload(flp_path.pck_itrf)
    cspice.unload(spk_111_file)

    create_spk_gmat_spkmerge_setup()
    run_spkmerge_gmat()


def create_spk_gmat_spkmerge_setup(flp_path):

    # get a list of all new spks to merge inside "master spk 111"
    gmat_files = []
    for file in os.listdir(flp_path.spk_new):
        if file.endswith("_gmat.bsp"):
            gmat_files.append(file)

    # NOTE: the spkmerge routine superseeds input files exactly the
    # other way around as the mk furnsh routine. That's why the for loop later on is inverted!
    # write spk_111_merge_setup_1 file
    file = open(flp_path.spk_gmat_merge_setup, 'w')

    text_1 = str(
        "LEAPSECONDS_KERNEL = " + flp_path.lsk_file + "\n" + \
        "SPK_KERNEL = " + flp_path.gmat_out_name + "\n" )
    file.write(text_1)

    for i in range(len(gmat_files)-1, -1, -1):
        file.write(','.join(["SOURCE_SPK_KERNEL = " + flp_path.spk_new + gmat_files[i] + "\n"]))

    file.close()


def run_spkmerge_gmat(flp_path):

    # run the actual utility program
    run_spk_gmat_merge = "spkmerge " + flp_path.spk_gmat_merge_setup + ' > /dev/null'
    subprocess.call(run_spk_gmat_merge, shell=True)


def plot_validation_spk(data_utc_sc_data_all_spk, gmat_prop, figure_size, timesteps_plot, flp_path, gmat):

    # iterate over all created spks
    cspice.furnsh(flp_path.lsk_file)
    spk = flp_path.spk_out_name
    cspice.furnsh(spk)

    # precalculations
    style = 'YYYY-MM-DD-HR-MN-SC.###'
    cover = cspice.utils.support_types.SPICEDOUBLE_CELL(1000)
    cspice.spkcov(spk, -513, cover)
    time_tmp = np.array([cspice.et2utc(cover[i], 'ISOC', 3) for i in range(len(cover))])
    time_start = cspice.timout(cover[0], style, 23).split(".")[0].replace("-", "")  # plot naming
    time_end = cspice.timout(cover[-1], style, 23).split(".")[0].replace("-", "")  # plot naming

    ET = []
    for i in range(0,len(time_tmp),2):
        et1 = cspice.utc2et(time_tmp[i])
        et2 = cspice.utc2et(time_tmp[i+1])
        ET_tmp = np.linspace(et1, et2, timesteps_plot)
        ET.append(ET_tmp)

    spk_data = []
    time = []
    for item in ET:
        for subitem in item:
            spk_data.append(cspice.spkezr('-513', subitem, 'ITRF93', 'NONE', 'EARTH')[0])
            time.append(cspice.timout(subitem, style, 23))
    cspice.unload(spk)

    # coordinate magnitude plot
    # timeline for ET
    timeline = []
    for i in range(len(ET)*timesteps_plot):
        sub = time[i].replace(".", "-").split("-")
        timeline.append(datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]),
                                              int(sub[3]), int(sub[4]), int(sub[5]), int(sub[6]), tzinfo=None))
    # timeline for ET_2
    ET_2 = []
    for item in data_utc_sc_data_all_spk:
        for subitem in item:
            ET_2.append(cspice.utc2et(subitem[0]))

    timeline_2 = []
    time_2 = cspice.timout(ET_2, style, 23)

    for i in range(len(ET_2)):
        sub_2 = time_2[i].replace(".", "-").split("-")
        timeline_2.append(datetime.datetime(int(sub_2[0]), int(sub_2[1]), int(sub_2[2]),
                                              int(sub_2[3]), int(sub_2[4]), int(sub_2[5]), int(sub_2[6]), tzinfo=None))


    if gmat_prop == 'on' and gmat == "yes":
        # timeline for combined view (spk TM and spk prop for TM gaps)
        spk_2 = flp_path.gmat_out_name
        ET_3 = np.linspace(cover[0], cover[-1], timesteps_plot)
        timeline_3 = []
        time_3 = cspice.timout(ET_3, style, 23)
        for i in range(len(ET_3)):
            sub_3 = time_3[i].replace(".", "-").split("-")
            timeline_3.append(datetime.datetime(int(sub_3[0]), int(sub_3[1]), int(sub_3[2]),
                                                int(sub_3[3]), int(sub_3[4]), int(sub_3[5]), int(sub_3[6]), tzinfo=None))

        cspice.furnsh(spk_2)
        cspice.furnsh(spk)
        cspice.furnsh(flp_path.pck_itrf)
        spk_data_3 = np.array([cspice.spkezr('-513', ET_3[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET_3))])
        cspice.unload(flp_path.pck_itrf)
        cspice.unload(spk_2)
        cspice.unload(spk)

        vec_len_3 = np.array([np.sqrt(np.square(spk_data_3[k][0]) + np.square(spk_data_3[k][1]) + np.square(spk_data_3[k][2]))
                              for k in range(len(spk_data_3))])


    vec_len_1 = np.array([np.sqrt(np.square(spk_data[k][0]) + np.square(spk_data[k][1]) + np.square(spk_data[k][2]))
                              for k in range(len(spk_data))])

    vec_len_2 = []
    x = []
    y = []
    z = []
    for item in data_utc_sc_data_all_spk:
        for subitem in item:
            # subitem[0] = utc; subitem[1] = sc clock time
            x_tmp = float(subitem[2]) / 1000
            y_tmp = float(subitem[3]) / 1000
            z_tmp = float(subitem[4]) / 1000
            vec_len_2.append(np.sqrt(np.square(x_tmp) + np.square(y_tmp) + np.square(z_tmp)))
            x.append(x_tmp)
            y.append(y_tmp)
            z.append(z_tmp)

    fig, ax = plt.subplots(figsize=(figure_size[0], figure_size[1]))
    ax.set_ylabel('distance [km]', fontsize=18)
    if gmat_prop == 'on' and gmat == "yes":
        plt.plot(timeline_3, spk_data_3[:,0], label='x spk gmat', color='red')
        plt.plot(timeline_3, spk_data_3[:,1], label='y spk gmat', color='blue')
        plt.plot(timeline_3, spk_data_3[:,2], label='z spk gmat', color='green')
    else:
        spk_data = np.asarray(spk_data).astype(np.float)
        plt.plot(timeline, spk_data[:,0], label='x spk', color='red')
        plt.plot(timeline, spk_data[:,1], label='y spk', color='blue')
        plt.plot(timeline, spk_data[:,2], label='z spk', color='green')

    plt.plot(timeline_2, x, label='x TM', linestyle='--', color='red')
    plt.plot(timeline_2, y, label='y TM', linestyle='--', color='blue')
    plt.plot(timeline_2, z, label='z TM', linestyle='--', color='green')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=6)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.28)
    fig.savefig(flp_path.plots + 'validation_spk_coordinates_' + time_start + '_' + time_end + '.png', dpi=fig.dpi)
    plt.show()

    fig, ax = plt.subplots(figsize=(figure_size[0], figure_size[1]))
    ax.set_ylabel('distance [km]', fontsize=18)
    if gmat_prop == 'on' and gmat == "yes":
        plt.plot(timeline_3, vec_len_3, label='propagation', color='green')
    plt.plot(timeline_2, vec_len_2, label='input data', color='black', marker='x')
    plt.plot(timeline, vec_len_1, label='spk input data', color='brown', marker='o')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=3)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.28)
    fig.savefig(flp_path.plots + 'validation_spk_magnitude_' + time_start + '_' + time_end + '.png', dpi=fig.dpi)
    plt.show()


def plot_spk_dif(data_utc_sc_data_all_spk, figure_size, flp_path, gmat_prop, gmat):
    # load the needed kernels
    cspice.furnsh(flp_path.lsk_file)
    cspice.furnsh(flp_path.pck_itrf)
    spk = flp_path.spk_out_name
    cspice.furnsh(spk)
    if gmat_prop == 'on' and gmat == "yes":
        spk_2 = flp_path.gmat_out_name
        cspice.furnsh(spk_2)
    # get nice timeline for plot
    ET = []
    style = 'YYYY-MM-DD-HR-MN-SC.###'
    for item in data_utc_sc_data_all_spk:
        for subitem in item:
            et_tmp = cspice.utc2et(subitem[0])
            ET.append(et_tmp)
    ET = ET[1:-1]  # needed to align with produced SPK file
    timeline = []
    time = cspice.timout(ET, style, 23)
    for i in range(len(ET)):
        sub_2 = time[i].replace(".", "-").split("-")
        timeline.append(datetime.datetime(int(sub_2[0]), int(sub_2[1]), int(sub_2[2]),
                                            int(sub_2[3]), int(sub_2[4]), int(sub_2[5]), int(sub_2[6]), tzinfo=None))
    # position of the FLP SC according to GPS:
    x = []
    y = []
    z = []
    for item in data_utc_sc_data_all_spk:
        for subitem in item:
            # subitem[0] = utc; subitem[1] = sc clock time
            x.append(float(subitem[2]) / 1000)
            y.append(float(subitem[3]) / 1000)
            z.append(float(subitem[4]) / 1000)
    x = x[1:-1]  # because of index shift for ET!
    y = y[1:-1]
    z = z[1:-1]
    # position of the FLP SC according to the SPK kernels produced:
    spk_data = np.array([cspice.spkezr('-513', ET[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET))])
    # difference between both positions:
    vec_diff_x = np.array([x[i] - spk_data[i, 0] for i in range(len(ET))])
    vec_diff_y = np.array([y[i] - spk_data[i, 1] for i in range(len(ET))])
    vec_diff_z = np.array([z[i] - spk_data[i, 2] for i in range(len(ET))])

    # plot to check SPK against input GPS at GPS data point times.
    fig, ax = plt.subplots(figsize=(figure_size[0], figure_size[1]))
    ax.set_ylabel('difference [km]', fontsize=18)
    plt.plot(timeline, vec_diff_x, label='x', color='green')
    plt.plot(timeline, vec_diff_y, label='y', color='black')
    plt.plot(timeline, vec_diff_z, label='z', color='brown')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=3)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.28)
    plt.show()

    # check SPK kernel with only GPS data points times (and linearly fit) with same SPK kernel with more
    # data points
    # new timeline

    if ET[-1] - ET[0] < 30000:
        points = ET[-1] - ET[0]
    else:
        points = 30000
    ET_2 = np.linspace(ET[0],ET[-1],points)
    timeline_2 = []
    time_2 = cspice.timout(ET_2, style, 23)
    for i in range(0,len(ET_2)-1):
        sub_2 = time_2[i].replace(".", "-").split("-")
        timeline_2.append(datetime.datetime(int(sub_2[0]), int(sub_2[1]), int(sub_2[2]),
                                          int(sub_2[3]), int(sub_2[4]), int(sub_2[5]), int(sub_2[6]), tzinfo=None))
    # coordinate interpolation of GPS data
    x_inter = []
    y_inter = []
    z_inter = []
    m = 0
    for i in range(len(ET_2)):
        for k in range(m,len(ET)-1):
            if ET[k] <= ET_2[i] and ET[k+1] > ET_2[i]:
            # this leads to huge differences if ET is not sampled regularly.
            # this is the reason the gmat propagation is off by 1000th of kilometers.
            # the "best" way to check the precision of the gmat propagation is its last value always shwon in the plot
            # before. (more or less by accident :D)
                # value from closest time before ET_2[i] + difference of x position * time delta / time delta GPS
                x_inter_tmp = x[k] + (x[k+1]-x[k])*((ET_2[i]-ET[k])/(ET[k+1]-ET[k]))
                y_inter_tmp = y[k] + (y[k + 1] - y[k]) * ((ET_2[i]-ET[k]) / (ET[k + 1] - ET[k]))
                z_inter_tmp = z[k] + (z[k + 1] - z[k]) * ((ET_2[i]-ET[k]) / (ET[k + 1] - ET[k]))
                if x_inter == []:
                    x_inter.append(x_inter_tmp)
                    y_inter.append(y_inter_tmp)
                    z_inter.append(z_inter_tmp)
                if x_inter[-1] != x_inter_tmp:
                    x_inter.append(x_inter_tmp)
                    y_inter.append(y_inter_tmp)
                    z_inter.append(z_inter_tmp)
                break
        m = k

    # position of FLP SC using SPK kernels
    spk_data_2 = np.array([cspice.spkezr('-513', ET_2[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET_2))])
    # difference of the new positions
    vec_diff_x_2 = np.array([x_inter[i] - spk_data_2[i, 0] for i in range(len(x_inter))])
    vec_diff_y_2 = np.array([y_inter[i] - spk_data_2[i, 1] for i in range(len(x_inter))])
    vec_diff_z_2 = np.array([z_inter[i] - spk_data_2[i, 2] for i in range(len(x_inter))])

    # get the absolut from the values and the sum of all absolut values
    vec_abs_diff_x = np.array([np.abs(vec_diff_x_2[i]) for i in range(len(vec_diff_x_2))])
    vec_abs_diff_y = np.array([np.abs(vec_diff_y_2[i]) for i in range(len(vec_diff_y_2))])
    vec_abs_diff_z = np.array([np.abs(vec_diff_z_2[i]) for i in range(len(vec_diff_z_2))])
    vec_abs_diff = np.array([vec_abs_diff_x[i] + vec_abs_diff_y[i] + vec_abs_diff_z[i] for i in range(len(vec_diff_x_2))])

    fig, ax = plt.subplots(figsize=(figure_size[0], figure_size[1]))
    ax.set_ylabel('difference [km]', fontsize=18)
    plt.plot(timeline_2, vec_diff_x_2, label='x', color='green')
    plt.plot(timeline_2, vec_diff_y_2, label='y', color='black')
    plt.plot(timeline_2, vec_diff_z_2, label='z', color='brown')
    plt.plot(timeline_2, vec_abs_diff, label='abs', color='red', linestyle='--')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=4)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.28)
    plt.show()


def create_input_ck(max_interval, flp_path):
    run_msopck(max_interval,flp_path)

    # get the ck_master coverage
    cspice.furnsh(flp_path.ck_out_name)
    cspice.furnsh(flp_path.sclk_file)
    cover = cspice.ckcov(flp_path.ck_out_name, -513000, False, 'INTERVAL', 0.0, 'TDB')
    time_tmp = np.array([cspice.et2utc(cover[i], 'C', 3) for i in range(len(cover))])
    print("\n CK coverage intervals: " + str(int(len(cover)/2)) + "\n")
    for i in range(0, len(time_tmp) - 1, 2):
        print(time_tmp[i] + " - " + time_tmp[i + 1])

    # Brief the files (doublecheck coverage) -- developing mode
    #ck_brief = 'ckbrief -t -a -utc ' + flp_path.lsk_file + ' ' + flp_path.sclk_file + ' ' + flp_path.ck_out + '*.bc'
    #subprocess.call(ck_brief, shell=True)


def run_msopck(max_interval, flp_path):

    # No deletion of master ck_111 file on purpose!

    # Create the script needed for the prediCkt utility program
    create_ck_master_setup(max_interval,flp_path)
    ck_source = []

    for file in os.listdir(flp_path.ck_source):
        if not file.endswith(".gitkeep"):
           ck_source.append(file)

    if ck_source != []:
        for i in range(len(ck_source)):
            # Create new ck file
            ck_command = 'msopck ' + flp_path.ck_master_setup + ' ' + flp_path.ck_source + ck_source[i] + \
                         ' ' + flp_path.ck_out_name + ' > /dev/null'
            subprocess.call(ck_command, shell=True)
    else:
        print('\n\nNo attitude input data provided!')


def create_ck_master_setup(max_interval, flp_path):

    file = open(flp_path.ck_master_setup, 'w')

    # write flp.furnsh file
    text = str(
        "\\begindata\n\n" + \
 \
        "LSK_FILE_NAME           = '" + flp_path.lsk_file + "'\n" + \
        "SCLK_FILE_NAME           = '" + flp_path.sclk_file + "'\n" + \
        "COMMENTS_FILE_NAME           = '" + flp_path.ck_master_comments + "'\n" + \
        "FRAMES_FILE_NAME           = '" + flp_path.fk_file + "'\n" + \
        "INTERNAL_FILE_NAME      = 'NAME 1'\n" + \
        "CK_TYPE                 = 3\n" + \
        "CK_SEGMENT_ID           = 'SEGMENT NAME 1'\n" + \
        "INSTRUMENT_ID           = -513000\n" + \
        "REFERENCE_FRAME_NAME    = 'J2000'\n" + \
        "ANGULAR_RATE_PRESENT    = 'MAKE UP'\n" + \
        "INPUT_DATA_TYPE         = 'SPICE QUATERNIONS'\n" + \
        "INPUT_TIME_TYPE         = 'UTC'\n" + \
        "MAXIMUM_VALID_INTERVAL  = " + str(max_interval) + "\n" + \
        "QUATERNION_NORM_ERROR   = 1.0E-3\n" + \
        "PRODUCER_ID             = 'IRS'\n" + \
        "DOWN_SAMPLE_TOLERANCE   = 0.001\n\n" + \
 \
        "\\begintext\n\n"
    )
    file.write(text)
    file.close()


def plot_validation_ck(data_utc_sc_data_all_ck, figure_size, timesteps_plot, flp_path):

    # iterate over all created spks
    cspice.furnsh(flp_path.lsk_file)
    cspice.furnsh(flp_path.sclk_file)
    ck = flp_path.ck_out_name
    cspice.furnsh(ck)

    # precalculations
    style = 'YYYY-MM-DD-HR-MN-SC.###'
    cover = cspice.ckcov(flp_path.ck_out_name, -513000, False, 'INTERVAL', 0.0, 'TDB')
    time_tmp = np.array([cspice.et2utc(cover[i], 'ISOC', 0) for i in range(len(cover))])
    time_start = cspice.timout(cover[0], style, 26).split(".")[0].replace("-", "")  # plot naming
    time_end = cspice.timout(cover[-1], style, 26).split(".")[0].replace("-", "")  # plot naming

    ET = []
    for i in range(0,len(time_tmp),2):
        if cover[i] != cover[i+1]:  # needed to not fail the ckgp routine later on ...
            et1 = cspice.utc2et(time_tmp[i])+1  # a small margin towards the ends is needed... (spice routine)
            et2 = cspice.utc2et(time_tmp[i+1])-1  # a small margin towards the ends is needed... (spice routine)
            ET_tmp = np.linspace(et1, et2, timesteps_plot)
            ET.append(ET_tmp)

    ck_data = []
    time = []
    for item in ET:
        for subitem in item:
            matrix, t= cspice.ckgp(-513000, cspice.sce2c(-513,subitem), 0, 'J2000')
            ck_data.append(cspice.m2q(matrix))
            time.append(cspice.timout(subitem, style, 23))


    ck_data_0 = []
    ck_data_1 = []
    ck_data_2 = []
    ck_data_3 = []
    for item in ck_data:
        ck_data_0.append(item[0])
        ck_data_1.append(item[1])
        ck_data_2.append(item[2])
        ck_data_3.append(item[3])

    # coordinate magnitude plot
    # timeline for ET
    timeline = []
    for i in range(len(ET)*timesteps_plot):
        sub = time[i].replace(".", "-").split("-")
        timeline.append(datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]),
                                              int(sub[3]), int(sub[4]), int(sub[5]), int(sub[6]), tzinfo=None))

    # timeline for ET_2
    ET_2 = []
    data_0 = []
    data_1 = []
    data_2 = []
    data_3 = []
    for item in data_utc_sc_data_all_ck:
        for subitem in item:
            ET_2.append(cspice.utc2et(subitem[0]))
            data_0.append(subitem[2])
            data_1.append(subitem[3])
            data_2.append(subitem[4])
            data_3.append(subitem[5])
    data_0 = np.asarray(data_0).astype(np.float)
    data_1 = np.asarray(data_1).astype(np.float)
    data_2 = np.asarray(data_2).astype(np.float)
    data_3 = np.asarray(data_3).astype(np.float)

    timeline_2 = []
    time_2 = cspice.timout(ET_2, style, 23)
    for i in range(len(ET_2)):
        sub_2 = time_2[i].replace(".", "-").split("-")
        timeline_2.append(datetime.datetime(int(sub_2[0]), int(sub_2[1]), int(sub_2[2]),
                                              int(sub_2[3]), int(sub_2[4]), int(sub_2[5]), int(sub_2[6]), tzinfo=None))


    fig, ax = plt.subplots(figsize=(figure_size[0], figure_size[1]))
    ax.set_ylabel('value [-]', fontsize=18)
    plt.plot(timeline, ck_data_0, label='ck quat q', color='red')
    plt.plot(timeline, ck_data_1, label='ck quat x', color='blue')
    plt.plot(timeline, ck_data_2, label='ck quat y', color='green')
    plt.plot(timeline, ck_data_3, label='ck quat z', color='brown')
    plt.plot(timeline_2, data_3[:], label='quat q', color='red', linestyle='--')
    plt.plot(timeline_2, data_0, label='quat x', color='blue', linestyle='--')
    plt.plot(timeline_2, data_1, label='quat y', color='green', linestyle='--')
    plt.plot(timeline_2, data_2, label='quat z', color='brown', linestyle='--')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=4)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.35)
    fig.savefig(flp_path.plots + 'validation_ck_quaternions_' + time_start + '_' + time_end + '.png', dpi=fig.dpi)
    plt.show()

    cspice.unload(flp_path.sclk_file)
    cspice.unload(ck)


def delete_inputs(flp_path):

    remove_old_spk_source = "rm -f " + flp_path.spk_source + "*"  # delete kernels/spk/source/ files...
    subprocess.call(remove_old_spk_source, shell=True)

    remove_old_spk_new = "rm -f " + flp_path.spk_new + "*"  # delete kernels/spk/new/ files...
    subprocess.call(remove_old_spk_new, shell=True)

    remove_old_spk_out = "rm -f " + flp_path.spk_out + "*"  # delete kernels/spk/out/ files...
    subprocess.call(remove_old_spk_out, shell=True)

    remove_old_gmat_out = "rm -f " + flp_path.gmat_out + "*"  # delete kernels/spk/gmat/ files...
    subprocess.call(remove_old_gmat_out, shell=True)

    remove_old_ck_source = "rm -f " + flp_path.ck_source + "*"  # delete kernels/ck/source/ files...
    subprocess.call(remove_old_ck_source, shell=True)

    remove_old_ck_out = "rm -f " + flp_path.ck_out + "*"  # delete kernels/ck/out/ files...
    subprocess.call(remove_old_ck_out, shell=True)

    remove_prop_ck = "rm -f " + flp_path.ck_prop + "*"  # delete kernels/ck/propagation/ files...
    subprocess.call(remove_prop_ck, shell=True)

    remove_prop_spk = "rm -f " + flp_path.spk_prop + "*"  # delete kernels/spk/propagation/ files...
    subprocess.call(remove_prop_spk, shell=True)


spk_ck_creation()
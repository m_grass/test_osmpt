'''
This script is designed as an easy interface to load observations into cosmographia. It could be used to check OSIRIS
pointings to the ground station as well as pictures downlinked. Note that this is the 3D interactive version of the
FOV_basemap script.
With this script it is possible to also show limb observations.
Note tha:
- the end time cannot be the start time.
- if the fill paremeter is set to true, it might show not exactly the observered area as observing it a second time
change it to "unobserverd" again.
'''


import os
import subprocess
import json
import spiceypy as cspice
from collections import OrderedDict
from h.path_setup import flp_path
from h.programs.functions import cosmographia_views


# ---------- user input ------------------
sensor = 'FLP_PAMCAM'
start_time = "2018-127T21:54:22"
end_time = "2018-127T21:54:22"
obs_rate = 0 # if set to 0 it will be a continuous observation
fill = False  # fill the area with is observed

exclude_old_obs = 'on'  # set to on if only the newest observation is of interest for you
exclude_stations = 'off'  # should the stations be shown?
exclude_targets = 'off'  # should the targests be shown?

cosmo_sensor = 'on'  # load a cosmographia view centered in the FLP sensor.
cosmo_earth = 'off'  # load a cosmographia view looking down to the earth north pole.

# -----------------------------

# get the right time format
cspice.furnsh(flp_path.lsk_file)
style = 'YYYY-MM-DD HR:MN:SC.###'
start = cspice.timout(cspice.str2et(start_time),style)
end = cspice.timout(cspice.str2et(end_time),style)
cspice.unload(flp_path.lsk_file)
name_time = start_time.split(".")[0].replace("-", "_").replace(":", "")
observation_name = sensor + '_' + name_time


def move_new_to_old():
    file = os.listdir(flp_path.json_obs_new)
    # there should be only 1 file
    if file != []:
        move_json_obs = 'mv ' + flp_path.json_obs_new + file[0] + ' ' + flp_path.json_obs_old + file[0]
        subprocess.call(move_json_obs, shell=True)


def create_json_observation(observation_name, sensor, start, end, obs_rate):

    footprint_color = [1, 0.5, 0]

    groups = OrderedDict([('startTime', start + ' UTC'),
                            ('endTime', end + ' UTC'),
                            ('obsRate', obs_rate)])
    geometry = OrderedDict([('type', 'Observations'),
                            ('sensor', sensor),
                            ('groups', [groups]),
                            ('footprintColor', footprint_color),
                            ('footprintOpacity', 0.4),
                            ('showResWithColor', True),
                            ('sideDivisions', 125),
                            ('alongTrackDivisions', 500),
                            ('shadowVolumeScaleFactor', 1.75),
                            ('fillInObservations', fill)])
    bodyFrame = OrderedDict([('type', 'BodyFixed'),
                             ('body', "Earth")])
    trajectoryFrame = OrderedDict([('type', 'BodyFixed'),
                                   ('body', "Earth")])
    items = OrderedDict([('class', "observation"),
                         ('name', observation_name),
                         ('center', "Earth"),
                         ('trajectoryFrame', trajectoryFrame),
                         ('bodyFrame', bodyFrame),
                         ('geometry', geometry)])
    json_data = OrderedDict([('version', '1.0'),
                             ('name', observation_name),
                             ('items', [items])])

    json_path = flp_path.json_obs_new + observation_name + ".json"
    with open(json_path, 'w') as outfile:
        json.dump(json_data, outfile, indent=3)


def update_scenario_json(exclude_targets, exclude_stations, exclude_old_obs):

    items = [
        "./metakernels.json",
        "./spacecraft_FLP.json",
        "./sensors/FLP_PAMCAM.json",
        "./sensors/FLP_OSIRIS.json"
    ]

    if exclude_stations != 'on':
        items.append("./stations.json")

    if exclude_targets != 'on':
        target_item_new = os.listdir(flp_path.json_target_new)
        items.append("./targets/new/" + target_item_new[0])
        target_items_old = os.listdir(flp_path.json_target_old)
        for i in range(len(target_items_old)):
            items.append("./targets/old/" + target_items_old[i])

    if exclude_old_obs != 'on':
        obs_items_old = os.listdir(flp_path.json_obs_old)
        for i in range(len(obs_items_old)):
            items.append("./observations/old/" + obs_items_old[i])

    obs_item_new = os.listdir(flp_path.json_obs_new)
    items.append("./observations/new/" + obs_item_new[0])  # there should only be 1 new item

    json_data = OrderedDict([('version', '1.0'),
                        ('name', 'load FLP'),
                        ('require',
                            items
                        )])

    json_path = flp_path.json_scenario
    with open(json_path, 'w') as outfile:
        json.dump(json_data, outfile, indent=3)


move_new_to_old()
create_json_observation(observation_name, sensor, start, end, obs_rate)
update_scenario_json(exclude_targets, exclude_stations, exclude_old_obs)
if cosmo_sensor == 'on':
    cosmographia_views.cosmo_sensor(sensor, exclude_targets, exclude_stations, start)
if cosmo_earth == 'on':
    cosmographia_views.cosmo_earth(end)


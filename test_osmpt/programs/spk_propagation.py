'''
This script should be used to propagate orbits in a timeframe up to 40 days. To achieve the best prediction currently
possible, the following steps needs to be done:

1. Place a csv file wiht at least 10 hours of continuous GPS TM data into the TM directory.
2. Run the spk_ck_creation script to create a SPK kernel for the corresponding time.
Note that the gmat_prop parameter should be set to "on" and the SPK_dif_plot should be activated to check that the GPS
and SPK data have good quality.
3. Run this script the first time. The following key:value parameter should be used:
        - utc_fix: short after the start time of the input file
        - utc_test: time window almost covering the whole input time window
        - utc_prop: time window larger then time window defined for utc_test
        - max_iter: 1 ; minimal value to speed up the process.
        - error_window: 'off' ; we want to see the whole utc_test period in the plots
        - del_i: is not of interest as it will not be reached before max_iter is reached. (leave it at 50)
        - step: 0.000003 ; this value was found to be a good value for the iteration later on. (so just don't change it)
During this step we are only interested in the output (plots) to adjust the time window and to validate that the input
data is valid to be used.
Plot 1: Both x-positions should be overlaying each other
Plot 2: Most important plot: I. check if the utc_fix time is at a convenient position (doesnt seem strange...)
                            II. spot a time window (if possible between 30-120 min) with good data sampling (you trust)
                           III. change the utc_test window to the spotted time window; change utc_fix if needed...
Note: if you see a plot with a "red wave" diverging (frequency roughly 90 min) change the input time slightly (10 sec
might be enough) and repeat step 3.
Plot 4: Not of interest but should show two lines roughly with the same shape.
4. Run this script for a second time. Besides the changes of the utc_test and utc_fix parameter, you should change:
    - max_iter: 1000
    - del_i: 50
    - step: 0.000003
    - utc_prop: use a time which lays around 1 month in the future (the time you need to propagate for mission planning
    reasons)
Note that these values were found through testing. Changing them could have a huge impact on the propagation result.
Plot 2: Should now show a plot with lines between [-1, 1]... if the are above those values, do not trust the propagation.
Plot 4: Both lines should be way more similar to each other as without the iteration process.
5. Hope, that you predicted right :D
Note that the result should be a propagation with an error of less than 20 km for a period of 1 month. Some predictions
achieved values of less then 2 km for all times within this period.

Note that this script was used for more specific testing as multiple input windows and multiple iterations with
different step sizes etc. but is reduced to its basic version as more complex versions lead to no significant
improvements but even less unpredicted behaviour. Please check out the documentation of this script for further details
in terms of "what to believe in" as an good output propagation.
Note that a drag area used in the underlying GMAT script of around 0.08 was found to fit most propagations. Do not
change that value too much even if it seems to be unrealistic.

Possible error debugging:
- check if your input file does not has TM gaps
- check if the input time window includes all times defined in this script
- only one data file should be supplied to make sure no overlapping data is provided...
'''


import os
import subprocess
import csv
import random
import re
import datetime
import matplotlib.pyplot as plt
import numpy as np
import spiceypy as cspice
from h.path_setup import flp_path

# initial start conditions
utc_fix = '2018 MAY 08 12:00:00'

# Get utc time for FLP GPS states to test against (for epsilon calculation)
utc_test = [
    '2018 MAY 09 08:00:00', '2018 MAY 09 10:00:00',
]
# SPK propagation timeframe:
utc_prop = [utc_fix, '2018 JUN 10 23:00:00']
# maximal iterations until abort
max_iter = [50]
error_window = 'off'

# condition to break the for loop before max_iter is reached (steps needed to find a better solution)
del_i = [30]
# step size; [km] for position; [m/s] for velocity (so the values are in the same order of magnitude)
step = [0.000003]
# drag area of the FLP SC for the JacchiaRObersts atmosphere model. Even though this value seems unrealistic it leads
# to good propagation results.
drag_area = 0.078


def manage_spk_data_input():

    # Get all TM input files
    print('Searching for input files ...')
    files_tmp = []
    for file in os.listdir(flp_path.TM_path):
        if file.endswith(".csv"):
            files_tmp.append(file)
    marker = '~'
    files = []
    for i in range(len(files_tmp)):
        if marker not in files_tmp[i]:
            files.append(files_tmp[i])
    print('Amount of input files detected: ' + str(len(files)))

    # Create new spk and ck source files
    cspice.furnsh(flp_path.lsk_file)  # needed to convert between times using cspice functions

    print('Started TM import ...')
    input = []
    for i in range(len(files)):
        with open(flp_path.TM_path + files[i]) as file:

            # read file content
            reader = csv.reader(file, delimiter=';')
            rows = [r for r in reader]
            print('File ' + str(i+1) + ' (' + files[i] + ') has ' + str(len(rows)) + ' rows')
            header = rows[0]
            print('Computations ongoing ...')
            input_data = rows[1:]

            # check for default lines and store data in lists
            utc = []
            sc_data = []
            data_utc_sc_data = []
            cspice.furnsh(flp_path.lsk_file)
            for k in range(len(input_data)):
                if input_data[k][2] != '0':
                    utc.append(input_data[k][0])
                    sc_data.append(input_data[k][2:])
                    data_utc_sc_data.append(input_data[k])

            if 'AYTPOS00' in header[2]:
                # append data from this file to the overall GPS data
                input.append(data_utc_sc_data)

    input.sort()  # needed to be sorted for a nice timeline spk - gps data comparsion plot
    cspice.unload(flp_path.lsk_file)
    print('Step 1 completed \n\n')
    return input


def create_gmat_spk(input, utc_fix, utc_test, max_iter, del_i, step, utc_prop, error_window):
    # load all SPICE kernels needed for spice function usage (not the SPK file, this will be loaded individually)
    cspice.furnsh(flp_path.lsk_file)
    cspice.furnsh(flp_path.fk_file)
    cspice.furnsh(flp_path.pck_itrf)
    # if there is no continuous GPS data available this routine will fail!
    # this is due to the fact that using GMAT propagated gaps might be not precise enough and would yield to an even
    # more unpredictable propgagation.
    cspice.furnsh(flp_path.spk_out_name)

    eps = [100000000, 100000000, 100000000]  # set a high epsilon as start error
    et_fix = cspice.str2et(utc_fix)
    et_test = []
    for i in range(len(utc_test)):
        et_test.append(cspice.str2et(utc_test[i]))

    ET_input = []
    for item in input:
        for subitem in item:
            et_tmp = cspice.utc2et(subitem[0])
            ET_input.append(et_tmp)

    cut = []
    for i in range(len(et_test)):  # iterate through all utc test windows
        for k in range(0,len(ET_input)-1):
            if ET_input[k] <= et_test[i] and ET_input[k+1] > et_test[i]:
                if cut == []:
                    cut.append(k + 1)
                    break
                if cut[-1] != k + 1:
                    cut.append(k + 1)
                    break

    if len(cut)%2 != 0:
        print('Please check your input. It needs to be at least 10 hour of continous data.')
        quit()

    # get the test times within the utc_test windows for which GPS data was available:
    ET= []
    for i in range(int(np.floor(len(cut)/2))):
        et = []
        for k in range(cut[2*i],cut[2*i+1]):
            et.append(ET_input[k])
        ET.append(et)

    # it is important to transform the (ITRF93 data) frame to the J2000Eq frame as this is the frame used for GMAT propagations!!!
    state = cspice.spkezr('-513', et_fix, 'J2000', 'NONE', 'EARTH')[0]

    progress_storage_list = []

    # start iterations
    print('If nothing is happening, maybe GMAT got a proplem, delete the  > /dev/null in the code to debug...')

    for loop in range(len(step)):
        # GMAT script calculation duration: # the 100[s] are used as buffer
        duration = np.max(ET[loop]) - et_fix + 100
        progress, eps, gmat_sim_spk_name, state = comp_state(duration, max_iter[loop], del_i[loop], ET[loop],
                                                                  state, utc_fix, step[loop], eps, loop)
        progress_storage_list.append(progress)
        #eps = [i * 1000 for i in eps] # because a longer timespan is simulated (and the points to check are getting more
        print(loop)
        # (because i set it like that)...which increases normally the difference error

    # remove all GMAT created test files...
    remove = "rm " + flp_path.gmat_storage_future + "*"
    subprocess.call(remove, shell=True)

    print('DATA review: \n')
    for item in progress_storage_list:
        for subitem in item:
            print(subitem)

    # compute the long term GMAT SPK:
    gmat_SPK_long_term(utc_prop, progress_storage_list[-1][-1])  # progress should have stored the latest (and therefore best) SC state

    # show the results of the best fit
    plot_gmat_comparison(flp_path.spk_prop_name, ET, error_window)


def comp_state(duration, max_iter, del_i, et_test, state_fix, utc_fix, step, eps, loop):
    # Values for for loop
    i_old = 0
    progress = []
    check_points = len(et_test)
    print(check_points)

    # iterate over start conditions to get the "true" values
    for i in range(max_iter):

        state_new = [state_fix[k] + step * np.tan(np.pi * random.uniform(0, 1)) for k in range(len(state_fix))]

        storage_spk_file, gmat_sim_spk_name = gmat_propagator(utc_fix, state_new, duration, i, loop)

        eps_new = error_calculator(et_test, storage_spk_file)

        print("[" + str(i) + "/" + str(max_iter) + "]")

        if sum(eps_new) <= sum(eps):
            state_fix = state_new
            print([i, i_old, state_fix, sum(eps_new)/check_points])
            eps = eps_new
            # progress.append([i, i_old[p], p, state_new, eps_new[p]/check_points])  #-> the state the error is calculated for
            progress.append([i, i_old, state_fix, sum(eps_new)/check_points])  # -> the state used as input for next iteration
            if i - i_old <= del_i:
                i_old = i

        if i - i_old > del_i:
            print("got it" + str(i) , str(i_old))
            break

    return progress, eps, gmat_sim_spk_name, state_fix


def gmat_propagator(utc_fix, state_new, duration, i, loop):
    # file locations and names
    utc_fix_start = cspice.et2utc(cspice.str2et(utc_fix), 'ISOC', 0)
    utc_fix_end = cspice.et2utc(cspice.str2et(utc_fix) + duration, 'ISOC', 0)
    gmat_sim_start = utc_fix_start.replace("-", "").replace(" ", "").replace(":", "")
    gmat_sim_stop = utc_fix_end.replace("-", "").replace(" ", "").replace(":", "")
    gmat_sim_spk_name = 'spk_prop_' + gmat_sim_start + '_' + gmat_sim_stop + "_v" + str(loop) + "_t" + str(i) + ".bsp"
    gmat_sim_script_name = 'spk_prop_' + gmat_sim_start + '_' + gmat_sim_stop + "_v" + str(loop) + "_t" + str(i) + ".script"

    default_script_name = flp_path.gmat_default_script
    storage_script_name = flp_path.gmat_storage_future + gmat_sim_script_name
    storage_spk_file = flp_path.gmat_storage_future + gmat_sim_spk_name
    gmat_console_path = flp_path.gmat

    # copy GMAT default_script
    copy = "cp " + default_script_name + " " + storage_script_name
    subprocess.call(copy, shell=True)

    # compute needed values for usage in GMAT script
    style = 'DD Mon YYYY HR:MN:SC.###'
    et_fix = cspice.str2et(utc_fix)
    state_utc = cspice.timout(et_fix, style, 25)

    # insert state_new (and utc_fix) parameters in GMAT script
    file = open(storage_script_name)
    input = file.read()
    file.close()
    file = open(storage_script_name, 'w')
    output = re.sub(r'FLP_SC\.Epoch\s*=\s*\'Start_Time\'', "FLP_SC.Epoch = '" + state_utc + "'", input)
    output = re.sub(r'FLP_SC\.ElapsedSecs\s*=\s*\'Duration\'', "FLP_SC.ElapsedSecs = " + str(duration), output)
    output = re.sub(r'FLP_SPK\.Filename\s*=\s*\'Output_File_Path_Name\'',
                    "FLP_SPK.Filename = '" + storage_spk_file + "'", output)
    output = re.sub(r'FLP_SC\.X\s*=\s*\'X\'', "FLP_SC.X = " + str(state_new[0]), output)
    output = re.sub(r'FLP_SC\.Y\s*=\s*\'Y\'', "FLP_SC.Y = " + str(state_new[1]), output)
    output = re.sub(r'FLP_SC\.Z\s*=\s*\'Z\'', "FLP_SC.Z = " + str(state_new[2]), output)
    output = re.sub(r'FLP_SC\.VX\s*=\s*\'VX\'', "FLP_SC.VX = " + str(state_new[3]), output)
    output = re.sub(r'FLP_SC\.VY\s*=\s*\'VY\'', "FLP_SC.VY = " + str(state_new[4]), output)
    output = re.sub(r'FLP_SC\.VZ\s*=\s*\'VZ\'', "FLP_SC.VZ = " + str(state_new[5]), output)
    output = re.sub(r'FLP_SC\.DragArea\s*=\s*\'DRAG_AREA\'', "FLP_SC.DragArea = " + str(drag_area), output)
    file.write(output)
    file.close()

    # Run GMAT script
    os.chdir(gmat_console_path)
    gmat_script_command = './GmatConsole ' + storage_script_name + ' > /dev/null'
    subprocess.call(gmat_script_command, shell=True)

    return storage_spk_file, gmat_sim_spk_name


def error_calculator(et_test, storage_spk_file):

    state_gps_test = np.array(
        [cspice.spkezr('-513', et_test[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(et_test))])
    cspice.furnsh(storage_spk_file)  # loading order is important!!
    state_gmat_test = np.array(
        [cspice.spkezr('-513', et_test[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(et_test))])
    cspice.unload(storage_spk_file)

    # calculate the error (epsilon)
    square = []
    eps_new_tmp_x = []
    eps_new_tmp_y = []
    eps_new_tmp_z = []
    for k in range(len(et_test)):
        eps_new_tmp_x.append(np.abs(state_gmat_test[k, 0] - state_gps_test[k, 0]))
        eps_new_tmp_y.append(np.abs(state_gmat_test[k, 1] - state_gps_test[k, 1]))
        eps_new_tmp_z.append(np.abs(state_gmat_test[k, 2] - state_gps_test[k, 2]))

    eps_new_x = sum(eps_new_tmp_x)
    eps_new_y = sum(eps_new_tmp_y)
    eps_new_z = sum(eps_new_tmp_z)

    eps_new = [eps_new_x, eps_new_y, eps_new_z]

    return eps_new


def plot_gmat_comparison(spk2, ET, error_window):

    if error_window == 'on':
        ET_plot = []
        for item in ET:
            for subitem in item:
                ET_plot.append(subitem)
    else:
        if ET[-1][-1]-ET[0][0] < 100000:
            points = ET[-1][-1]-ET[0][0]
        else:
            points = 100000
        ET_plot = np.linspace(ET[0][0],ET[-1][-1], points)



    print('Start date plot: ' + cspice.et2utc(ET_plot[0], 'C', 0))
    print('End date plot: ' + cspice.et2utc(ET_plot[-1], 'C', 0))

    # load needed kernels and calculate the states
    flp_gps_plot = np.array(
        [cspice.spkezr('-513', ET_plot[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET_plot))])

    # Note: loading order is very important!!!!
    cspice.furnsh(spk2)
    flp_gmat_plot = np.array(
        [cspice.spkezr('-513', ET_plot[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET_plot))])
    cspice.unload(spk2)

    diff_pos = np.array([np.abs(flp_gps_plot[i, 0] - flp_gmat_plot[i, 0]) \
                                 + np.abs(flp_gps_plot[i, 1] - flp_gmat_plot[i, 1]) \
                                 + np.abs(flp_gps_plot[i, 2] - flp_gmat_plot[i, 2]) \
                         for i in range(len(flp_gps_plot))])

    diff_vel = np.array([np.abs(flp_gps_plot[i, 3] - flp_gmat_plot[i, 3]) \
                                 + np.abs(flp_gps_plot[i, 4] - flp_gmat_plot[i, 4]) \
                                 + np.abs(flp_gps_plot[i, 5] - flp_gmat_plot[i, 5]) \
                         for i in range(len(flp_gps_plot))])

    # general input to get nice labels
    style = 'YYYY-MM-DD-HR-MN-SC.###'
    time = []
    for item in ET_plot:
        time.append(cspice.timout(item, style, 23))
    timeline = []
    for i in range(len(time)):
        sub = time[i].replace(".", "-").split("-")
        timeline.append(datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]),
                                          int(sub[3]), int(sub[4]), int(sub[5]), int(sub[6]), tzinfo=None))

    # plot x position
    fig, ax = plt.subplots(figsize=(10, 7))
    plt.plot(timeline, flp_gps_plot[:, 0], label='gps spk x')
    plt.plot(timeline, flp_gmat_plot[:, 0], label='gmat spk x')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=2)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.28)
    plt.show()

    # plot x,y,z position difference and sart(sqaured) difference
    fig, ax = plt.subplots(figsize=(10, 7))
    plt.plot(timeline, flp_gps_plot[:, 0] - flp_gmat_plot[:, 0], label='diff x pos')
    plt.plot(timeline, flp_gps_plot[:, 1] - flp_gmat_plot[:, 1], label='diff y pos')
    plt.plot(timeline, flp_gps_plot[:, 2] - flp_gmat_plot[:, 2], label='diff z pos')
    plt.plot(timeline, diff_pos, '--', label='diff sum pos')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=4)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.28)
    plt.show()

    # plot x,y,z velocity difference
    fig, ax = plt.subplots(figsize=(10, 7))
    plt.plot(timeline, flp_gps_plot[:, 3] - flp_gmat_plot[:, 3], label='diff x vel')
    plt.plot(timeline, flp_gps_plot[:, 4] - flp_gmat_plot[:, 4], label='diff y vel')
    plt.plot(timeline, flp_gps_plot[:, 5] - flp_gmat_plot[:, 5], label='diff z vel')
    plt.plot(timeline, diff_vel, '--', label='diff sum vel')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=4)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.28)
    plt.show()


    # calculate vector length
    vec_len_gps = np.array(
        [np.sqrt(np.square(flp_gps_plot[k][0]) + np.square(flp_gps_plot[k][1]) + np.square(flp_gps_plot[k][2]))
         for k in range(len(flp_gps_plot))])

    vec_len_gmat = np.array(
        [np.sqrt(np.square(flp_gmat_plot[k][0]) + np.square(flp_gmat_plot[k][1]) + np.square(flp_gmat_plot[k][2]))
         for k in range(len(flp_gmat_plot))])

    fig, ax = plt.subplots(figsize=(10, 7))
    ax.set_ylabel('distance [km]', fontsize=18)
    plt.plot(timeline, vec_len_gps, label='spk gps', color='green')
    plt.plot(timeline, vec_len_gmat, label='spk gmat', color='red')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=3)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.28)
    plt.show()


def spk_window(spk,sc_id):
    cover = cspice.utils.support_types.SPICEDOUBLE_CELL(1000)
    cspice.spkcov(spk, sc_id, cover)
    intervals = cspice.wncard(cover)
    print(intervals)
    # check if it is one continuous interval or many seperated ones (in this case a
    # error message is thrown as I don't want to include not_continous intervalls
    if intervals >= 2:
        print("Attention: The spk coverage is not continous!!!")

    cspice.furnsh(flp_path.lsk_file)
    tmp_begin = cover[0]
    tmp_end = cover[-1]
    time_0 = cspice.et2utc(tmp_begin, 'ISOC', 0)
    time_1 = cspice.et2utc(tmp_end, 'ISOC', 0)
    time_tmp = [time_0, time_1]
    cspice.unload(flp_path.lsk_file)

    return time_tmp


def gmat_SPK_long_term(utc_prop, progress_end):

    print('\n GMAT SPK propagation for final file started... \n')

    # input paramter for GMAT script
    mod_state = progress_end[2]
    duration = cspice.str2et(utc_prop[1]) - cspice.str2et(utc_prop[0])

    # pathes
    gmat_sim_script_name = 'spk_prop_long_duration.script'
    default_script_name = flp_path.gmat_default_script
    storage_script_name = flp_path.gmat_storage_future + gmat_sim_script_name
    gmat_console_path = flp_path.gmat

    # copy GMAT default_script
    copy = "cp " + default_script_name + " " + storage_script_name
    subprocess.call(copy, shell=True)

    # compute needed values for usage in GMAT script
    style = 'DD Mon YYYY HR:MN:SC.###'
    state_utc = cspice.timout(cspice.str2et(utc_prop[0]), style, 25)

    # insert state_new (and utc_fix) parameters in GMAT script
    file = open(storage_script_name)
    input = file.read()
    file.close()
    file = open(storage_script_name, 'w')
    output = re.sub(r'FLP_SC\.Epoch\s*=\s*\'Start_Time\'', "FLP_SC.Epoch = '" + state_utc + "'", input)
    output = re.sub(r'FLP_SC\.ElapsedSecs\s*=\s*\'Duration\'', "FLP_SC.ElapsedSecs = " + str(duration), output)
    output = re.sub(r'FLP_SPK\.Filename\s*=\s*\'Output_File_Path_Name\'',
                    "FLP_SPK.Filename = '" + flp_path.spk_prop_name + "'", output)
    output = re.sub(r'FLP_SC\.X\s*=\s*\'X\'', "FLP_SC.X = " + str(mod_state[0]), output)
    output = re.sub(r'FLP_SC\.Y\s*=\s*\'Y\'', "FLP_SC.Y = " + str(mod_state[1]), output)
    output = re.sub(r'FLP_SC\.Z\s*=\s*\'Z\'', "FLP_SC.Z = " + str(mod_state[2]), output)
    output = re.sub(r'FLP_SC\.VX\s*=\s*\'VX\'', "FLP_SC.VX = " + str(mod_state[3]), output)
    output = re.sub(r'FLP_SC\.VY\s*=\s*\'VY\'', "FLP_SC.VY = " + str(mod_state[4]), output)
    output = re.sub(r'FLP_SC\.VZ\s*=\s*\'VZ\'', "FLP_SC.VZ = " + str(mod_state[5]), output)
    output = re.sub(r'FLP_SC\.DragArea\s*=\s*\'DRAG_AREA\'', "FLP_SC.DragArea = " + str(drag_area), output)
    file.write(output)
    file.close()

    # Run GMAT script
    os.chdir(gmat_console_path)
    gmat_script_command = './GmatConsole ' + storage_script_name + ' > /dev/null'
    subprocess.call(gmat_script_command, shell=True)



input = manage_spk_data_input()
create_gmat_spk(input, utc_fix, utc_test, max_iter, del_i, step, utc_prop, error_window)

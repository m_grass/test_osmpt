'''
This script fills the gap which arises from the simplified CK generation routine. Using only one CK file where all input
data gets added step by step produces different segments in the CK kernel. These different segments will be shown with
their coverage individually. This means that there is a need to check every segment to be sure that the coverage gap
found in segment is a true gap in the overall kernel file. This routine is only needed for debug reasons if "no spice
data" is displayed in Cosmographia or some other routines fail because of transformation issues. If only one CK input
file is provided, the ckbrief -dump option could be used to get all the coverage with a terminal command.
'''


import spiceypy as cspice
import os
import numpy as np
from h.path_setup import flp_path


# load lsk, sclk and ck_master file
cspice.furnsh(flp_path.lsk_file)
cspice.furnsh(flp_path.sclk_file)
for item in os.listdir(flp_path.ck_out):
    if not item.endswith(".gitkeep"):
        master_name = item
        cspice.furnsh(flp_path.ck_out + master_name)

# precalculations
style = 'YYYY-MM-DD-HR-MN-SC.###'
cover = cspice.ckcov(flp_path.ck_out + master_name, -513000, False, 'INTERVAL', 0.0, 'TDB')
time_tmp = np.array([cspice.et2utc(cover[i], 'C', 0) for i in range(len(cover))])

for i in range(0,len(time_tmp)-1,2):
    print(time_tmp[i] + " - " + time_tmp[i+1])
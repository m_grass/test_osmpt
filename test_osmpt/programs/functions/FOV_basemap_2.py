'''
This routine projects the FOV of a specific time epoch (e.g. for a picture) onto a map. It could be used to roughly
verify the SPK and CK precision as well as the internal attitude calculation mechanism of the FLP. On the other hand
it helps to indicate landscapes easier.
On the other hand it could be used for a longer time period to plot the FOV path of an instrument on the ground. As a
special example it could be used to check for OSIRIS laser downlink pointing.
Notes:
The figsize values are high to produce a high resolution plot saved in the "plots" directory.
'''


from matplotlib import pyplot as plt
from mpl_toolkits.basemap import Basemap
from matplotlib.patches import Polygon
import numpy as np
import spiceypy as cspice
from h.path_setup import flp_path

start_time = "2018-127T21:46:46"
end_time = "2018-127T21:54:22"

# 2018-127T21:46:46 # time for which ck and spk data is available and satellite pointing fully to earth
# 2018-127T21:54:22

instrument_name = 'FLP_PAMCAM'  # sensor of interest
n_time_points = 7
fill = True  # [False, True]
mid_points = 50
figsize_1 = 70
figsize_2 = 70
revert_limb = 'on'

# load the FLP kernel
cspice.furnsh(flp_path.mk_out)
duration = int(cspice.utc2et(end_time)-cspice.utc2et(start_time))


def get_FOV_edge_vectors(FLP_INS_id, mid_points):
    """
    :param instrument_id: NAIF id of instrument
    :param mid_points: Number of points sampled on each side of rectangular FOV between the corners.
    :return: Ordered list of vectors which lie along the FOV rectangle edge.
    """
    if mid_points < 0:
        raise RuntimeError("Number of mid points must be positive.")
    # get the FOV corner vectors from SPICE
    ik_output = cspice.getfov(FLP_INS_id, 10)
    fov_shape = ik_output[0]

    if fov_shape != "RECTANGLE" and fov_shape != "CIRCLE":
        raise RuntimeError("Non-rectangular and non-circular FOVs not supported.")

    if fov_shape == "CIRCLE":

        vec = ik_output[4][0]
        circle_radius = vec[1]
        circle_boresight_len = vec[2]
        fov_edge_vectors = []
        n_fractions = mid_points + 1  # how precise you want to get the circle
        angle = np.linspace(0, 2*np.pi, n_fractions)

        for i in range(n_fractions):
            fov_edge_vectors.append((np.sin(angle[i])*circle_radius, np.cos(angle[i])*circle_radius, circle_boresight_len))
        return fov_edge_vectors, fov_shape

    if fov_shape == "RECTANGLE":
        corner_vector = ik_output[4]

        # compute mid-point vectors between each two adjacent corner pair
        fov_edge_vectors = []

        for i in range(mid_points):  # first side
            vec_1 = corner_vector[0][0]
            vec_2 = corner_vector[0][1] - (corner_vector[0][1] - corner_vector[1][1])*i/mid_points
            vec_3 = corner_vector[0][2]
            fov_edge_vectors.append((vec_1, vec_2, vec_3))
        for i in range(mid_points):  # second side
            vec_1 = corner_vector[1][0] - (corner_vector[1][0] - corner_vector[2][0])*i/mid_points
            vec_2 = corner_vector[1][1]
            vec_3 = corner_vector[1][2]
            fov_edge_vectors.append((vec_1, vec_2, vec_3))
        for i in range(mid_points):  # third side
            vec_1 = corner_vector[2][0]
            vec_2 = corner_vector[2][1] - (corner_vector[2][1] - corner_vector[3][1])*i/mid_points
            vec_3 = corner_vector[2][2]
            fov_edge_vectors.append((vec_1, vec_2, vec_3))
        for i in range(mid_points):  # fourth side
            vec_1 = corner_vector[3][0] - (corner_vector[3][0] - corner_vector[0][0])*i/mid_points
            vec_2 = corner_vector[3][1]
            vec_3 = corner_vector[3][2]
            fov_edge_vectors.append((vec_1, vec_2, vec_3))

        return fov_edge_vectors, fov_shape


def main(start_time, instrument_name,n_time_points,duration,mid_points, revert_limb):

    FLP_INS_id = cspice.bodn2c(instrument_name)

    start_et = cspice.str2et(start_time)
    # linearly sample the desired time window
    ets = np.linspace(start_et, start_et + duration, num=n_time_points)

    FOV_edge_vectors, fov_shape = get_FOV_edge_vectors(FLP_INS_id, mid_points=mid_points)
    # now we will project the FOV at each time point onto Earth's surface in CEA projection

    lon_points = []
    lat_points = []
    n, radii = cspice.bodvrd("EARTH", "RADII", 3)
    flat = (radii[0]-radii[2])/radii[0]

    for et in ets:
        # intercept each FOV edge vector with Earth's surface
        # sincpt returns multiple outputs, we care about the first one which is coordinates,
        # of the intercepted surface point in ITRF93 frame, hence the [0]
        # This will throw a SpiceError if any of the vectors fails to intercept the surface

        #---- this needs to be changed to a proper error check----
        projected_FOV_edge_vectors = []

        try:
            for i in range(len(FOV_edge_vectors)):
                projected_FOV_edge_vectors.append(cspice.sincpt("ELLIPSOID", "EARTH", et, "ITRF93",
                                                 "NONE", "FLP", cspice.bodc2n(FLP_INS_id), FOV_edge_vectors[i])[0])
        except:

            projected_FOV_edge_vectors = exception_calculation(FOV_edge_vectors, et, FLP_INS_id, revert_limb)


        #---------------------------------------------------------

        # transform the coordinates to planetographic coordinates (0, 2 pi)
        surface_lon_lat_radian = [cspice.recpgr('EARTH',rectan,radii[0],flat) for rectan in projected_FOV_edge_vectors]
        # change from radians to degrees and omit the altitude (which is normally zero) (0-360)
        surface_lon_degree = [cspice.dpr()*lon_lat_radian[0] for lon_lat_radian in surface_lon_lat_radian]
        surface_lat_degree = [cspice.dpr() * lon_lat_radian[1] for lon_lat_radian in surface_lon_lat_radian]
        lon_points.append(surface_lon_degree)
        lat_points.append(surface_lat_degree)

    plot_1(lon_points, lat_points)


def plot_1(lon_points, lat_points):
    # now we have all the projections, we plot them one by one in blue
    fig, ax = plt.subplots(figsize=(figsize_1, figsize_2))

    # this is all done to automatically know the size of the basemap to include...
    # 1. get the min and max values for longitude and latitude
    # 2. plot them
    # 3. get the default axis limit values from matplotlib as input for the basemap...
    for i in range(len(lon_points)):
        for k in range(len(lon_points[i])):
            if lon_points[i][k] > 180:
                lon_points[i][k] = lon_points[i][k] - 360

    x_min = min(lon_points[0])
    x_max = max(lon_points[0])
    y_min = min(lat_points[0])
    y_max = max(lat_points[0])
    for i in range(len(lon_points)):
        if max(lon_points[i]) > x_max:
            x_max = max(lon_points[i])
        if min(lon_points[i]) < x_min:
            x_min = min(lon_points[i])
        if max(lat_points[i]) > y_max:
            y_max = max(lat_points[i])
        if min(lat_points[i]) < y_min:
            y_min = min(lat_points[i])
    plt.plot(x_min,y_min)
    plt.plot(x_max,y_max)
    ymin, ymax = ax.get_ylim()
    xmin, xmax = ax.get_xlim()

    # generate the basemap
    map = Basemap(projection='merc', resolution='h',
                  llcrnrlon=xmin, llcrnrlat=ymin,
                  urcrnrlon=xmax, urcrnrlat=ymax)
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary(fill_color='aqua')
    map.fillcontinents(color='coral',lake_color='aqua')

    # plot the FOV intersection into the Basemap
    for i in range(len(lon_points)):
        FOV = []
        for k in range(len(lon_points[i])):
            FOV.append(map(lon_points[i][k], lat_points[i][k]))
        poly = Polygon(FOV, color='blue', alpha=0.6, fill=fill)
        plt.gca().add_patch(poly)

    # draw stations
    x_station, y_station = map(343.4882, 28.3009)  # GS1
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(210.506, 64.2008)  # HAM_Radio, HAM
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(226.456, 68.3183)  # Inuvik, INU
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(9.10384, 48.7495)  # Stuttgart, IRS
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(10.1818, 54.2629)  # Kiel, KIE
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(101.719, 2.9906)  # Malaysia, MAL
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(11.8835, 78.9277)  # NyAlesund, NYA
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(302.0992, -63.3211)  # OHiggins, OHG
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(11.2781, 48.0847)  # Oberpfaffenhofen, OPH
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(11.0853, 47.8801)  # Weilheim, WHM
    map.plot(x_station, y_station, marker='.', color='red')

    # draw parallels and meridians.
    # label parallels on right and top
    # meridians on bottom and left
    parallels = np.arange(-90, 90, 1)
    # labels = [left,right,top,bottom]
    map.drawparallels(parallels, labels=[False, True, True, False])
    meridians = np.arange(0, 360, 1)
    map.drawmeridians(meridians, labels=[True, False, False, True])

    # Title and plot
    plt.title(str(start_time) + " - " + str(end_time) + " , " + str(n_time_points) + " points")
    plot_name = start_time.split(".")[0].replace("-", "_").replace(":", "")
    fig.savefig(flp_path.plots + 'basemap_' + instrument_name + '_' + plot_name + '.png', dpi=fig.dpi)
    plt.show()


def exception_calculation(FOV_edge_vectors, et, FLP_INS_id, revert_limb):

    counter = 0
    for p in range(len(FOV_edge_vectors)):
        try:
            cspice.sincpt("ELLIPSOID", "EARTH", et, "ITRF93", "NONE", "FLP", cspice.bodc2n(FLP_INS_id), FOV_edge_vectors[p])[0]
        except:
            continue
        else:
            counter = 1
            print(p)
            break

    if counter == 1:
        print('Some piece of the Earth is inside the instrument FOV')
    else:
        print('The Earth is not inside the FOV of the specified instrument')
        quit()

    fov_edge_1 = []
    fov_edge_2 = []
    try:
        for cut_forward in range(p,len(FOV_edge_vectors)):
            fov_edge_1.append(cspice.sincpt("ELLIPSOID", "EARTH", et, "ITRF93",
                                            "NONE", "FLP", cspice.bodc2n(FLP_INS_id), FOV_edge_vectors[cut_forward])[0])
    except:
        print(cut_forward)

    if p == 0:
        try:
            for cut_backward in reversed(range(p,len(FOV_edge_vectors))):
                fov_edge_2.append(cspice.sincpt("ELLIPSOID", "EARTH", et, "ITRF93",
                                                "NONE", "FLP", cspice.bodc2n(FLP_INS_id), FOV_edge_vectors[cut_backward])[0])
        except:
            print(cut_backward)

        fov_2_1 = FOV_edge_vectors[cut_backward + 1]  # last working one
        fov_2_2 = FOV_edge_vectors[cut_backward]  # first not working one

    fov_1_1 = FOV_edge_vectors[cut_forward-1]  # last working one
    fov_1_2 = FOV_edge_vectors[cut_forward]  # first not working one


    for g in range(10):

        n = 20
        fov_x_1 = np.linspace(fov_1_1[0], fov_1_2[0], n)
        fov_y_1 = np.linspace(fov_1_1[1], fov_1_2[1], n)
        fov_z_1 = np.linspace(fov_1_1[2], fov_1_2[2], n)
        vec_1 = []
        for i in range(n):
            vec_1.append([fov_x_1[i], fov_y_1[i], fov_z_1[i]])
        try:
            for m in range(n):
                fov_edge_1.append(cspice.sincpt("ELLIPSOID", "EARTH", et, "ITRF93",
                                                "NONE", "FLP", cspice.bodc2n(FLP_INS_id), vec_1[m])[0])
        except:
            fov_1_1 = vec_1[m - 1]
            fov_1_2 = vec_1[m]

        if p == 0:
            fov_x_2 = np.linspace(fov_2_2[0], fov_2_1[0], n)
            fov_y_2 = np.linspace(fov_2_2[1], fov_2_1[1], n)
            fov_z_2 = np.linspace(fov_2_2[2], fov_2_1[2], n)
            vec_2 = []
            for i in range(n):
                vec_2.append([fov_x_2[i], fov_y_2[i], fov_z_2[i]])
            try:
                for f in reversed(range(n)):
                    fov_edge_2.append(cspice.sincpt("ELLIPSOID", "EARTH", et, "ITRF93",
                                                   "NONE", "FLP", cspice.bodc2n(FLP_INS_id), vec_2[f])[0])
            except:
                fov_2_1 = vec_2[f + 1]
                fov_2_2 = vec_2[f]


    limb_fov_list = limb_fov(et)

    if revert_limb == 'on':
        projected_FOV_edge_vectors = fov_edge_1 + list(reversed(limb_fov_list)) + list(reversed(fov_edge_2))
    else:
        projected_FOV_edge_vectors = fov_edge_1 + list(limb_fov_list) + list(reversed(fov_edge_2))

    return projected_FOV_edge_vectors


def limb_fov(et):
    limb_fov_list = []

    method = 'TANGENT/ELLIPSOID'
    target = 'EARTH'
    fixref = 'ITRF93'
    abcorr = 'NONE'
    corloc = 'CENTER'
    obsrvr = 'FLP'
    refvec = [0, 0, 1]
    rolstp = 0.01 * cspice.rpd()
    ncuts = 36000
    schstp = 10
    soltol = 1
    maxn = 100000
    points = \
        cspice.limbpt(method, target, et, fixref, abcorr, corloc, obsrvr, refvec, rolstp, ncuts, schstp, soltol, maxn)[
            1]

    pos = cspice.spkpos('FLP', et, 'J2000', 'NONE', 'EARTH')[0]
    raydir = np.array([item - pos for item in points])
    inst = instrument_name
    rframe = 'J2000'
    abcorr = 'NONE'
    observer = 'FLP'
    visible = np.array([cspice.fovray(inst, item, rframe, abcorr, observer, et) for item in raydir])
    for k in range(len(raydir)):
        if visible[k] == True:
            limb_fov_list.append(raydir[k])

    return limb_fov_list


# to execute the function(the program)
if __name__=='__main__':
    main(start_time, instrument_name,n_time_points,duration,mid_points,revert_limb)
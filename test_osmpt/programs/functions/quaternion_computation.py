'''
This script computes, plots and stores quaternions. These quaternions represent rotations needed to aim a specific
ephemeris object or target on Earth. To validate the output of this routine in cosmographia make sure you do not load
any other TM quaternion files as the newly produced ones, labeled: TM_test_output . Otherwise the planned data could be
overwritten by other data (especially if you calculate these quaternions for the past). This routine is meant to serve
as a Mission planning script. If further constraints e.g. constellations have to be met one or many geometry finder
routines will be executed beforehand as shown in the occultation_computation script.
'''


import spiceypy as cspice
import numpy as np
import datetime
from matplotlib import pyplot as plt
from h.path_setup import flp_path

cspice.furnsh(flp_path.mk_out)


start = '2017 OCT 19 01:34:55.426'
end = '2017 OCT 19 01:37:03.640'
instrument = 'FLP_MICS'
target = 'IRS'

# precalculations
et_start = cspice.str2et(start)
et_end = cspice.str2et(end)
ET = np.linspace(et_start,et_end,et_end-et_start)
instrument_ID = str(cspice.bodn2c(instrument))  # ID code for instrument name
frame_instrument_pool = 'INS' + instrument_ID + '_FOV_FRAME'
frame_instrument = cspice.gcpool(frame_instrument_pool, 0, 1)[0]  # frame name of instrument
boresight_pool = 'INS' + instrument_ID + '_BORESIGHT'
boresight = cspice.gdpool(boresight_pool, 0, 3)  # boresight vector of sensor
abcorr = 'NONE'
obsrvr = 'FLP'

# vector to target
vec = np.array([cspice.spkpos(target, ET[i], 'J2000', abcorr, obsrvr)[0] for i in range(len(ET))])

q = []
a = []
b = []
c = []
# the boresight_j2000 vector equals the boresight vector in the instrument frame as we define that the instrument frame
# is orientated as the j2000 frame. This means it needs to rotate about the output quaternion to look at the specified
# target. But this rotation needs to be "implemented". This is a difference compared with the already existing attitude
# data where the SC was already rotated!
for i in range(len(vec)):
    # normal vector of cross product
    H = cspice.ucrss(boresight, vec[i])
    # rotation angle   # vnorm boresight_j2000 = 1; -> not calculated.
    I = cspice.vdot(vec[i], boresight) / (cspice.vnorm(vec[i]))
    J = np.arccos(I)
    K = cspice.axisar(H, J)
    q.append(cspice.m2q(K)[0])  # store the quaternion entries seperated for easier plot
    a.append(cspice.m2q(K)[1])
    b.append(cspice.m2q(K)[2])
    c.append(cspice.m2q(K)[3])


# plot the calculated data
style = 'YYYY-MM-DD-HR-MN-SC.###'
timeline = []
for item in ET:
    time = cspice.timout(item, style, 23)
    sub = time.replace(".", "-").split("-")
    timeline.append(datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]),
                                      int(sub[3]), int(sub[4]), int(sub[5]), int(sub[6]), tzinfo=None))

fig, ax = plt.subplots(figsize=(15,7))
ax.set_ylabel('Rotation [-]', fontsize=18)
plt.plot(timeline, q, label='quat q', color='green')
plt.plot(timeline, a, label='quat a', color='brown')
plt.plot(timeline, b, label='quat b', color='blue')
plt.plot(timeline, c, label='quat c', color='red')
plt.gcf().autofmt_xdate()
ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=4)
for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(15)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(15)
plt.grid()
fig.savefig(flp_path.plots + 'quaternion_computation.png', dpi=fig.dpi)
plt.show()

# write values as TM input file
style_2 = 'YYYY-MM-DD HR-MN-SC.###'
name = 'TM_test_output.csv'
file = open(flp_path.TM_produced + name, 'w')
text = str(
    '"time";"timestamp";"AYTFQU00";"AYTFQU01";"AYTFQU02";"AYTFQU03"' + '\n'
)
file.write(text)
for i in range(len(ET)):
    file.write(';'.join([cspice.timout(ET[i],style_2,23), 'sclk_clock', str(a[i]), str(b[i]), str(c[i]), str(q[i]) + '\n']))
file.close()

cspice.unload(flp_path.mk_out)
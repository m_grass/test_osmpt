'''
This routine is built around a specific need: Check whether the a target is in the instrument FOV.
This was done for the FLP_OSIRIS instrument points with its circular FOV and the GS1 as target. This way it could be
determined if a downlink to the station could be established referring geometric need and according to the CKs and SPKs
generated before which do have some implementation errors and the overall noise of the GPS data... .
'''



import spiceypy as cspice
from h.path_setup import flp_path

cspice.furnsh(flp_path.mk_out)

# define input data
begin = "2018 MAR 15 10:00:00"
end = "2018 APR 09 23:00:00"

inst = 'FLP_OSIRIS'
target = 'GS1'
tshape = 'POINT'
tframe = ""  # should be ignored and left blank for tshape = POINT
abcorr = 'NONE'
obsrvr = 'FLP'

step = 1  #  stepsize [sec]

result = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(2)

# pre-computations
et_begin = cspice.utc2et(begin)
et_end = cspice.utc2et(end)
ET = [et_begin, et_end]
cspice.appndd(ET, cnfine)

# compute gf search 1
result = cspice.gftfov(inst, target, tshape, tframe, abcorr, obsrvr, step, cnfine)

# print results
count = cspice.wncard(result)
print(count)

if count == 0:
    print("Result window is empty.\n\n")
else:
    for i in range(count):
        # Fetch the endpoints of the Ith interval of the result window.
        start, stop = cspice.wnfetd(result, i)

        if (start == stop):
            time = cspice.et2utc(start, 'C', 3)
            print("Event time: " + time + "\n")
        else:
            time_left = cspice.et2utc( start, 'C', 3)
            time_right = cspice.et2utc( stop, 'C', 3)
            print( "Interval " + str(i+1) + "\n")
            print( "From : " + time_left + " \n")
            print( "To   : " + time_right + " \n")
            print( " \n" )



cspice.unload(flp_path.mk_out)
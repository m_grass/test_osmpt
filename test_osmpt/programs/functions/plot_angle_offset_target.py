'''
This function plots the angle between the specified instrument boresight vector and the FLP SC->target vector
plotted over a specified time frame. The time frame is split into shorter time windows fulfilling the conditions
defined. (Elevation over ground)
'''

import spiceypy as cspice
import numpy as np
import datetime
from matplotlib import pyplot as plt
from h.path_setup import flp_path

cspice.furnsh(flp_path.mk_out)

instrument = 'FLP_OSIRIS'
obsrvr = 'GS1'  # it is called observer because of the gf routine, think of it as "target" [valid: targets or GS generated with pinpoint]
begin = "2018 MAR 22 22:18:30"
end = "2018 MAR 23 22:18:30"
n_point = 2000
abcorr = 'NONE'
refval = 3 * cspice.rpd()  # elevation [degree]
time_offset = 140  # time offset before and after the pass condition is met [s]


# precomputations to get all parameters
et_begin = cspice.utc2et(begin)
et_end = cspice.utc2et(end)
et = [et_begin, et_end]
instrument_ID = str(cspice.bodn2c(instrument))  # ID code for sensor name
frame_instrument_pool = 'INS' + instrument_ID + '_FOV_FRAME'
frame_instrument = cspice.gcpool(frame_instrument_pool, 0, 1)[0]  # frame name of sensor
boresight_pool = 'INS' + instrument_ID + '_BORESIGHT'
boresight = cspice.gdpool(boresight_pool, 0, 3)  # boresight vector of sensor
target_ID = str(cspice.bodn2c(obsrvr))  # ID code for target (or observer?)
frame_obsrvr_pool = 'FRAME_1' + target_ID + '_NAME'  # this will work only for pinpoint generated tf files.
frame_obsrvr = cspice.gcpool(frame_obsrvr_pool, 0, 1)[0]  # frame name of target
crdsys = 'LATITUDINAL'
coord = 'LATITUDE'
relate = '>'
adjust = 0
step = 20 * 60  #  stepsize [min]
nintvls = 7500
result = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
cspice.appndd(et, cnfine)


# compute gf search
# elevation above x [degrees]
cspice.gfposc('FLP', frame_obsrvr, abcorr, obsrvr, crdsys, coord, relate, refval, adjust, step, nintvls, cnfine, result)
count = cspice.wncard(result)
print("Number of result windows: " + str(count))

if (count == 0):
    print("Result window is empty.\n\n")
else:

    # actual angle_offset_target calculations:
    for i in range(count):

        # get pass time
        et_start, et_stop = cspice.wnfetd(result, i)
        ET = np.linspace(et_start-time_offset, et_stop+time_offset,n_point)

        # FLP SC -> target vector
        vec = np.array([cspice.spkpos('FLP', ET[i], frame_instrument, abcorr, obsrvr)[0] for i in range(len(ET))])

        # angle in degree derived from dot product; the boresight norm is already and therefore does not need to be calculated
        # the minus sign is included to change the vector orientation ...
        angle = np.array([cspice.dpr()*np.arccos(cspice.vdot(-vec[i,:], boresight)/cspice.vnorm(vec[i,:])) for i in range(len(ET))])

        # get nice timeline
        style = 'YYYY-MM-DD-HR-MN-SC.###'
        timeline = []
        for i in range(len(ET)):
            time = cspice.timout(ET[i], style, 23)
            sub = time.replace(".", "-").split("-")
            timeline.append(datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]),int(sub[3]), int(sub[4]), int(sub[5]), int(sub[6]), tzinfo=None))

        # plot the data
        fig, ax = plt.subplots(figsize=(15, 8))
        plt.plot(timeline, angle, label='angle [degrees]')
        plt.gcf().autofmt_xdate()
        ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=1)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        plt.grid()
        plt.title(
            str(obsrvr) + " " + str(cspice.et2utc(et_start, 'C', 0)) + " - " + str(cspice.et2utc(et_stop, 'C', 0)),
            fontsize=18)
        plot_name = cspice.et2utc(et_start, 'ISOC', 0).replace("-", "_").replace(":", "")
        fig.subplots_adjust(top=0.95)  # adjust the shown plot
        fig.subplots_adjust(bottom=0.2)
        fig.subplots_adjust(left=0.08)
        fig.savefig(flp_path.plots + str(obsrvr) + '_angular_offset_' + plot_name + '.png', dpi=fig.dpi)
        plt.show()

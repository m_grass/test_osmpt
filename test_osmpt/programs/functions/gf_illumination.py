'''
This script should be used to determine (photo) opportunities if special requirements for the illumination of a target
exist. Usage scenarios could be the quantification of illuminiation angles on the shutter times of the cameras.
The minimum and maximum angles to be specified are:
The overall angle between source-target-FLP (phase angle)
The angle between surface normal of target and the source (incident angle)
The angle between surfache normal of the target and the FLP (emission angle)
'''



import spiceypy as cspice
from h.path_setup import flp_path


# define time interval and step size
begin = "2018 JUN 01 20:00:00"
end = "2018 JUN 02 23:00:00"
step = 200  #  stepsize [s]
# define point (on Earth surface) -> http://dateandtime.info/citycoordinates.php
alt   = 0.4  # [km]
lon =  10  # [degrees]
lat =  50  # [degress]
# define constrains; use default, if this you do not want to put an additional constraint on it
phase_angle_min = 0  # default = 0 [degree]
phase_angle_max = 90  # default = 180 [degree]
incidence_angle_min = 0  # default = 0 [degree]
incidence_angle_max = 180  # default = 180 [degree]
emission_angle_min =  0 # default = 0 [degree]
emission_angle_max =  40 # default = 180 [degree]

# "Predefined" parameters for gfsntc and gfilum
method = "Ellipsoid"
angtyp = 'PHASE'
target = 'EARTH'
illumn = 'SUN'
frame = 'ITRF93'
abcorr_ilum = 'LT+S'
obsrvr = 'FLP'
adjust = 0
nintvls = 75000
cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(2)

# load metakernel
cspice.furnsh(flp_path.mk_out)

# pre-computations
et_begin = cspice.utc2et(begin)
et_end = cspice.utc2et(end)
ET = [et_begin, et_end]
cspice.appndd(ET, cnfine)
# convert planetocentric r/lon/lat to Cartesian vector
radii = cspice.bodvrd( 'Earth', 'RADII', 3 )[1]
f = (radii[0]-radii[2])/radii[0]
lon_rad = lon * cspice.rpd()
lat_rad = lat * cspice.rpd()
spoint = cspice.georec( lon_rad, lat_rad, alt, radii[0], f )  # also input for gfilum!



# compute gf search:

# phase angle above  x [degrees]
refval = phase_angle_min * cspice.rpd()  # now in [rad]
relate = '>'
result = cspice.utils.support_types.SPICEDOUBLE_CELL(1500000)
cspice.gfilum(method, angtyp, target, illumn, frame, abcorr_ilum, obsrvr, spoint, relate, refval, adjust, step, nintvls, cnfine, result)
count = cspice.wncard(result)
print(count)

# phase angle below  x [degrees]
refval = phase_angle_max * cspice.rpd()  # now in [rad]
relate = '<'
result_2 = cspice.utils.support_types.SPICEDOUBLE_CELL(1500000)
cspice.gfilum(method, angtyp, target, illumn, frame, abcorr_ilum, obsrvr, spoint, relate, refval, adjust, step, nintvls,result, result_2)
count = cspice.wncard(result_2)
print(count)

# incidence angle above  x [degrees]
refval = incidence_angle_min * cspice.rpd()  # now in [rad]
relate = '>'
result_3 = cspice.utils.support_types.SPICEDOUBLE_CELL(1500000)
cspice.gfilum(method, angtyp, target, illumn, frame, abcorr_ilum, obsrvr, spoint, relate, refval, adjust, step, nintvls, result_2, result_3)
count = cspice.wncard(result_3)
print(count)

# incidence angle below x [degrees]
refval = incidence_angle_max * cspice.rpd()  # now in [rad]
relate = '<'
result_4 = cspice.utils.support_types.SPICEDOUBLE_CELL(1500000)
cspice.gfilum(method, angtyp, target, illumn, frame, abcorr_ilum, obsrvr, spoint, relate, refval, adjust, step, nintvls,result_3, result_4)
count = cspice.wncard(result_4)
print(count)

# emission angle above  x [degrees]
refval = emission_angle_min * cspice.rpd()  # now in [rad]
relate = '>'
result_5 = cspice.utils.support_types.SPICEDOUBLE_CELL(1500000)
cspice.gfilum(method, angtyp, target, illumn, frame, abcorr_ilum, obsrvr, spoint, relate, refval, adjust, step, nintvls, result_4, result_5)
count = cspice.wncard(result_5)
print(count)

# emission angle below x [degrees]
refval = emission_angle_max * cspice.rpd()  # now in [rad]
relate = '<'
result_6 = cspice.utils.support_types.SPICEDOUBLE_CELL(1500000)
cspice.gfilum(method, angtyp, target, illumn, frame, abcorr_ilum, obsrvr, spoint, relate, refval, adjust, step, nintvls,result_5, result_6)
count = cspice.wncard(result_6)
print(count)


# print results
if (count == 0):
    print("Result window is empty.\n\n")
else:
    for i in range(count):
        # Fetch the endpoints of the Ith interval of the result window.
        left, right = cspice.wnfetd(result_6, i)

        if (left == right):
            time = cspice.et2utc(left, 'C', 3)
            print("Event time: " + time + "\n")
        else:
            time_left = cspice.et2utc( left, 'C', 3)
            time_right = cspice.et2utc( right, 'C', 3)
            print( "Interval " + str(i+1) + "\n")
            print( "From : " + time_left + " \n")
            print( "To   : " + time_right + " \n")
            print( " \n" )

'''
This script computes the minimal angle (appearing at the highest elevation of the SC in the topocentric target frame)
between the SC subobserver point and a target. This routine is not validated yet.
'''


import spiceypy as cspice
import numpy as np
from h.path_setup import flp_path


# load metakernel
cspice.furnsh(flp_path.mk_out)

# define input data
begin = "2018 MAY 10 12:21:30"
end = "2018 MAY 20 12:26:00"

target = 'IRS'
abcorr = 'NONE'
obsrvr = 'FLP'
relate = '<'

refval = 3000  # seems to be a useful value
adjust = 0
step = 20 * 60  #  stepsize [min]
nintvls = 7500

result = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(2)

# pre-computations
et_begin = cspice.utc2et(begin)
et_end = cspice.utc2et(end)
ET = [et_begin, et_end]
cspice.appndd(ET, cnfine)

# compute gf search 1
cspice.gfdist(target, abcorr, obsrvr, relate, refval, adjust, step, nintvls, cnfine, result)
# compute gf search 2
relate_2 = 'LOCMIN'
result_2 = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
cspice.gfdist(target, abcorr, obsrvr, relate_2, refval, adjust, step, nintvls, result, result_2)


# print results and store the time of interest (ET)
count_2 = cspice.wncard(result_2)
print(count_2)

ET = []
time_max = []
for i in range(count_2):
    time_et_1, time_et_2 = cspice.wnfetd(result_2, i)
    if time_et_1 == time_et_2:  # this should always be the case
        time_max.append(cspice.et2utc(time_et_1, 'C', 3))
        ET.append(time_et_1)

# calculate the FLP-(subobserver)surface vectors
flp_spoint = np.array([cspice.subpnt("INTERCEPT/ELLIPSOID", "EARTH", ET[i], "ITRF93", "NONE", "FLP")[2]
                                    for i in range(len(ET))])
# calculate the FLP-target vectors
flp_target = np.array([cspice.spkpos('-513', ET[i], 'ITRF93', 'NONE', target)[0] for i in range(len(ET))])
# calculate the target-(subobserver)surface
spoint = np.array([cspice.subpnt("INTERCEPT/ELLIPSOID", "EARTH", ET[i], "ITRF93", "NONE", "FLP")[0]
                                    for i in range(len(ET))])
target = np.array([cspice.spkpos(target, ET[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET))])
target_spoint = target - spoint

rot_matrix = np.array([cspice.pxform("ITRF93","FLP_ANGLE_SIGN", ET[i]) for i in range(len(ET))])
target_spoint_flp_angle_sign = np.array([cspice.mxv(rot_matrix[i],target_spoint[i]) for i in range(len(ET))])

flp_spoint_distance = []
flp_target_distance = []
target_spoint_distance = []
for i in range(len(ET)):
    flp_spoint_distance.append(
        np.sqrt(np.square(flp_spoint[i,0]) + np.square(flp_spoint[i,1]) + np.square(flp_spoint[i,2])))
    flp_target_distance.append(
        np.sqrt(np.square(flp_target[i, 0]) + np.square(flp_target[i, 1]) + np.square(flp_target[i, 2])))
    target_spoint_distance.append(
        np.sqrt(np.square(target_spoint[i, 0]) + np.square(target_spoint[i, 1]) + np.square(target_spoint[i, 2])))

angle_rad = []
for i in range(len(ET)):
    angle_rad.append(
        np.arccos((np.square(flp_target_distance[i]) +
                   np.square(flp_spoint_distance[i]) -
                   np.square(target_spoint_distance[i])) /
                  (2 * flp_target_distance[i] * flp_spoint_distance[i])))

angle = []
for i in range(len(ET)):
    if target_spoint_flp_angle_sign[i,1] > 0:
        angle.append((cspice.dpr() * angle_rad[i])*(-1))  # here we have to revert the sign for the angle! to point right
    else:
        angle.append(cspice.dpr() * angle_rad[i])  # this is automatically pointing right

quaternion = []
for i in range(len(ET)):
    quaternion.append([np.sin(angle_rad[i]/2), 0, 0, np.cos(angle_rad[i]/2)])

for i in range(len(time_max)):
    print( "Minimum distance at: " + time_max[i] + '\n'
           "Minimum resulting angle [degree]: " + str(angle[i]) + '\n'
           #"Resulting quaternion [q, a, b, c]: " + str(quaternion[i]) + '\n' +  # NOT CHECKED YET
           )

cspice.unload(flp_path.mk_out)
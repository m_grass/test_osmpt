'''
This function plots the angle between the specified instrument boresight vector and the FLP SC->target vector
plotted over a specified time frame. The time frame is split into shorter time windows fulfilling the conditions
defined. (Elevation over ground)
The times the conditions for the paths are met will be printed to the output.
'''

import spiceypy as cspice
import numpy as np
import datetime
from matplotlib import pyplot as plt
from h.path_setup import flp_path

cspice.furnsh(flp_path.mk_out)

obsrvr = 'AALEN'  # it is called observer because of the gf routine, think of it as "target" [valid: targets or GS generated with pinpoint]
begin = "2018 MAY 09 01:00:00.000"
end = "2018 JUN 01 01:00:00.000"
abcorr = 'NONE'
refval = 2 * cspice.rpd()  # elevation [degree]
time_offset = 0  # time offset before and after the pass condition is met [s]
plot = 'off'

# precomputations to get all parameters
et_begin = cspice.utc2et(begin)
et_end = cspice.utc2et(end)
et = [et_begin, et_end]
target_ID = str(cspice.bodn2c(obsrvr))  # ID code for target
frame_obsrvr_pool = 'FRAME_1' + target_ID + '_NAME'  # this will work only for pinpoint generated tf files.
frame_obsrvr = cspice.gcpool(frame_obsrvr_pool, 0, 1)[0]  # frame name of target
crdsys = 'LATITUDINAL'
coord = 'LATITUDE'
relate = '>'
adjust = 0
step = 20 * 60  #  stepsize [min]
nintvls = 7500
result = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(2)
cspice.appndd(et, cnfine)


# compute gf search
# elevation above x [degrees]
cspice.gfposc('FLP', frame_obsrvr, abcorr, obsrvr, crdsys, coord, relate, refval, adjust, step, nintvls, cnfine, result)
count = cspice.wncard(result)
print("Number of result windows: " + str(count))
# max. elevation time
relate_2 = 'LOCMAX'
result_2 = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
cspice.gfposc('FLP', frame_obsrvr, abcorr, obsrvr, crdsys, coord, relate_2, refval, adjust, step, nintvls, result, result_2)

if (count == 0):
    print("Result window is empty.\n\n")
else:

    # actual plot_pass calculations:
    for k in range(count):

        # get maximal elevation for output
        max_el_l, max_el_r = cspice.wnfetd(result_2, k)

        # get pass time
        et_start, et_stop = cspice.wnfetd(result, k)
        ET = np.linspace(et_start-time_offset, et_stop+time_offset,(et_stop+time_offset)-(et_start-time_offset))

        # target-> FLP SC vector
        vec = np.array([cspice.spkpos('FLP', ET[i], frame_obsrvr, abcorr, obsrvr)[0] for i in range(len(ET))])

        # convert from rectangular coordinates to spherical coordinates
        r = np.array([cspice.recsph(-vec[i,:])[0] for i in range(len(ET))])  # km
        # Angle between the point and the positive z-axis [degrees] ; the z axis is the zenit direction of the topocentric frame.
        colat = np.array([cspice.dpr()*cspice.recsph(-vec[i, :])[1] for i in range(len(ET))])
        # angle between the positive X-axis and the orthogonal projection of the point onto the XY plane. [degrees]
        # the x axis is pointing north [topocentric frame].
        lon = np.array([cspice.dpr()*cspice.recsph(-vec[i, :])[2] for i in range(len(ET))])


        if plot == 'on':
            # get nice timeline
            style = 'YYYY-MM-DD-HR-MN-SC.###'
            timeline = []
            for i in range(len(ET)):
                time = cspice.timout(ET[i], style, 23)
                sub = time.replace(".", "-").split("-")
                timeline.append(datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]),int(sub[3]), int(sub[4]), int(sub[5]), int(sub[6]), tzinfo=None))

            # plot the data
            fig, ax1 = plt.subplots(figsize=(15, 8))

            ax1.set_ylabel('distance [km]',fontsize=18)
            line_1 = ax1.plot(timeline, r, label='distance', linestyle=':', color='red', marker='x')
            ax2 = ax1.twinx()
            ax2.set_ylabel('angle [degree]', fontsize=18)
            line_2 = ax2.plot(timeline, colat, label='colat', color='blue', marker='.')
            line_3 = ax2.plot(timeline, lon, label='lon', color='green', marker='o')
            ax2.set_ylim(-190,190)

            fig.tight_layout()
            plt.gcf().autofmt_xdate()
            lines = line_1+line_2+line_3
            labels = [item.get_label() for item in lines]
            plt.legend(lines, labels, fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=3)
            for tick in ax1.xaxis.get_major_ticks():
                tick.label.set_fontsize(15)
            for tick in ax1.yaxis.get_major_ticks():
                tick.label.set_fontsize(15)
            for tick in ax2.yaxis.get_major_ticks():
                tick.label.set_fontsize(15)
            plt.grid()
            plt.title(str(obsrvr) + " " + str(cspice.et2utc(et_start, 'C', 0)) + " - " + str(cspice.et2utc(et_stop, 'C', 0)), fontsize=18)
            plot_name = cspice.et2utc(et_start, 'ISOC', 0).replace("-", "_").replace(":", "")
            fig.subplots_adjust(top=0.95)  # adjust the shown plot
            fig.subplots_adjust(bottom=0.2)
            fig.subplots_adjust(left=0.08)
            fig.savefig(flp_path.plots + str(obsrvr) + '_pass_' + plot_name + '.png', dpi=fig.dpi)
            plt.show()

        # Print characteristic output values
        # Fetch the endpoints of the Ith interval of the result window.
        if (max_el_l == max_el_r):
            max_el = max_el_l

            if (et_start == et_stop):
                time = cspice.et2utc(et_start, 'C', 3)
                print("Event time: " + time + "\n")
            else:
                time_left = cspice.et2utc(et_start, 'C', 3)
                time_right = cspice.et2utc(et_stop, 'C', 3)
                time_max = cspice.et2utc(max_el, 'C', 3)
                print("Interval " + str(k + 1) + "\n")
                print("From : " + time_left + " \n")
                print("To   : " + time_right + " \n")
                print("Max  : " + time_max + " \n")
                print(" \n")

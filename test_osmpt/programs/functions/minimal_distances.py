'''
This script generates the times of local minima distance between a target and the FLP SC. This might be useful as this
will be the times with the highest resultion achievable with the onboard cameras. Note that this is an easy routine
which also serves as geometry-finder implementation demonstration.
'''



import spiceypy as cspice
from h.path_setup import flp_path

cspice.furnsh(flp_path.mk_out)

# define input data
begin = "2017 OCT 01 01:00:00"
end = "2017 OCT 01 23:00:00"

target = 'MILAN'
abcorr = 'NONE'
obsrvr = 'FLP'
relate = '<'

refval = 3000  # seems to be a useful value
adjust = 0
step = 20 * 60  #  stepsize [min]
nintvls = 7500

result = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(2)

# pre-computations
et_begin = cspice.utc2et(begin)
et_end = cspice.utc2et(end)
ET = [et_begin, et_end]
cspice.appndd(ET, cnfine)

# compute gf search 1
cspice.gfdist(target, abcorr, obsrvr, relate, refval, adjust, step, nintvls, cnfine, result)
# compute gf search 2
relate_2 = 'LOCMIN'
result_2 = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
cspice.gfdist(target, abcorr, obsrvr, relate_2, refval, adjust, step, nintvls, result, result_2)


# print results
count = cspice.wncard(result_2)
print(count)

if count == 0:
    print("Result window is empty.\n\n")
else:
    for i in range(count):
        # Fetch the endpoints of the Ith interval of the result window.
        start, stop = cspice.wnfetd(result_2, i)

        if (start == stop):
            time = cspice.et2utc(start, 'C', 3)
            print("Event time: " + time + "\n")
        else:
            time_left = cspice.et2utc( start, 'C', 3)
            time_right = cspice.et2utc( stop, 'C', 3)
            print( "Interval " + str(i+1) + "\n")
            print( "From : " + time_left + " \n")
            print( "To   : " + time_right + " \n")
            print( " \n" )



cspice.unload(flp_path.mk_out)
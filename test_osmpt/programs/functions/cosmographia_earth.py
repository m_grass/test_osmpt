'''
This script should be used if Cosmographia should be started with a default Earth view (with a camera viewpoint above
the north pole. This view is useful to check if all newly added targets are loaded or to launch cosmographia after
new TM data is used.
Note that you need to change the visualisation start time manually.
'''

import spiceypy as cspice
import os
import subprocess
from h.path_setup import flp_path

utc = '2018 MAY 19 08:56:38.259'

# '2018 MAY 08 10:20:51.460'  WHM/IRS pass  3-day window
# '2018 MAY 29 15:47:06.260'  # time las vegas pointing...



def cosmo_earth(utc):
    cspice.furnsh(flp_path.mk_out)
    et = cspice.str2et(utc)
    jd = str(cspice.j2000() + et / cspice.spd())
    cosmo_launch = "./Cosmographia.sh"+" -style=gtk"

    # center, select, frame, jd, cam_x, cam_y, cam_z, cam_qw, cam_qx, cam_qy, cam_qz, ts, cam_fov
    val = ['Earth', 'Earth', 'bfix', jd , '0.0', '0.0', '20000', '1.0', '0.0', '0.0', '0.0', '0', '40']
    cosmo_state = "'cosmo:" + val[0] + "?&select=" + val[1] + "&frame=" + val[2] + "&jd=" + val[3] + "&x=" + \
                        val[4] + "&y=" + val[5] + "&z=" + val[6] + "&qw=" + val[7] + "&qx=" + val[8] + "&qy=" + val[9] + \
                        "&qz=" + val[10] + "&ts=" + val[11] + "&fov=" + val[12] + "'"

    cosmographia_command = cosmo_launch + " -u " + cosmo_state + " -p " + flp_path.cosmo_script_earth + " " + flp_path.cosmo_json
    os.chdir(flp_path.cosmographia)
    subprocess.call(cosmographia_command, shell=True)

cosmo_earth(utc)
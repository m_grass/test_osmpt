'''
-------------- Mission planning -------------
This script is an example program on how to costumize constellation searches for specific geometry events. This script
focus' on a scenario "moon rise behind earth". In order to make use out of this program, the following steps have to
be performed:
1. Load SPK kernel into TM directory for the time of interest.
2. Run the spk_ck_creation program.
3. Execute this program. (Copy output times if needed)
4. Copy the produced kernels "TM/produced_quat" to the "TM" directory (this is manually on purpose!)
5. Run the spk_ck_program again.
6. Run the cosmographia_sensor program (with on of the output times from step 3)
7. Go a little bit forwards, backwards in time [max. 1 hour] to see if the scenario of interest really appears.
'''


import spiceypy as cspice
import numpy as np
import datetime
from matplotlib import pyplot as plt
from h.path_setup import flp_path

cspice.furnsh(flp_path.mk_out)

# define input data
begin = "2018 MAY 07 22:00:00.00"
end = "2018 JUN 09 20:00:00.00"
inst = 'FLP_MICS'
target = 'MOON'
phase_angle_max = 40
plot = 'off'

# parameter for gfilum search (done first , to hopefully speed up the search)
method = "Ellipsoid"
angtyp = 'PHASE'
illumn = 'SUN'
frame = 'IAU_MOON'
adjust = 0
nintvls = 75000
abcorr = 'NONE'
obsrvr = 'FLP'
step = 1000
result_0 = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
cnfine_0 = cspice.utils.support_types.SPICEDOUBLE_CELL(2)
# convert planetocentric r/lon/lat to Cartesian vector
# define point (on Moon surface)
alt   = 0  # [km]
lon =  0  # [degrees]
lat =  90  # [degress]
radii = cspice.bodvrd( 'Moon', 'RADII', 3 )[1]
f = (radii[0]-radii[2])/radii[0]
lon_rad = lon * cspice.rpd()
lat_rad = lat * cspice.rpd()
spoint = cspice.georec( lon_rad, lat_rad, alt, radii[0], f )  # also input for gfilum!

# parameter for gfoclt search
occtyp = "PARTIAL"
front = "EARTH"
fshape = "ELLIPSOID"
fframe = "IAU_EARTH"
back = "MOON"
bshape = "ELLIPSOID"
bframe = "IAU_MOON"
step_1 = 100  #  stepsize [sec]
result_1 = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
cnfine_1 = cspice.utils.support_types.SPICEDOUBLE_CELL(2)


# pre-computations
et_begin = cspice.utc2et(begin)
et_end = cspice.utc2et(end)
ET = [et_begin, et_end]
cspice.appndd(ET, cnfine_0)

# compute gf search 0 (pahse angles to see some parts of the moon)
# phase angle below  x [degrees]
refval = phase_angle_max * cspice.rpd()  # now in [rad]
relate = '<'
cspice.gfilum(method, angtyp, target, illumn, frame, abcorr, obsrvr, spoint, relate, refval, adjust, step, nintvls, cnfine_0, result_0)
count_0 = cspice.wncard(result_0)
print(count_0)

# compute gf search 1 (full occultations)
cspice.gfoclt(occtyp, front, fshape, fframe, back, bshape, bframe, abcorr, obsrvr, step_1, result_0, result_1)
# print results
count = cspice.wncard(result_1)
print(count)

instrument_ID = str(cspice.bodn2c(inst))  # ID code for instrument name
frame_instrument_pool = 'INS' + instrument_ID + '_FOV_FRAME'
frame_instrument = cspice.gcpool(frame_instrument_pool, 0, 1)[0]  # frame name of instrument
boresight_pool = 'INS' + instrument_ID + '_BORESIGHT'
boresight = cspice.gdpool(boresight_pool, 0, 3)  # boresight vector of sensor

if count == 0:
    print("Result window is empty.\n\n")
else:
    for i in range(count):
        # Fetch the endpoints of the Ith interval of the result window.
        start, stop = cspice.wnfetd(result_1, i)

        ET = np.linspace(start-3600, stop+3600,(stop+3600)-(start-3600))  # the 3600 is included to have CK coverage 1 hour before and after the event

        # vector to target
        vec = np.array([cspice.spkpos(target, ET[i], 'J2000', abcorr, obsrvr)[0] for i in range(len(ET))])

        q = []
        a = []
        b = []
        c = []
        # the boresight_j2000 vector equals the boresight vector in the instrument frame as we define that the instrument frame
        # is orientated as the j2000 frame. This means it needs to rotate about the output quaternion to look at the specified
        # target. But this rotation needs to be "implemented". This is a difference compared with the already existing attitude
        # data where the SC was already rotated!
        for k in range(len(vec)):
            # normal vector of cross product
            H = cspice.ucrss(boresight, vec[k])
            # rotation angle   # vnorm boresight_j2000 = 1; -> not calculated.
            I = cspice.vdot(vec[k], boresight) / (cspice.vnorm(vec[k]))
            J = np.arccos(I)
            K = cspice.axisar(H, J)
            q.append(cspice.m2q(K)[0])  # store the quaternion entries seperated for easier plot
            a.append(cspice.m2q(K)[1])
            b.append(cspice.m2q(K)[2])
            c.append(cspice.m2q(K)[3])


        if plot == 'on':
            # plot the calculated data
            style = 'YYYY-MM-DD-HR-MN-SC.###'
            timeline = []
            for item in ET:
                time = cspice.timout(item, style, 23)
                sub = time.replace(".", "-").split("-")
                timeline.append(datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]),
                                                  int(sub[3]), int(sub[4]), int(sub[5]), int(sub[6]), tzinfo=None))

            fig, ax = plt.subplots(figsize=(15, 7))
            ax.set_ylabel('Rotation [-]', fontsize=18)
            plt.plot(timeline, q, label='quat q', color='green')
            plt.plot(timeline, a, label='quat a', color='brown')
            plt.plot(timeline, b, label='quat b', color='blue')
            plt.plot(timeline, c, label='quat c', color='red')
            plt.gcf().autofmt_xdate()
            ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=4)
            for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(15)
            for tick in ax.yaxis.get_major_ticks():
                tick.label.set_fontsize(15)
            plt.grid()
            # fig.savefig(flp_path.plots + 'validation_spk_magnitude_' + time_start + '_' + time_end + '.png', dpi=fig.dpi)
            plt.show()

        # write values as TM input file
        style_2 = 'YYYY-MM-DD HR-MN-SC.###'
        name = 'TM_test_output_' + str(i) + '.csv'
        file = open(flp_path.TM_produced + name, 'w')
        text = str(
            '"time";"timestamp";"AYTFQU00";"AYTFQU01";"AYTFQU02";"AYTFQU03"' + '\n'
        )
        file.write(text)
        for k in range(len(ET)):
            file.write(';'.join(
                [cspice.timout(ET[k], style_2, 23), 'sclk_clock', str(a[k]), str(b[k]), str(c[k]), str(q[k]) + '\n']))
        file.close()

        if (start == stop):
            time = cspice.et2utc(start, 'C', 3)
            print("Event time: " + time + "\n")
        else:
            time_left = cspice.et2utc( start, 'C', 3)
            time_right = cspice.et2utc( stop, 'C', 3)
            print( "Interval " + str(i+1) + "\n")
            print( "From : " + time_left + " \n")
            print( "To   : " + time_right + " \n")
            print( " \n" )



cspice.unload(flp_path.mk_out)



'''
This script should be used if Cosmographia should be started with a default sensor view of the specified sensor.
This view is useful to improve the feeling about what should be shown in taken pictures. This is useful if you want to
validated pre-caculated scenarios as "moon rising over Earth".
With this routine it is possible to show the pointing of an instrument even if it does not have intercepting points
of all FOV rays with Earth as needed for the FOV_basemap script.
Note that you need to change the visualisation start time manually. The sensor name needs to be sensor name defined in
"json/sensor_xxxx". You can define if you want to see only the newest target created, all targets and if the GS
should be shown.
'''


import spiceypy as cspice
import os
import subprocess
import json
from h.path_setup import flp_path
from collections import OrderedDict

utc = '2018 MAY 19 10:32:10.026'
sensor = 'FLP_MICS'  # make sure you use the real sensor name defined in "json/sensor_xxxx"
exclude_stations = 'off'
exclude_old_targets = 'off'

cspice.furnsh(flp_path.mk_out)


def get_fov_angle():
    # always use DEGREE
    sensor_ID = str(cspice.bodn2c(sensor))
    in_ref_angle = 'INS' + sensor_ID + '_FOV_REF_ANGLE'
    in_cross_angle = 'INS' + sensor_ID + '_FOV_CROSS_ANGLE'
    ref_angle_temp = cspice.gdpool(in_ref_angle, 0, 1)
    cross_angle_temp = cspice.gdpool(in_cross_angle, 0, 1)
    ref_angle = ref_angle_temp[0]
    cross_angle = cross_angle_temp[0]
    if ref_angle >= cross_angle:
        FOV_angle = ref_angle * 2.5  # multiply by 2 to get a nice fit of the FOV in the cosmographia screenshot
    else:
        FOV_angle = cross_angle * 2.5

    return FOV_angle


def cosmoscript_sensor(sensor):

    file = open(flp_path.cosmo_script_sensor, 'w')

    text = str( 'import cosmoscripting' + '\n\n' + \
                'cosmo = cosmoscripting.Cosmo()' + '\n\n' + \
                'cosmo.hideTrajectory("Moon")' + '\n' + \
                'cosmo.hideTrajectory("Mercury")' + '\n' + \
                'cosmo.hideTrajectory("Venus")' + '\n' + \
                'cosmo.hideTrajectory("Earth")' + '\n' + \
                'cosmo.hideTrajectory("Mars")' + '\n' + \
                'cosmo.hideTrajectory("Jupiter")' + '\n' + \
                'cosmo.hideTrajectory("Saturn")' + '\n' + \
                'cosmo.hideTrajectory("Uranus")' + '\n' + \
                'cosmo.hideTrajectory("Neptune")' + '\n\n' + \
                'cosmo.showObject("' + sensor + '")'
                )
    file.write(text)


def update_scenario_json_sensor(exclude_old_targets, exclude_stations):

    items = [
        "./metakernels.json",
        "./spacecraft_FLP.json",
        "./sensors/" + sensor + ".json"
    ]

    if exclude_stations != 'on':
        items.append("./stations.json")

    if exclude_old_targets != 'on':
        target_items_old = os.listdir(flp_path.json_target_old)
        for i in range(len(target_items_old)):
            items.append("./targets/old/" + target_items_old[i])


    obs_item_new = os.listdir(flp_path.json_target_new)
    items.append("./targets/new/" + obs_item_new[0])  # there should only be 1 new item

    json_data = OrderedDict([('version', '1.0'),
                        ('name', 'load FLP'),
                        ('require',
                            items
                        )])

    json_path = flp_path.json_scenario
    with open(json_path, 'w') as outfile:
        json.dump(json_data, outfile, indent=3)


def launch_cosmographia(utc, FOV_angle):

    et = cspice.str2et(utc)
    jd = str(cspice.j2000() + et / cspice.spd())

    cosmo_launch = "./Cosmographia.sh"+" -style=gtk"

    # center, select, frame, jd, cam_x, cam_y, cam_z, cam_qw, cam_qx, cam_qy, cam_qz, ts, cam_fov
    val = [sensor, 'FLP', 'bfix', jd , '0.0', '0.0', '0.1', '0.0', '0.0', '1.0', '0.0', '0', str(FOV_angle)]

    cosmo_state = "'cosmo:" + val[0] + "?&select=" + val[1] + "&frame=" + val[2] + "&jd=" + val[3] + "&x=" + \
                        val[4] + "&y=" + val[5] + "&z=" + val[6] + "&qw=" + val[7] + "&qx=" + val[8] + "&qy=" + val[9] + \
                        "&qz=" + val[10] + "&ts=" + val[11] + "&fov=" + val[12] + "'"

    cosmographia_command = cosmo_launch + " -u " + cosmo_state + " -p " + flp_path.cosmo_script_sensor + " " + flp_path.cosmo_json

    os.chdir(flp_path.cosmographia)
    subprocess.call(cosmographia_command, shell=True)


FOV_angle = get_fov_angle()
cosmoscript_sensor(sensor)
update_scenario_json_sensor(exclude_old_targets, exclude_stations)
launch_cosmographia(utc, FOV_angle)
'''
This file should be used to propagate the orbit some days in the future.
'''


import os
import subprocess
import csv
import random
import re
import datetime
import matplotlib.pyplot as plt
import numpy as np
import spiceypy as cspice
from h.path_setup import flp_path


# initial start conditions
utc_fix = '2018 MAR 16 10:25:00'
# Get utc time for FLP GPS states to test against (for epsilon calculation)
utc_test = [
    '2018 MAR 16 10:25:00', '2018 MAR 16 11:40:00',
    '2018 MAR 16 11:50:00', '2018 MAR 16 13:20:00',
    '2018 MAR 16 13:40:00', '2018 MAR 16 14:30:00',
    '2018 APR 06 10:00:00', '2018 APR 06 17:00:00'
]
# maximal iterations until abort
max_iter = [1000, 1000, 1000, 1]
# condition to break the for loop before max_iter is reached (steps needed to find a better solution)
del_i = [50, 50, 50, 1]
# step size; [km] for position; [m/s] for velocity (so the values are in the same order of magnitude)
step = [0.000003, 0.0000003, 0.00000003, 0.000000000003]




def manage_spk_data_input():

    # Get all TM input files
    print('Searching for input files ...')
    files_tmp = []
    for file in os.listdir(flp_path.TM_path):
        if file.endswith(".csv"):
            files_tmp.append(file)
    marker = '~'
    files = []
    for i in range(len(files_tmp)):
        if marker not in files_tmp[i]:
            files.append(files_tmp[i])
    print('Amount of input files detected: ' + str(len(files)))

    # Create new spk and ck source files
    cspice.furnsh(flp_path.lsk_file)  # needed to convert between times using cspice functions

    print('Started TM import ...')
    input = []
    for i in range(len(files)):
        with open(flp_path.TM_path + files[i]) as file:

            # read file content
            reader = csv.reader(file, delimiter=';')
            rows = [r for r in reader]
            print('File ' + str(i+1) + ' (' + files[i] + ') has ' + str(len(rows)) + ' rows')
            header = rows[0]
            print('Computations ongoing ...')
            input_data = rows[1:]

            # check for default lines and store data in lists
            utc = []
            sc_data = []
            data_utc_sc_data = []
            cspice.furnsh(flp_path.lsk_file)
            for k in range(len(input_data)):
                if input_data[k][2] != '0':
                    utc.append(input_data[k][0])
                    sc_data.append(input_data[k][2:])
                    data_utc_sc_data.append(input_data[k])

            if 'AYTPOS00' in header[2]:
                # append data from this file to the overall GPS data
                input.append(data_utc_sc_data)

    input.sort()  # needed to be sorted for a nice timeline spk - gps data comparsion plot

    print('Step 1 completed \n\n')
    cspice.unload(flp_path.lsk_file)

    return input


def create_gmat_spk(input, utc_fix, utc_test, max_iter, del_i, step):
    # load all SPICE kernels needed for spice function usage (not the SPK file, this will be loaded individually)
    cspice.furnsh(flp_path.lsk_file)
    cspice.furnsh(flp_path.fk_file)
    cspice.furnsh(flp_path.pck_itrf)
    # if there is no continuous GPS data available this routine will fail!
    # this is due to the fact that using GMAT propagated gaps might be not precise enough and would yield to an even
    # more unpredictable propgagation.
    cspice.furnsh(flp_path.spk_out_name)

    eps = 100000000  # set a high epsilon as start error
    et_fix = cspice.str2et(utc_fix)
    et_test = []
    for i in range(len(utc_test)):
        et_test.append(cspice.str2et(utc_test[i]))


    #et_test = [  # offset in [seconds]
    #    et_fix + 0, et_fix + 25200,  # do not use the end date of one as the beginning of another!
    #    et_fix + 0, et_fix + 25200,
    #    et_fix + 1900000, et_fix + 2000000
    #           ]

    ET_input = []
    for item in input:
        for subitem in item:
            et_tmp = cspice.utc2et(subitem[0])
            if et_tmp not in ET_input:  # no time two times...
                ET_input.append(et_tmp)

    cut = []
    for i in range(len(et_test)):  # iterate through all utc test windows
        for k in range(0,len(ET_input)-1):
            if ET_input[k] <= et_test[i] and ET_input[k+1] > et_test[i]:
                if cut == []:
                    cut.append(k + 1)
                    break
                if cut[-1] != k + 1:
                    cut.append(k + 1)
                    break

    if len(cut)%2 != 0:
        print('Please check your input. It needs to be at least 10 hour of continous data.')
        quit()

    # get the test times within the utc_test windows for which GPS data was available:
    ET= []
    for i in range(int(np.floor(len(cut)/2))):
        et = []
        for k in range(cut[2*i],cut[2*i+1]):
            et.append(ET_input[k])
        ET.append(et)

    # it is important to transform the (ITRF93 data) frame to the J2000Eq frame as this is the frame used for GMAT propagations!!!
    state = cspice.spkezr('-513', et_fix, 'J2000', 'NONE', 'EARTH')[0]

    progress_storage_list = []
    state_storage_list = [state]
    eps_storage_list = [eps]

    # start iterations
    print('If nothing is happening, maybe GMAT got a proplem, delete the  > /dev/null in the code to debug...')
    for loop in range(len(step)):
        duration = np.max(
            ET[loop]) - et_fix + 100  # GMAT script calculation duration: # the 100[s] are used as buffer
        state, progress, eps, gmat_sim_spk_name = comp_state(duration, max_iter[loop], del_i[loop], ET[loop],
                                                                  state, utc_fix, step[loop], eps, loop)
        progress_storage_list.append(progress)
        state_storage_list.append(state)
        eps_storage_list.append(eps)
        eps = 1000 * eps  # because a longer timespan is simulated (and the points to check are getting more
        print(loop)
        # (because i set it like that)...which increases normally the difference error

    # Copy best kernel and remove all other created files
    move = "mv " + flp_path.gmat_storage_future + gmat_sim_spk_name + " " + flp_path.spk_prop_name
    subprocess.call(move, shell=True)
    remove = "rm " + flp_path.gmat_storage_future + "*"
    subprocess.call(remove, shell=True)

    # print results (docu)
    state_storage = np.asarray(state_storage_list).astype(np.float)
    eps_storage = np.asarray(eps_storage_list).astype(np.float)

    print(utc_fix)

    for g in range(len(progress_storage_list)):
        for k in range(len(progress_storage_list[g])):
            print(progress_storage_list[g][k])

    for i in range(len(state_storage) - 1):
        print(state_storage[i] - state_storage[i + 1], eps_storage[i + 1])

    # show the results of the best fit
    plot_gmat_comparison(flp_path.spk_out_name, flp_path.spk_prop_name)
    plot_gmat_comparison_2(flp_path.spk_prop_name, ET)


def comp_state(duration, max_iter, del_i, et_test, state_fix, utc_fix, step, eps, loop):
    # Values for for loop
    i_old = 0
    progress = []
    check_points = len(et_test)
    print(check_points)

    # iterate over start conditions to get the "true" values
    for i in range(max_iter):

        state_new = [state_fix[k] + step * np.tan(np.pi * random.uniform(0, 1)) for k in range(len(state_fix))]

        storage_spk_file, gmat_sim_spk_name = gmat_propagator(utc_fix, state_new, duration, i, loop)

        eps_new = error_calculator(et_test, storage_spk_file)

        print("[" + str(i) + "/" + str(max_iter) + "]")

        if eps_new <= eps:
            progress.append([i, i_old, state_new, eps_new/check_points])
            eps = eps_new
            state_fix = [m for m in state_new]
            print([i, i_old, state_new, eps_new/check_points])
            if i - i_old <= del_i:
                i_old = i
            else:
                print("got it")
                break

    return state_fix, progress, eps, gmat_sim_spk_name


def gmat_propagator(utc_fix, state_new, duration, i, loop):
    # file locations and names
    utc_fix_start = cspice.et2utc(cspice.str2et(utc_fix), 'ISOC', 0)
    utc_fix_end = cspice.et2utc(cspice.str2et(utc_fix) + duration, 'ISOC', 0)
    gmat_sim_start = utc_fix_start.replace("-", "").replace(" ", "").replace(":", "")
    gmat_sim_stop = utc_fix_end.replace("-", "").replace(" ", "").replace(":", "")
    gmat_sim_spk_name = 'spk_prop_' + gmat_sim_start + '_' + gmat_sim_stop + "_v" + str(loop) + "_t" + str(i) + ".bsp"
    gmat_sim_script_name = 'spk_prop_' + gmat_sim_start + '_' + gmat_sim_stop + "_v" + str(loop) + "_t" + str(i) + ".script"

    default_script_name = flp_path.gmat_default_script
    storage_script_name = flp_path.gmat_storage_future + gmat_sim_script_name
    storage_spk_file = flp_path.gmat_storage_future + gmat_sim_spk_name
    gmat_console_path = flp_path.gmat

    # copy GMAT default_script
    copy = "cp " + default_script_name + " " + storage_script_name
    subprocess.call(copy, shell=True)

    # compute needed values for usage in GMAT script
    style = 'DD Mon YYYY HR:MN:SC.###'
    et_fix = cspice.str2et(utc_fix)
    state_utc = cspice.timout(et_fix, style, 25)

    # insert state_new (and utc_fix) parameters in GMAT script
    file = open(storage_script_name)
    input = file.read()
    file.close()
    file = open(storage_script_name, 'w')
    output = re.sub(r'FLP_SC\.Epoch\s*=\s*\'Start_Time\'', "FLP_SC.Epoch = '" + state_utc + "'", input)
    output = re.sub(r'FLP_SC\.ElapsedSecs\s*=\s*\'Duration\'', "FLP_SC.ElapsedSecs = " + str(duration), output)
    output = re.sub(r'FLP_SPK\.Filename\s*=\s*\'Output_File_Path_Name\'',
                    "FLP_SPK.Filename = '" + storage_spk_file + "'", output)
    output = re.sub(r'FLP_SC\.X\s*=\s*\'X\'', "FLP_SC.X = " + str(state_new[0]), output)
    output = re.sub(r'FLP_SC\.Y\s*=\s*\'Y\'', "FLP_SC.Y = " + str(state_new[1]), output)
    output = re.sub(r'FLP_SC\.Z\s*=\s*\'Z\'', "FLP_SC.Z = " + str(state_new[2]), output)
    output = re.sub(r'FLP_SC\.VX\s*=\s*\'VX\'', "FLP_SC.VX = " + str(state_new[3]), output)
    output = re.sub(r'FLP_SC\.VY\s*=\s*\'VY\'', "FLP_SC.VY = " + str(state_new[4]), output)
    output = re.sub(r'FLP_SC\.VZ\s*=\s*\'VZ\'', "FLP_SC.VZ = " + str(state_new[5]), output)
    file.write(output)
    file.close()

    # Run GMAT script
    os.chdir(gmat_console_path)
    gmat_script_command = './GmatConsole ' + storage_script_name + ' > /dev/null'
    subprocess.call(gmat_script_command, shell=True)

    return storage_spk_file, gmat_sim_spk_name


def error_calculator(et_test, storage_spk_file):

    state_gps_test = np.array(
        [cspice.spkezr('-513', et_test[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(et_test))])
    cspice.furnsh(storage_spk_file)  # loading order is important!!
    state_gmat_test = np.array(
        [cspice.spkezr('-513', et_test[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(et_test))])
    cspice.unload(storage_spk_file)

    # calculate the error (epsilon)
    square = []
    eps_new_tmp = []
    for k in range(len(et_test)):
        for i in range(6):
            if i < 3:
                square.append(np.square(state_gmat_test[k, i] - state_gps_test[k, i]))
            if i >= 3:
                square.append(np.square(state_gmat_test[k, i] * 1000 - state_gps_test[
                    k, i] * 1000))  # get the velocity in the size of the coordinates

        eps_new_tmp.append(np.sqrt(sum(square)))

    eps_new = sum(eps_new_tmp)
    return eps_new


def plot_gmat_comparison(spk1, spk2):
    # get the time window to plot (overlapping time for 2 compared spk kernels)
    time_gps_tmp = spk_window(spk1, -513)
    time_gmat_tmp = spk_window(spk2, -513)

    et_gmat_tmp_0 = cspice.utc2et(time_gmat_tmp[0])
    et_gmat_tmp_1 = cspice.utc2et(time_gmat_tmp[1])
    et_gps_tmp_0 = cspice.utc2et(time_gps_tmp[0])
    et_gps_tmp_1 = cspice.utc2et(time_gps_tmp[1])

    print(time_gps_tmp)
    print(time_gmat_tmp)

    if et_gps_tmp_0 < et_gmat_tmp_0:
        et_tmp_0 = et_gmat_tmp_0
    else:
        et_tmp_0 = cspice.utc2et(time_gps_tmp[0])

    if et_gps_tmp_1 > et_gmat_tmp_1:
        et_tmp_1 = et_gmat_tmp_1
    else:
        et_tmp_1 = et_gps_tmp_1

    if et_tmp_1-et_tmp_0 < 100000:
        points = et_tmp_1-et_tmp_0
    else:
        points = 100000
    ET_plot = np.linspace(et_tmp_0, et_tmp_1, points)
    print(cspice.et2utc(ET_plot[0], 'C', 0))
    print(cspice.et2utc(ET_plot[-1], 'C', 0))

    # load needed kernels and calculate the states
    flp_gps_plot = np.array(
        [cspice.spkezr('-513', ET_plot[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET_plot))])

    # Note: loading order is very important!!!!
    cspice.furnsh(spk2)
    flp_gmat_plot = np.array(
        [cspice.spkezr('-513', ET_plot[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET_plot))])
    cspice.unload(spk2)

    diff_pos = np.array([np.sqrt(np.square(flp_gps_plot[i, 0] - flp_gmat_plot[i, 0]) \
                                 + np.square(flp_gps_plot[i, 1] - flp_gmat_plot[i, 1]) \
                                 + np.square(flp_gps_plot[i, 2] - flp_gmat_plot[i, 2])) \
                         for i in range(len(flp_gps_plot))])

    diff_vel = np.array([np.sqrt(np.square(flp_gps_plot[i, 3] - flp_gmat_plot[i, 3]) \
                                 + np.square(flp_gps_plot[i, 4] - flp_gmat_plot[i, 4]) \
                                 + np.square(flp_gps_plot[i, 5] - flp_gmat_plot[i, 5])) \
                         for i in range(len(flp_gps_plot))])

    # general input to get nice labels
    style = 'YYYY-MM-DD-HR-MN-SC.###'
    time = []
    for item in ET_plot:
        time.append(cspice.timout(item, style, 23))
    timeline = []
    for i in range(len(time)):
        sub = time[i].replace(".", "-").split("-")
        timeline.append(datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]),
                                          int(sub[3]), int(sub[4]), int(sub[5]), int(sub[6]), tzinfo=None))

    # plot x position
    fig, ax = plt.subplots(figsize=(15, 5))
    plt.plot(timeline, flp_gps_plot[:, 0], label='gps spk x')
    plt.plot(timeline, flp_gmat_plot[:, 0], label='gmat spk x')
    plt.gcf().autofmt_xdate()
    plt.legend()
    plt.grid()
    plt.show()

    # plot x,y,z position difference and sart(sqaured) difference
    fig, ax = plt.subplots(figsize=(15, 5))
    plt.plot(timeline, flp_gps_plot[:, 0] - flp_gmat_plot[:, 0], label='dif gps spk x - gmat spk x')
    plt.plot(timeline, flp_gps_plot[:, 1] - flp_gmat_plot[:, 1], label='dif gps spk y - gmat spk y')
    plt.plot(timeline, flp_gps_plot[:, 2] - flp_gmat_plot[:, 2], label='dif gps spk z - gmat spk z')
    plt.plot(timeline, diff_pos, '--', label='dif sqrt(square) pos')
    plt.gcf().autofmt_xdate()
    plt.legend()
    plt.grid()
    plt.show()

    # plot x,y,z velocity difference
    fig, ax = plt.subplots(figsize=(15, 5))
    plt.plot(timeline, flp_gps_plot[:, 3] - flp_gmat_plot[:, 3], label='dif gps spk vx - gmat spk vx')
    plt.plot(timeline, flp_gps_plot[:, 4] - flp_gmat_plot[:, 4], label='dif gps spk vy - gmat spk vy')
    plt.plot(timeline, flp_gps_plot[:, 5] - flp_gmat_plot[:, 5], label='dif gps spk vz - gmat spk vz')
    plt.plot(timeline, diff_vel, '--', label='dif sqrt(square) vel')
    plt.gcf().autofmt_xdate()
    plt.legend()
    plt.grid()
    plt.show()


def plot_gmat_comparison_2(spk2, ET):


    if ET[1][-1]-ET[0][0] < 100000:
        points = ET[1][-1]-ET[0][0]
    else:
        points = 100000
    ET_plot = np.linspace(ET[0][0],ET[1][-1], points)
    print(cspice.et2utc(ET_plot[0], 'C', 0))
    print(cspice.et2utc(ET_plot[-1], 'C', 0))

    # load needed kernels and calculate the states
    flp_gps_plot = np.array(
        [cspice.spkezr('-513', ET_plot[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET_plot))])

    # Note: loading order is very important!!!!
    cspice.furnsh(spk2)
    flp_gmat_plot = np.array(
        [cspice.spkezr('-513', ET_plot[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET_plot))])
    cspice.unload(spk2)

    diff_pos = np.array([np.sqrt(np.square(flp_gps_plot[i, 0] - flp_gmat_plot[i, 0]) \
                                 + np.square(flp_gps_plot[i, 1] - flp_gmat_plot[i, 1]) \
                                 + np.square(flp_gps_plot[i, 2] - flp_gmat_plot[i, 2])) \
                         for i in range(len(flp_gps_plot))])

    diff_vel = np.array([np.sqrt(np.square(flp_gps_plot[i, 3] - flp_gmat_plot[i, 3]) \
                                 + np.square(flp_gps_plot[i, 4] - flp_gmat_plot[i, 4]) \
                                 + np.square(flp_gps_plot[i, 5] - flp_gmat_plot[i, 5])) \
                         for i in range(len(flp_gps_plot))])

    # general input to get nice labels
    style = 'YYYY-MM-DD-HR-MN-SC.###'
    time = []
    for item in ET_plot:
        time.append(cspice.timout(item, style, 23))
    timeline = []
    for i in range(len(time)):
        sub = time[i].replace(".", "-").split("-")
        timeline.append(datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]),
                                          int(sub[3]), int(sub[4]), int(sub[5]), int(sub[6]), tzinfo=None))

    # plot x position
    fig, ax = plt.subplots(figsize=(15, 5))
    plt.plot(timeline, flp_gps_plot[:, 0], label='gps spk x')
    plt.plot(timeline, flp_gmat_plot[:, 0], label='gmat spk x')
    plt.gcf().autofmt_xdate()
    plt.legend()
    plt.grid()
    plt.show()

    # plot x,y,z position difference and sart(sqaured) difference
    fig, ax = plt.subplots(figsize=(15, 5))
    plt.plot(timeline, flp_gps_plot[:, 0] - flp_gmat_plot[:, 0], label='dif gps spk x - gmat spk x')
    plt.plot(timeline, flp_gps_plot[:, 1] - flp_gmat_plot[:, 1], label='dif gps spk y - gmat spk y')
    plt.plot(timeline, flp_gps_plot[:, 2] - flp_gmat_plot[:, 2], label='dif gps spk z - gmat spk z')
    plt.plot(timeline, diff_pos, '--', label='dif sqrt(square) pos')
    plt.gcf().autofmt_xdate()
    plt.legend()
    plt.grid()
    plt.show()

    # plot x,y,z velocity difference
    fig, ax = plt.subplots(figsize=(15, 5))
    plt.plot(timeline, flp_gps_plot[:, 3] - flp_gmat_plot[:, 3], label='dif gps spk vx - gmat spk vx')
    plt.plot(timeline, flp_gps_plot[:, 4] - flp_gmat_plot[:, 4], label='dif gps spk vy - gmat spk vy')
    plt.plot(timeline, flp_gps_plot[:, 5] - flp_gmat_plot[:, 5], label='dif gps spk vz - gmat spk vz')
    plt.plot(timeline, diff_vel, '--', label='dif sqrt(square) vel')
    plt.gcf().autofmt_xdate()
    plt.legend()
    plt.grid()
    plt.show()


def spk_window(spk,sc_id):
    cover = cspice.utils.support_types.SPICEDOUBLE_CELL(1000)
    cspice.spkcov(spk, sc_id, cover)
    intervals = cspice.wncard(cover)
    print(intervals)
    # check if it is one continuous interval or many seperated ones (in this case a
    # error message is thrown as I don't want to include not_continous intervalls
    if intervals >= 2:
        print("Attention: The spk coverage is not continous!!!")

    cspice.furnsh(flp_path.lsk_file)
    tmp_begin = cover[0]
    tmp_end = cover[-1]
    time_0 = cspice.et2utc(tmp_begin, 'ISOC', 0)
    time_1 = cspice.et2utc(tmp_end, 'ISOC', 0)
    time_tmp = [time_0, time_1]
    cspice.unload(flp_path.lsk_file)

    return time_tmp


input = manage_spk_data_input()
create_gmat_spk(input, utc_fix, utc_test, max_iter, del_i, step)

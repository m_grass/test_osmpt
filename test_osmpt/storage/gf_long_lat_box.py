import spiceypy as cspice
from h.path_setup import flp_path

# use GFSNTC four times to define search box
# define input data
begin = "2018 FEB 27 10:00:00"
end = "2018 FEB 27 16:00:00"
step = 200  #  stepsize [s]

latitude_min = 45  # default = -90
latitude_max = 55  # default = 90
longitude_min = 5  # default = -179.99 (because -180 is not allowed)
longitude_max = 15  # default = 180

# "Predefined" parameters for gfsntc and gfilum
target = 'EARTH'  #
frame = 'ITRF93'
method = "Ellipsoid"
obsrvr = 'FLP'
abcorr_sntc = 'NONE'
dref = 'FLP_PAMCAM'
dvec = [0, 0, 1]
crdsys = 'GEODETIC'
adjust = 0
nintvls = 750
cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(2)

# load metakernel
cspice.furnsh(flp_path.mk_out)

# pre-computations
et_begin = cspice.utc2et(begin)
et_end = cspice.utc2et(end)
ET = [et_begin, et_end]
cspice.appndd(ET, cnfine)

# compute gf search:

# latitude above  x [degrees]
coord = 'LATITUDE'
refval = latitude_min * cspice.rpd()  # now in [rad]
relate = '>'
result = cspice.utils.support_types.SPICEDOUBLE_CELL(1500)
cspice.gfsntc(target, frame, method, abcorr_sntc, obsrvr, dref, dvec, crdsys, coord, relate, refval, adjust, step, nintvls, cnfine, result)
count = cspice.wncard(result)
print(count)

# latitude below  x [degrees]
coord = 'LATITUDE'
refval = latitude_max * cspice.rpd()  # now in [rad]
relate = '<'
result_2 = cspice.utils.support_types.SPICEDOUBLE_CELL(1500)
cspice.gfsntc(target, frame, method, abcorr_sntc, obsrvr, dref, dvec, crdsys, coord, relate, refval, adjust, step, nintvls, result, result_2)
count = cspice.wncard(result_2)
print(count)

# latitude above  x [degrees]
coord = 'LONGITUDE'
refval = longitude_min * cspice.rpd()  # now in [rad]
relate = '>'
result_3 = cspice.utils.support_types.SPICEDOUBLE_CELL(1500)
cspice.gfsntc(target, frame, method, abcorr_sntc, obsrvr, dref, dvec, crdsys, coord, relate, refval, adjust, step, nintvls, result_2, result_3)
count = cspice.wncard(result_3)
print(count)

# latitude below  x [degrees]
coord = 'LONGITUDE'
refval = longitude_max * cspice.rpd()  # now in [rad]
relate = '<'
result_4 = cspice.utils.support_types.SPICEDOUBLE_CELL(1500)
cspice.gfsntc(target, frame, method, abcorr_sntc, obsrvr, dref, dvec, crdsys, coord, relate, refval, adjust, step, nintvls, result_3, result_4)
count = cspice.wncard(result_4)
print(count)

# print results
if (count == 0):
    print("Result window is empty.\n\n")
else:
    for i in range(count):
        # Fetch the endpoints of the Ith interval of the result window.
        left, right = cspice.wnfetd(result_4, i)

        if (left == right):
            time = cspice.et2utc(left, 'C', 3)
            print("Event time: " + time + "\n")
        else:
            time_left = cspice.et2utc( left, 'C', 3)
            time_right = cspice.et2utc( right, 'C', 3)
            print( "Interval " + str(i+1) + "\n")
            print( "From : " + time_left + " \n")
            print( "To   : " + time_right + " \n")
            print( " \n" )

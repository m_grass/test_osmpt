'''
This routine is designed to find (partial) occultations of the Moon within the FOV of an instrument.
Attitude data for the time of interest needs to be available!
This routine could be changed slightly to check for Sun or Star or Planet occultations.
'''


import spiceypy as cspice
from h.path_setup import flp_path

cspice.furnsh(flp_path.mk_out)

# define input data
begin = "2018 JAN 01 10:00:00"
end = "2018 JAN 20 01:00:00"
inst = 'FLP_PAMCAM'

# "internal" input data
target = 'MOON'
tshape = 'ELLIPSOID'
tframe = "IAU_MOON"  # should be ignored and left blank for tshape = POINT
abcorr = 'NONE'
obsrvr = 'FLP'
step_1 = 100  #  stepsize [sec]
step_2 = 100
step_3 = 1
result = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(2)
occtyp = "PARTIAL"
front = "EARTH"
fshape = "ELLIPSOID"
fframe = "IAU_EARTH"
back = "MOON"
bshape = "ELLIPSOID"
bframe = "IAU_MOON"

# pre-computations
et_begin = cspice.utc2et(begin)
et_end = cspice.utc2et(end)
ET = [et_begin, et_end]
cspice.appndd(ET, cnfine)

# compute gf search 1 (full occultations)
cspice.gfoclt(occtyp, front, fshape, fframe, back, bshape, bframe, abcorr, obsrvr, step_1, cnfine, result)
# print results
count = cspice.wncard(result)
print(count)

# compute gf search 2
result_2 = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
result_2 = cspice.gftfov(inst, target, tshape, tframe, abcorr, obsrvr, step_2, result)
# print results
count_2 = cspice.wncard(result_2)
print(count_2)

# compute gf search 3
target = 'EARTH'
tframe = "IAU_EARTH"
result_3 = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
result_3 = cspice.gftfov(inst, target, tshape, tframe, abcorr, obsrvr, step_3, result_2)
# print results
count_3 = cspice.wncard(result_3)
print(count_3)

if count_3 == 0:
    print("Result window is empty.\n\n")
else:
    for i in range(count_3):
        # Fetch the endpoints of the Ith interval of the result window.
        start, stop = cspice.wnfetd(result_3, i)

        if (start == stop):
            time = cspice.et2utc(start, 'C', 3)
            print("Event time: " + time + "\n")
        else:
            time_left = cspice.et2utc( start, 'C', 3)
            time_right = cspice.et2utc( stop, 'C', 3)
            print( "Interval " + str(i+1) + "\n")
            print( "From : " + time_left + " \n")
            print( "To   : " + time_right + " \n")
            print( " \n" )



cspice.unload(flp_path.mk_out)
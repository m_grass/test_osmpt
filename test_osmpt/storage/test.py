import spiceypy as cspice
from h.path_setup import flp_path

cspice.furnsh(flp_path.mk_out)

utc = '2018 MAY 14 09:43:23.744'

ET = cspice.str2et(utc)
cspice.spkpos('IRS',ET, 'FLP_T', 'NONE', 'FLP')



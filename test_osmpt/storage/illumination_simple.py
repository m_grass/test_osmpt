'''
This routine might be useful to check the illumination condition for photos done in the past.
'''


import spiceypy as cspice
from h.path_setup import flp_path

# load kernels
cspice.furnsh(flp_path.mk_out)
radii = cspice.bodvrd( 'Earth', 'RADII', 3 )[1]
f = (radii[0]-radii[2])/radii[0]

# define point (roughly on Earth surface)
alt   = 0.4  # [km]
lon =  10  # [degrees]
lat =  50  # [degress]

# convert planetocentric r/lon/lat to Cartesian vector
lon_rad = lon * cspice.rpd()
lat_rad = lat * cspice.rpd()
point = cspice.georec( lon_rad, lat_rad, alt, radii[0], f )

# convert UTC to ET
et = cspice.str2et( '2018 JAN 31 01:20:00' )

# compute illumination angles modeling Earth as an ellipsoid
trgepc, srfvec, phase, solar, emissn = cspice.ilumin('Ellipsoid', 'Earth', et, 'ITRF93', 'LT+S', 'FLP', point)

# distance between target point and FLP: [km]
dist = cspice.vnorm(srfvec)
print(dist)

# angle between the spoint-obsrvr vector and the spoint-sun vector (Sun-spoint-FLP): [degrees]
angle_1 = phase * cspice.dpr()
print(angle_1)

# angle between the surface normal vector at 'spoint' and the spoint-sun vector: [degrees]
angle_2 = solar * cspice.dpr()
print(angle_2)

# angle between the surface normal vector at 'spoint' and the spoint-observer vector: [degrees]
angle_3 = emissn * cspice.dpr()
print(angle_3)
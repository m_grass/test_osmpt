import datetime
import matplotlib.pyplot as plt
import numpy as np
import spiceypy as cspice
from h.path_setup import flp_path

time_start = '2018 MAR 09 00:30:00'
time_end = '2018 MAR 09 09:30:00'

# first number = drag area *1000
# second number = flux area *10
# third number = order of gravitational potential
# fourth: gravity field model
# fifth: kg
# sixth + seventh: date + time
name = 'docu'

# load standard kernels
cspice.furnsh(flp_path.lsk_file)
cspice.furnsh(flp_path.pck_itrf)
# get ET timeline and plot timeline
et1 = cspice.utc2et(time_start)
et2 = cspice.utc2et(time_end)
if et2-et1 < 100000:
    points = et2-et1
else:
    points = 100000
ET = np.linspace(et1, et2, points)
style = 'YYYY-MM-DD-HR-MN-SC.###'
time = cspice.timout(ET, style, 23)
timeline = []
for i in range(len(ET)):
    sub_2 = time[i].replace(".", "-").split("-")
    timeline.append(datetime.datetime(int(sub_2[0]), int(sub_2[1]), int(sub_2[2]),
                                      int(sub_2[3]), int(sub_2[4]), int(sub_2[5]), int(sub_2[6]), tzinfo=None))

# SPK data GPS (including GMAT gap fixes!)
cspice.furnsh(flp_path.compare_spk_long_term_TM)
cspice.furnsh(flp_path.compare_spk_long_term_TM_gaps)
spk_data_gps = np.array([cspice.spkezr('-513', ET[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET))])
cspice.furnsh(flp_path.compare_spk_long_term_TM_gaps)
cspice.unload(flp_path.compare_spk_long_term_TM)

# SPK data GMAT
cspice.furnsh(flp_path.spk_prop_name)
spk_data_gmat = np.array([cspice.spkezr('-513', ET[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET))])
cspice.unload(flp_path.spk_prop_name)


diff_pos = np.array([np.abs(spk_data_gps[i, 0] - spk_data_gmat[i, 0]) \
                                 + np.abs(spk_data_gps[i, 1] - spk_data_gmat[i, 1]) \
                                 + np.abs(spk_data_gps[i, 2] - spk_data_gmat[i, 2]) \
                         for i in range(len(spk_data_gps))])

diff_vel = np.array([np.abs(spk_data_gps[i, 3] - spk_data_gmat[i, 3]) \
                                 + np.abs(spk_data_gps[i, 4] - spk_data_gmat[i, 4]) \
                                 + np.abs(spk_data_gps[i, 5] - spk_data_gmat[i, 5]) \
                         for i in range(len(spk_data_gps))])

# plot x position
fig, ax = plt.subplots(figsize=(10, 7))
plt.plot(timeline, spk_data_gps[:, 0], label='gps spk x')
plt.plot(timeline, spk_data_gmat[:, 0], label='gmat spk x')
plt.gcf().autofmt_xdate()
ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=3)
for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(15)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(15)
plt.grid()
fig.subplots_adjust(bottom=0.25)
plt.show()

# plot x,y,z position difference and sart(sqaured) difference
fig, ax = plt.subplots(figsize=(10, 7))
plt.plot(timeline, spk_data_gps[:, 0] - spk_data_gmat[:, 0], label='diff x pos')
plt.plot(timeline, spk_data_gps[:, 1] - spk_data_gmat[:, 1], label='diff x pos')
plt.plot(timeline, spk_data_gps[:, 2] - spk_data_gmat[:, 2], label='diff x pos')
plt.plot(timeline, diff_pos, '--', label='diff sum pos')
plt.gcf().autofmt_xdate()
ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=4)
for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(15)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(15)
plt.grid()
fig.subplots_adjust(bottom=0.25)
fig.savefig(flp_path.plots + 'spk_prop_long_term_2_' + name + '.png', dpi=fig.dpi)
plt.show()

# calculate vector length
vec_len_gps = np.array([np.sqrt(np.square(spk_data_gps[k][0]) + np.square(spk_data_gps[k][1]) + np.square(spk_data_gps[k][2]))
                              for k in range(len(spk_data_gps))])

vec_len_gmat = np.array([np.sqrt(np.square(spk_data_gmat[k][0]) + np.square(spk_data_gmat[k][1]) + np.square(spk_data_gmat[k][2]))
                              for k in range(len(spk_data_gmat))])

fig, ax = plt.subplots(figsize=(10, 7))
ax.set_ylabel('distance [km]', fontsize=18)
plt.plot(timeline, vec_len_gps, label='spk gps', color='green')
plt.plot(timeline, vec_len_gmat, label='spk gmat', color='red')
plt.gcf().autofmt_xdate()
ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=2)
for tick in ax.xaxis.get_major_ticks():
    tick.label.set_fontsize(15)
for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(15)
plt.grid()
fig.subplots_adjust(bottom=0.25)
plt.show()
'''
This script is designed to find every pass for values above a minimal elevation for a topocentric target system. It can
be used to crosscheck the spk files with external routines to provide the same output. It should be used to find the
pass times for GS passes of interest. Furthermore it can be used to determine times when the FLP could theoretically see
a specific target.
Note that there is already a better script (plot_pass) available to check for pass times.
'''


import spiceypy as cspice
from h.path_setup import flp_path


# load metakernel
cspice.furnsh(flp_path.mk_out)

# define input data
begin = "2018 MAY 01 12:00:00"
end = "2018 MAY 08 12:00:00"

target = 'FLP'
frame = 'IRS_TOPO'
abcorr = 'NONE'
obsrvr = 'IRS'
crdsys = 'LATITUDINAL'
coord = 'LATITUDE'
relate = '>'

refval = 80 * cspice.rpd()  # elevation [degree]
adjust = 0
step = 20 * 60  #  stepsize [min]
nintvls = 7500

result = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(2)

# pre-computations
et_begin = cspice.utc2et(begin)
et_end = cspice.utc2et(end)
ET = [et_begin, et_end]
cspice.appndd(ET, cnfine)

# compute gf search
# elevation above x [degrees]
cspice.gfposc(target, frame, abcorr, obsrvr, crdsys, coord, relate, refval, adjust, step, nintvls, cnfine, result)



print(target, frame, abcorr, obsrvr, crdsys, coord, relate, refval, adjust, step, nintvls, cnfine, result)

# max. elevation time
relate_2 = 'LOCMAX'
result_2 = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
cspice.gfposc(target, frame, abcorr, obsrvr, crdsys, coord, relate_2, refval, adjust, step, nintvls, result, result_2)


# print results
count = cspice.wncard(result)
count_2 = cspice.wncard(result_2)
print(count)
print(count_2)

if (count == 0):
    print("Result window is empty.\n\n")
else:
    for i in range(count):
        # Fetch the endpoints of the Ith interval of the result window.
        left, right = cspice.wnfetd(result, i)
        max_el_l, max_el_r = cspice.wnfetd(result_2, i)

        if (max_el_l == max_el_r):
            max_el = max_el_l

            if (left == right):
                time = cspice.et2utc(left, 'C', 3)
                print("Event time: " + time + "\n")
            else:
                time_left = cspice.et2utc( left, 'C', 3)
                time_right = cspice.et2utc( right, 'C', 3)
                time_max = cspice.et2utc(max_el, 'C', 3)
                print( "Interval " + str(i+1) + "\n")
                print( "From : " + time_left + " \n")
                print( "To   : " + time_right + " \n")
                print( "Max  : " + time_max + " \n")
                print( " \n" )


%General Mission Analysis Tool(GMAT) Script
%Created: 2018-03-05 09:44:18


%----------------------------------------
%---------- Spacecraft
%----------------------------------------

Create Spacecraft FLP_SC;
FLP_SC.CoordinateSystem = EarthMJ2000Eq; % GMAT default
FLP_SC.Cd = 2.2; % GMAT default
FLP_SC.Cr = 1.8; % GMAT default
FLP_SC.NAIFId = -513;
FLP_SC.DateFormat = UTCGregorian;
FLP_SC.Epoch = 'Start_Time';
FLP_SC.X = 'X';
FLP_SC.Y = 'Y';
FLP_SC.Z = 'Z';
FLP_SC.VX = 'VX';
FLP_SC.VY = 'VY';
FLP_SC.VZ = 'VZ';
FLP_SC.DryMass = 100;
FLP_SC.DragArea = 'DRAG_AREA';
FLP_SC.SRPArea = 1.5;


%----------------------------------------
%---------- ForceModels
%----------------------------------------

Create ForceModel FLP_FM;
FLP_FM.PrimaryBodies = {Earth}; % GMAT default
FLP_FM.PointMasses = {Luna, Sun};
FLP_FM.SRP = On;
FLP_FM.Drag.AtmosphereModel = JacchiaRoberts;
FLP_FM.RelativisticCorrection = On;
FLP_FM.GravityField.Earth.Degree = 60;
FLP_FM.GravityField.Earth.Order = 60;
FLP_FM.GravityField.Earth.PotentialFile = 'JGM3.cof';
FLP_FM.GravityField.Earth.EarthTideModel = 'SolidAndPole';


%----------------------------------------
%---------- Propagator
%----------------------------------------

Create Propagator FLP_PROP;
FLP_PROP.FM = FLP_FM;
FLP_PROP.Type = RungeKutta89; % GMAT default


%----------------------------------------
%---------- Subscribers
%----------------------------------------

Create EphemerisFile FLP_SPK;
FLP_SPK.Spacecraft = FLP_SC;
FLP_SPK.Filename = 'Output_File_Path_Name';
FLP_SPK.FileFormat = SPK;
FLP_SPK.EpochFormat = UTCGregorian; % GMAT default
FLP_SPK.InitialEpoch = InitialSpacecraftEpoch;
FLP_SPK.FinalEpoch = FinalSpacecraftEpoch;
FLP_SPK.StepSize = IntegratorSteps;
FLP_SPK.Interpolator = Hermite;
FLP_SPK.InterpolationOrder = 7;  % GMAT default
FLP_SPK.CoordinateSystem = EarthMJ2000Eq; % GMAT default
FLP_SPK.WriteEphemeris = true; % GMAT default


%----------------------------------------
%---------- Mission Sequence
%----------------------------------------

BeginMissionSequence;
Propagate FLP_PROP(FLP_SC) {FLP_SC.ElapsedSecs = 'Duration'};

\begindata

PATH_VALUES = ( '/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/' )

PATH_SYMBOLS = ( 'KERNELS' )

KERNELS_TO_LOAD = (

'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/lsk/naif/naif0012.tls', 
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/naif/de432s.bsp', 
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/pck/naif/pck00010.tpc', 
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/pck/naif/earth_000101_180827_180605.bpc', 

'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/sclk/flp.tsc', 
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/FLP.tf', 
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/ik/instruments.ti',

'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/stations/stations.tf', 
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/stations/stations.bsp',

'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/out/spk_master_merge_new.bsp',

'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/ck/out/ck_master.bc',

'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/AALEN.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/AREA_51.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/BARCELONA.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/BEIJING.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/BERLIN.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/HONG_KONG.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/HONOLULU.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/LAS_VEGAS.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/LONDON.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/LOS_ANGELES.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/MADRID.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/MARKTSCHORGAST.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/MILAN.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/MUNICH.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/NEW_YORK.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/SAN_FRANCISCO.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/SINGAPUR.tf',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/fk/targets/SYDNEY.tf',

'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/AALEN.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/AREA_51.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/BARCELONA.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/BEIJING.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/BERLIN.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/HONG_KONG.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/HONOLULU.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/LAS_VEGAS.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/LONDON.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/LOS_ANGELES.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/MADRID.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/MARKTSCHORGAST.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/MILAN.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/MUNICH.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/NEW_YORK.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/SAN_FRANCISCO.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/SINGAPUR.bsp',
'/home/flp/test_osmpt/test_osmpt/test_osmpt/kernels/spk/targets/SYDNEY.bsp',


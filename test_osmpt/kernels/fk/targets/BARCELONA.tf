KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/out/BARCELONA.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-05-04T15:45:22
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_101_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/BARCELONA.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'BARCELONA'
   NAIF_BODY_CODE                      += 399241
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame BARCELONA_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame BARCELONA_TOPO is centered at the
      site BARCELONA, which has Cartesian coordinates
 
         X (km):                  0.4788786448949E+04
         Y (km):                  0.1805340345411E+03
         Z (km):                  0.4194944569177E+04
 
      and planetodetic coordinates
 
         Longitude (deg):         2.1589900000000
         Latitude  (deg):        41.3887900000000
         Altitude   (km):         0.4700000000006E-01
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_BARCELONA_TOPO                =  1399241
   FRAME_1399241_NAME                  =  'BARCELONA_TOPO'
   FRAME_1399241_CLASS                 =  4
   FRAME_1399241_CLASS_ID              =  1399241
   FRAME_1399241_CENTER                =  399241
 
   OBJECT_399241_FRAME                 =  'BARCELONA_TOPO'
 
   TKFRAME_1399241_RELATIVE            =  'ITRF93'
   TKFRAME_1399241_SPEC                =  'ANGLES'
   TKFRAME_1399241_UNITS               =  'DEGREES'
   TKFRAME_1399241_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399241_ANGLES              =  (   -2.1589900000000,
                                             -48.6112100000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_101_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'BARCELONA' )
BARCELONA_CENTER       = 399
BARCELONA_FRAME        = 'ITRF93'
BARCELONA_IDCODE       = 399241
BARCELONA_LATLON       = ( 41.38879 2.15899 0.047 )
BARCELONA_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
BARCELONA_UP       = 'Z'
BARCELONA_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

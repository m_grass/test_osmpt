KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/out/NEW_YORK.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-05-03T16:49:39
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_101_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/NEW_YORK.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'NEW_YORK'
   NAIF_BODY_CODE                      += 399239
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame NEW_YORK_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame NEW_YORK_TOPO is centered at the
      site NEW_YORK, which has Cartesian coordinates
 
         X (km):                  0.1333983376360E+04
         Y (km):                 -0.4653983032433E+04
         Z (km):                  0.4138460877076E+04
 
      and planetodetic coordinates
 
         Longitude (deg):       -74.0059700000000
         Latitude  (deg):        40.7142700000000
         Altitude   (km):         0.5699999999981E-01
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_NEW_YORK_TOPO                 =  1399239
   FRAME_1399239_NAME                  =  'NEW_YORK_TOPO'
   FRAME_1399239_CLASS                 =  4
   FRAME_1399239_CLASS_ID              =  1399239
   FRAME_1399239_CENTER                =  399239
 
   OBJECT_399239_FRAME                 =  'NEW_YORK_TOPO'
 
   TKFRAME_1399239_RELATIVE            =  'ITRF93'
   TKFRAME_1399239_SPEC                =  'ANGLES'
   TKFRAME_1399239_UNITS               =  'DEGREES'
   TKFRAME_1399239_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399239_ANGLES              =  ( -285.9940300000000,
                                             -49.2857300000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_101_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'NEW_YORK' )
NEW_YORK_CENTER       = 399
NEW_YORK_FRAME        = 'ITRF93'
NEW_YORK_IDCODE       = 399239
NEW_YORK_LATLON       = ( 40.71427 -74.00597 0.057 )
NEW_YORK_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
NEW_YORK_UP       = 'Z'
NEW_YORK_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

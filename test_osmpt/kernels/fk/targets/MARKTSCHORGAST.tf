KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/targets/MARKTSCHORGAST.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-05-06T14:53:34
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_101_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/MARKTSCHORGAST.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'MARKTSCHORGAST'
   NAIF_BODY_CODE                      += 399246
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame MARKTSCHORGAST_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame MARKTSCHORGAST_TOPO is centered at the
      site MARKTSCHORGAST, which has Cartesian coordinates
 
         X (km):                  0.4015560603785E+04
         Y (km):                  0.8282685154340E+03
         Z (km):                  0.4869923845354E+04
 
      and planetodetic coordinates
 
         Longitude (deg):        11.6546500000000
         Latitude  (deg):        50.0947500000000
         Altitude   (km):         0.4789999999998E+00
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_MARKTSCHORGAST_TOPO           =  1399246
   FRAME_1399246_NAME                  =  'MARKTSCHORGAST_TOPO'
   FRAME_1399246_CLASS                 =  4
   FRAME_1399246_CLASS_ID              =  1399246
   FRAME_1399246_CENTER                =  399246
 
   OBJECT_399246_FRAME                 =  'MARKTSCHORGAST_TOPO'
 
   TKFRAME_1399246_RELATIVE            =  'ITRF93'
   TKFRAME_1399246_SPEC                =  'ANGLES'
   TKFRAME_1399246_UNITS               =  'DEGREES'
   TKFRAME_1399246_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399246_ANGLES              =  (  -11.6546500000000,
                                             -39.9052500000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_101_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'MARKTSCHORGAST' )
MARKTSCHORGAST_CENTER       = 399
MARKTSCHORGAST_FRAME        = 'ITRF93'
MARKTSCHORGAST_IDCODE       = 399246
MARKTSCHORGAST_LATLON       = ( 50.09475 11.65465 0.479 )
MARKTSCHORGAST_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
MARKTSCHORGAST_UP       = 'Z'
MARKTSCHORGAST_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

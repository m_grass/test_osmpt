KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/targets/LAS_VEGAS.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-06-13T12:03:03
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_target_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/LAS_VEGAS.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'LAS_VEGAS'
   NAIF_BODY_CODE                      += 399255
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame LAS_VEGAS_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame LAS_VEGAS_TOPO is centered at the
      site LAS_VEGAS, which has Cartesian coordinates
 
         X (km):                 -0.2189804550261E+04
         Y (km):                 -0.4666837779100E+04
         Z (km):                  0.3744242689150E+04
 
      and planetodetic coordinates
 
         Longitude (deg):      -115.1372200000000
         Latitude  (deg):        36.1749700000000
         Altitude   (km):         0.6130000000005E+00
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_LAS_VEGAS_TOPO                =  1399255
   FRAME_1399255_NAME                  =  'LAS_VEGAS_TOPO'
   FRAME_1399255_CLASS                 =  4
   FRAME_1399255_CLASS_ID              =  1399255
   FRAME_1399255_CENTER                =  399255
 
   OBJECT_399255_FRAME                 =  'LAS_VEGAS_TOPO'
 
   TKFRAME_1399255_RELATIVE            =  'ITRF93'
   TKFRAME_1399255_SPEC                =  'ANGLES'
   TKFRAME_1399255_UNITS               =  'DEGREES'
   TKFRAME_1399255_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399255_ANGLES              =  ( -244.8627800000000,
                                             -53.8250300000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_target_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'LAS_VEGAS' )
LAS_VEGAS_CENTER       = 399
LAS_VEGAS_FRAME        = 'ITRF93'
LAS_VEGAS_IDCODE       = 399255
LAS_VEGAS_LATLON       = ( 36.17497 -115.13722 0.613 )
LAS_VEGAS_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
LAS_VEGAS_UP       = 'Z'
LAS_VEGAS_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

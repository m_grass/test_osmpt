KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/targets/SYDNEY.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-05-06T14:51:44
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_101_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/SYDNEY.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'SYDNEY'
   NAIF_BODY_CODE                      += 399244
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame SYDNEY_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame SYDNEY_TOPO is centered at the
      site SYDNEY, which has Cartesian coordinates
 
         X (km):                 -0.4646056417245E+04
         Y (km):                  0.2553418217493E+04
         Z (km):                 -0.3534316979609E+04
 
      and planetodetic coordinates
 
         Longitude (deg):       151.2073200000000
         Latitude  (deg):       -33.8678500000000
         Altitude   (km):         0.5800000000277E-01
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_SYDNEY_TOPO                   =  1399244
   FRAME_1399244_NAME                  =  'SYDNEY_TOPO'
   FRAME_1399244_CLASS                 =  4
   FRAME_1399244_CLASS_ID              =  1399244
   FRAME_1399244_CENTER                =  399244
 
   OBJECT_399244_FRAME                 =  'SYDNEY_TOPO'
 
   TKFRAME_1399244_RELATIVE            =  'ITRF93'
   TKFRAME_1399244_SPEC                =  'ANGLES'
   TKFRAME_1399244_UNITS               =  'DEGREES'
   TKFRAME_1399244_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399244_ANGLES              =  ( -151.2073200000000,
                                            -123.8678500000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_101_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'SYDNEY' )
SYDNEY_CENTER       = 399
SYDNEY_FRAME        = 'ITRF93'
SYDNEY_IDCODE       = 399244
SYDNEY_LATLON       = ( -33.86785 151.20732 0.058 )
SYDNEY_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
SYDNEY_UP       = 'Z'
SYDNEY_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

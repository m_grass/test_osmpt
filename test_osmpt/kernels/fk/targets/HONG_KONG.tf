KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/targets/HONG_KONG.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-05-07T18:42:31
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_101_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/HONG_KONG.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'HONG_KONG'
   NAIF_BODY_CODE                      += 399250
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame HONG_KONG_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame HONG_KONG_TOPO is centered at the
      site HONG_KONG, which has Cartesian coordinates
 
         X (km):                 -0.2416441991764E+04
         Y (km):                  0.5387463965225E+04
         Z (km):                  0.2403698325112E+04
 
      and planetodetic coordinates
 
         Longitude (deg):       114.1576900000000
         Latitude  (deg):        22.2855200000000
         Altitude   (km):         0.3000000000848E-02
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_HONG_KONG_TOPO                =  1399250
   FRAME_1399250_NAME                  =  'HONG_KONG_TOPO'
   FRAME_1399250_CLASS                 =  4
   FRAME_1399250_CLASS_ID              =  1399250
   FRAME_1399250_CENTER                =  399250
 
   OBJECT_399250_FRAME                 =  'HONG_KONG_TOPO'
 
   TKFRAME_1399250_RELATIVE            =  'ITRF93'
   TKFRAME_1399250_SPEC                =  'ANGLES'
   TKFRAME_1399250_UNITS               =  'DEGREES'
   TKFRAME_1399250_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399250_ANGLES              =  ( -114.1576900000000,
                                             -67.7144800000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_101_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'HONG_KONG' )
HONG_KONG_CENTER       = 399
HONG_KONG_FRAME        = 'ITRF93'
HONG_KONG_IDCODE       = 399250
HONG_KONG_LATLON       = ( 22.28552 114.15769 0.003 )
HONG_KONG_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
HONG_KONG_UP       = 'Z'
HONG_KONG_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 

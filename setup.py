from setuptools import setup, find_packages
from os import path
from io import open

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name="test_osmpt",
    version="0.0.20",
    author="Markus Grass",
    author_email="markus_grass@web.de",
    description="Open Source Mission Planning Tool",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/m_grass/test_osmpt/",
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    classifiers=(
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
    ),
    keywords='open_source mission_planning',
    include_package_data=True
)
